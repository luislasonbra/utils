if b2 == nil then
  require('Box2D')('2.3.2')
end

require("utils.b2d.access")
require("utils.b2d.new")
require("utils.b2d.index")
require("utils.b2d.query")
require("utils.b2d.callback")
b2.draw = require("utils.b2d.ddraw")