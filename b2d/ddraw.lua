local _styles =
{
  line = 1,
  color = Color(255, 0, 255),
  jcolor = Color(255, 0, 0), -- red
  ccolor = Color(0, 255, 0), -- lime
  mcolor = Color(0, 0, 255), -- blue
  fcolor = Color(0, 255, 255), -- yellow
  -- draw sensor fixtures?
  sensors = true,
  -- draw centers of mass?
  centers = true,
  -- draw velocities?
  velocities = true,
  -- draw positions of bodies?
  positions = true
}

local ddraw = {}

function ddraw.set(key, value)
  _styles[key] = value
end

function ddraw.get(key)
  return _styles[key]
end

function ddraw.draw_body(body, c, wc)
  local line = _styles.line
  local color = _styles.color
  local sensors = _styles.sensors
  -- canvas options
  c:set_line_style(line, color, 1)
  c:set_fill_style(color, 0.5)
  -- collision shapes
  local fixture = body:GetFixtureList()
  while fixture do
    if sensors == true or fixture:IsSensor() == false then
      local shape = fixture:GetShape()
      local t = shape:GetType()
      if t == "circle" then
        local r = shape:GetRadius()
        local lpx, lpy = shape:GetLocalPositionXY()
        local lpx2, lpy2 = lpx, lpy + r
        if wc then
          lpx, lpy = body:GetWorldPointXY(lpx, lpy)
          lpx2, lpy2 = body:GetWorldPointXY(lpx2, lpy2)
        end
        c:move_to(lpx, lpy)
        c:circle(r)
        c:fill_preserve()
        c:stroke()
        c:line_to(lpx2, lpy2)
        c:stroke()
      elseif t == "polygon" or t == "chain" then
        local vertices = shape:GetVertices()
        --assert(#vertices >= 3)
        if #vertices > 0 then
          local vx, vy = vertices[1].x, vertices[1].y
          if wc then
            vx, vy = body:GetWorldPointXY(vx, vy)
          end
          c:move_to(vx, vy)
          for i = 2, #vertices do
            vx, vy = vertices[i].x, vertices[i].y
            if wc then
              vx, vy = body:GetWorldPointXY(vx, vy)
            end
            c:line_to(vx, vy)
          end
          if t ~= "chain" then
            c:close_path()
            c:fill_preserve()
          end
          c:stroke()
        end
      elseif t == "edge" then
        local v1 = shape.vertex1
        local v2 = shape.vertex2
        local v1x, v1y = v1.x, v1.y
        if wc then
          v1x, v1y = body:GetWorldPointXY(v1x, v1y)
        end
        local v2x, v2y = v2.x, v2.y
        if wc then
          v2x, v2y = body:GetWorldPointXY(v2x, v2y)
        end
        c:move_to(v1x, v1y)
        c:line_to(v2x, v2y)
        c:stroke()
      else
        error("unknown shape type")
      end
    end
    fixture = fixture:GetNext()
  end
  -- center of mass
  if _styles.centers then
    local lx, ly = body:GetLocalCenterXY()
    if wc then
      lx, ly = body:GetWorldPointXY(lx, ly)
    end
    c:move_to(lx, ly)
    c:square(line*4)
    c:set_fill_style(_styles.mcolor, 1)
    c:fill()
  end
  if _styles.positions then
    -- center of body
    local lx1, ly1 = 0, 0
    local lx2, ly2 = line*8, 0
    local lx3, ly3 = 0, 0
    local lx4, ly4 = 0, line*8
    if wc then
      lx1, ly1 = body:GetWorldPointXY(lx1, ly1)
      lx2, ly2 = body:GetWorldPointXY(lx2, ly2)
      lx3, ly3 = body:GetWorldPointXY(lx3, ly3)
      lx4, ly4 = body:GetWorldPointXY(lx4, ly4)
    end
    c:move_to(lx1, ly1)
    c:line_to(lx2, ly2)
    c:set_line_style(line, _styles.ccolor, 1)
    c:stroke()
    c:move_to(lx3, ly3)
    c:line_to(lx4, ly4)
    c:set_line_style(line, _styles.color, 1)
    c:stroke()
  end
  -- velocity
  if _styles.velocities then
    --[[
    local lx, ly = body:GetLocalCenterXY()
    if wc then
      lx, ly = body:GetWorldPointXY(lx, ly)
    end
    local a = body:GetAngle()
    local av = body:GetAngularVelocity()
    local ad = math.deg(a)
    local avd = math.deg(av)
    c:move_to(lx, ly)
    c:arc(ad, ad + avd, line*10, line*10)
    c:set_line_style(line, _styles.ccolor, 1)
    c:stroke()
    ]]
    --[[
    local lvx, lvy = body:GetLinearVelocityXY()
    c:move_to(lx, ly)
    c:rel_line_to(lvx, lvy)
    c:set_line_style(line, _styles.fcolor, 1)
    c:stroke()
    ]]
  end
end

function ddraw.draw_bodies(w, c, wc)
  assert(w, "world is nil")
  -- sync bodies
  local b = w:GetBodyList()
  while b do
    ddraw.draw_body(b, c, wc)
    b = b:GetNext()
  end
end

function ddraw.draw_joint(joint, c)
  local line = _styles.line
  local a1x, a1y = joint:GetAnchorAXY()
  local a2x, a2y = joint:GetAnchorBXY()
  c:move_to(a1x, a1y)
  c:circle(line*5)
  if joint:GetType() == "pulleyJoint" then
    local g1 = joint:GetGroundAnchorA()
    local g2 = joint:GetGroundAnchorB()
    c:line_to(g1.x, g1.y)
    c:circle(line*5)
    c:line_to(g2.x, g2.y)
    c:circle(line*5)
  end
  c:line_to(a2x, a2y)
  c:circle(line*5)
  c:set_line_style(line, _styles.jcolor, 1)
  c:stroke()
end

function ddraw.draw_joints(w, c)
  -- draw joints
  local j = w:GetJointList()
  while j do
    ddraw.draw_joint(j, c)
    j = j:GetNext()
  end
end

local mf = b2.Manifold()
local wmf = b2.WorldManifold()
function ddraw.draw_contact(contact, c)
  local line = _styles.line
  --mf = contact:GetManifold()
  contact:GetManifoldCopy(mf)
  contact:GetWorldManifold(wmf)
  --contact:GetWorldManifoldObj(wmf)
  
  local npoints = mf.pointCount
  -- todo: can't figure out the number of collision points without "GetManifold"
  --assert(mf.pointCount == #mf.points)
  
  -- number of contact points
  -- the number of points is either 1 or 2
  -- 2 for edge to edge collisions
  -- 1 for vertex to edge or circle collisions
  if npoints == 2 then
    -- edge to edge
    local p1 = wmf.points[1]
    local p2 = wmf.points[2]
    c:move_to(p1.x, p1.y)
    c:line_to(p2.x, p2.y)
    c:set_line_style(line*2, _styles.color, 1)
    c:stroke()
  elseif npoints == 0 then
    -- non-solid contact
    return
  end
  
  local b1 = contact:GetFixtureA():GetBody()
  local b2 = contact:GetFixtureB():GetBody()
  local m1 = b1:GetMass()
  local m2 = b2:GetMass()

  local n = wmf.normal
  local nx, ny = n.x, n.y
  --[[
  if b1:GetType() == "staticBody" then
    b1, b2 = b2, b1
    nx, ny = -nx, -ny
  end
  ]]

  for i = 1, npoints do
    local p = wmf.points[i]
    local px, py = p.x, p.y
    
    -- contact point
    -- these points are used in the collision response
    -- when applying an impulse to push the fixtures apart
    -- if "isBullet" is false, these might not be
    -- in the exact location where
    -- the fixtures would first have touched
    
    c:move_to(px, py)
    c:square(line*4)
    c:set_fill_style(_styles.color, 1)
    c:fill()
    
    local mp = mf.points[i]
    local ni = mp.normalImpulse
    local ti = mp.tangentImpulse
    if m2 > 0 then -- b1:GetType() ~= "static" then
      -- impulse from b1 to b2
      -- separation/normal impulse
      assert(ni >= 0)
      c:move_to(px, py)
      c:rel_line_to(nx*ni, ny*ni)
      c:set_line_style(line*2, _styles.fcolor, 1)
      c:stroke()
      -- friction/tangent impulse
      c:move_to(px, py)
      c:rel_line_to(ny*ti, -nx*ti)
      c:set_line_style(line*2, _styles.ccolor, 1)
      c:stroke()
    end
    if m1 > 0 then -- b2:GetType() ~= "static" then
      -- impulses form b2 to b1
      local s = 1/m1*m2
      local ni2 = ni*s
      local ti2 = ti*s
      c:move_to(px, py)
      c:rel_line_to(-nx*ni2, -ny*ni2)
      c:set_line_style(line*2, _styles.fcolor, 1)
      c:stroke()
      c:move_to(px, py)
      c:rel_line_to(-ny*ti2, nx*ti2)
      c:set_line_style(line*2, _styles.ccolor, 1)
      c:stroke()
    end
    
    --[[
    local v1x, v1y = contact:GetFixtureA():GetBody():GetLinearVelocityFromWorldPointXY(p.x, p.y)
    local v2x, v2y = contact:GetFixtureB():GetBody():GetLinearVelocityFromWorldPointXY(p.x, p.y)
    local v3x, v3y = v2x - v1x, v2y - v1y
    c:move_to(p.x, p.y)
    c:rel_line_to(v3x*10, v3y*10)
    c:set_line_style(line*2, _styles.mcolor, 1)
    c:stroke()
    ]]
  end
end

function ddraw.draw_contatlist(b, c)
  local ce = b:GetContactList()
  while ce do
    if ce.contact:IsTouching() then
      ddraw.draw_contact(ce.contact, c)
    end
    ce = ce.next
  end
end

function ddraw.draw_contacts(w, c)
  local b = w:GetBodyList()
  while b do
    ddraw.draw_contatlist(b, c)
    b = b:GetNext()
  end
--[[
  -- draw contacts
  local ct = w:GetContactList()
  while ct do
    if ct:IsTouching() then
      ddraw.draw_contact(ct, c)
    end
    ct = ct:GetNext()
  end
  ]]
end

return ddraw