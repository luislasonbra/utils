local circle = b2.CircleShape()
local polygon = b2.PolygonShape()
local fixture = b2.FixtureDef()
local filter = fixture.filter

-- creates shape from xml
function xml.createShape(shape, b2body)
  local bid = b2body.id or "nil"
  local tag = xml.tag(shape)
  
  -- fixture
  fixture.density = xml.find_number(shape, "Density") or 0
  fixture.friction = xml.find_number(shape, "Friction") or 0
  fixture.restitution = xml.find_number(shape, "Restitution") or 0
  fixture.isSensor = xml.find_boolean(shape, "IsSensor") or false
  -- filter
  filter.categoryBits = xml.find_number(shape, "Filter", "category") or 0x0001
  filter.maskBits = xml.find_number(shape, "Filter", "mask") or 0xffff
  filter.groupIndex = xml.find_number(shape, "Filter", "group") or 0
  
  if tag == "ConcaveShape" then
    -- concave polygon
    local path = xml.find_path(shape, "Path")
    assert(#path >= 3, "polygon path with less than 3 vertices")
    local out = {}
    local ret = b2.Fill(path, out)
    if ret == false then
      error("triangulation failed in body: " .. bid)
    end
    assert(#out%3 == 0, "triangulation failed")
    assert(#out >= 3, "triangulation failed")
    polygon.vertexCount = 3
    fixture.shape = polygon
    local triangle = {}
    for i = 1, #out - 2, 3 do
      -- todo: reuse b2vec2s instead of creating them
      triangle[1] = out[i]
      triangle[2] = out[i + 1]
      triangle[3] = out[i + 2]
      --b2.ValidatePolygon(triangle)
      polygon:Set(triangle)
      local b2shape = b2body:CreateFixture(fixture)
      if b2shape == nil then
        error("partially generated polygon in body: " .. bid)
      end
    end
  else
    if tag == "CircleShape" then
      -- circle
      circle.radius = xml.find_number(shape, "Radius")
      -- todo: change "localPosition" to "position"
      local lp = xml.find_point(shape, "LocalPosition")
      if lp then
        circle.position = lp
      else
        circle.position:Set(0, 0)
      end
      fixture.shape = circle
    elseif tag == "PolygonShape" then
      -- convex polygon
      local path = xml.find_path(shape, "Path")
      assert(path, "polygon without path")
      assert(#path >= 2, "polygon with less than 2 vertices")
      assert(#path <= b2.maxPolygonVertices, "polygon exceeds maxPolygonVertices")
      --b2.ValidatePolygon(path)
      polygon.vertexCount = #path
      polygon:Set(path)
      fixture.shape = polygon
    elseif tag == "ChainShape" then
      -- chain
      -- todo: can't reuse
      local chain = b2.ChainShape()
      local path = xml.find_path(shape, "Path")
      if shape.loop == "true" then
        chain:CreateLoop(path)
      else
        chain:CreateChain(path)
      end
      fixture.shape = chain
    else
      error("unknown shape type: " .. tag .. " in body: " .. bid)
      return
    end

    local b2shape = b2body:CreateFixture(fixture)
    assert(b2shape, "could not generate shape")
  end
end
