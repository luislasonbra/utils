local def = b2.BodyDef()

local shapeTypes =
{
  CircleShape = true,
  PolygonShape = true,
  ChainShape = true,
  ConcaveShape = true
}

-- creates body from xml
function xml.createBody(body, b2world)
  local bid = body.id
  assert(bid, "missing body id")
  
  def.type = xml.search(body, "Type", 1)
  
  -- definition
  def.position = xml.find_point(body, "Position")
  def.angle = xml.find_number(body, "Angle") or 0
  def.linearDamping = xml.find_number(body, "LinearDamping") or 0
  def.angularDamping = xml.find_number(body, "AngularDamping") or 0
  def.fixedRotation = xml.find_boolean(body, "FixedRotation") or false
  def.bullet = xml.find_boolean(body, "Bullet") or false
  def.allowSleep = xml.find_boolean(body, "AllowSleep") or false
  local awake = xml.find_boolean(body, "Awake")
  if awake == nil then
    awake = true
  end
  def.awake = awake
  local active = xml.find_boolean(body, "Active")
  if active == nil then
    active = true
  end
  def.active = active
  def.gravityScale = xml.find_number(body, "GravityScale") or 1
  
  local b2body = b2world:CreateBody(def)
  if b2body == nil then
    error("could not create body: " .. bid)
  end
  b2body.id = bid

  -- velocities
  local lv = xml.find_point(body, "LinearVelocity")
  if lv then
    b2body:SetLinearVelocity(lv)
  end
  local av = xml.find_number(body, "AngularVelocity")
  if av then
    b2body:SetAngularVelocity(av)
  end

  -- shapes
  for i = 1, #body, 1 do
    local tag = xml.tag(body[i])
    if shapeTypes[tag] then
      xml.createShape(body[i], b2body)
    end
  end
  
  return b2body
end