local function _search(s, t, a)
  for i = 1, #s do
    local v = s[i]
    if v[0] == t then
      if a then
        v = v[a]
      end
      return v
    end
  end
end
local _b2_Vec2 = b2.Vec2

xml.search = _search

function xml.find_number(s, t, a)
  local r = _search(s, t, a or 1)
  if r then
    return tonumber(r)
  end
end

function xml.find_boolean(s, t, a)
  local r = _search(s, t, a or 1)
  if r == "true" then
    return true
  elseif r == "false" then
    return false
  end
end

function xml.find_point(s, t)
  local r = _search(s, t)
  if r then
    local x = tonumber(r.x)
    local y = tonumber(r.y)
    return _b2_Vec2(x, y)
  end
end

function xml.find_path(s, t)
  local r = _search(s, t)
  local path = {}
  for i = 1, #r do
    local v = r[i]
    local x = tonumber(v.x)
    local y = tonumber(v.y)
    path[i] = _b2_Vec2(x, y)
  end
  return path
end