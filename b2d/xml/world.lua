local buffer = b2.Vec2(0, 0)

local jointTypes =
{
  RevoluteJoint = true,
  WeldJoint = true,
  PrismaticJoint = true,
  DistanceJoint = true,
  PulleyJoint = true,
  GearJoint = true,
  RopeJoint = true
}

-- returns b2world from xml
function xml.createWorld(w, b2world)
  for i = 1, #w do
    local v = w[i]
    local tag = xml.tag(v)
    if tag == "Gravity" then
      local g = b2.Vec2(v.x, v.y)
      b2world:SetGravity(g)
    elseif tag == "Body" then
      local b = xml.createBody(v, b2world)
      local bid = tonumber(v.id)
      b2world:SetBodyID(bid, b)
    else
      -- is it a joint?
      if jointTypes[tag] then
        local j = xml.createJoint(v, b2world)
        local jid = tonumber(v.id)
        b2world:SetJointID(jid, j)
      end
    end
  end
  return b2world
end

-- load world
function xml.loadWorld(file, b2world)
  local box = xml.load(file)
  -- could not load XML file
  assert(box, "could not load XML file")
  local w = xml.find(box, "World")
  assert(w, "invalid Box2D XML file")
  
  if b2world == nil then
    b2world = b2.NewWorld()
    -- todo: set contact listener
    -- before any bodies or shapes are created
    b2world:Index()
  end
  
  xml.createWorld(w, b2world)
  
  return b2world
end

-- unload world
function xml.unloadWorld(b2world)
  b2world:Destroy()
  b2world:Deindex()
end