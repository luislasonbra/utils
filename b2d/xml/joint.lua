local defs =
{
  RevoluteJoint = b2.RevoluteJointDef(),
  PrismaticJoint = b2.PrismaticJointDef(),
  DistanceJoint = b2.DistanceJointDef(),
  PulleyJoint = b2.PulleyJointDef(),
  WeldJoint = b2.WeldJointDef(),
  RopeJoint = b2.RopeJointDef(),
  GearJoint = b2.GearJointDef()
}

-- creates joint from xml
function xml.createJoint(j, b2world)
  local jid = j.id
  assert(jid, "missing joint id")
  
  local tag = xml.tag(j)
  local def = defs[tag]
  
  -- unsupported joint type
  if def == nil then
    error("unsupported joint type: " .. tag)
  end

  if tag == "GearJoint" then
    def.ratio = xml.find_number(j, "Ratio") or 1
    local j1 = xml.find_number(j, "JointA")
    def.joint1 = b2world:GetJoint(j1)
    local j2 = xml.find_number(j, "JointB")
    def.joint2 = b2world:GetJoint(j2)
    if def.joint1 == nil then
      error("joint1 does not exist: " .. jid)
    end
    if def.joint2 == nil then
      error("joint2 does not exist: " .. jid)
    end
    if def.joint1 == def.joint2 then
      error("joint1 == joint2 in gear joint: " .. jid)
    end
  else
    -- local anchors
    def.localAnchorA = xml.find_point(j, "LocalAnchorA")
    def.localAnchorB = xml.find_point(j, "LocalAnchorB")
    
    if tag == "RopeJoint" then
      def.maxLength = xml.find_number(j, "MaxLength") or 0
      if def.maxLength < b2.linearSlop then
        error("max length < linearSlop in joint: " .. jid)
      end
    elseif tag == "WeldJoint" then
      def.referenceAngle = xml.find_number(j, "ReferenceAngle") or 0
      def.frequencyHz = xml.find_number(j, "FrequencyHz") or 0
      def.dampingRatio = xml.find_number(j, "DampingRatio") or 0
    elseif tag == "PulleyJoint" then
      -- ground anchors
      def.groundAnchorA = xml.find_point(j, "GroundAnchorA")
      def.groundAnchorB = xml.find_point(j, "GroundAnchorB")
      def.lengthA = xml.find_number(j, "LengthA") or 0
      def.lengthB = xml.find_number(j, "LengthB") or 0
      def.maxLengthA = xml.find_number(j, "MaxLengthA") or 0
      def.maxLengthB = xml.find_number(j, "MaxLengthB") or 0
      def.ratio = xml.find_number(j, "Ratio") or 1
    elseif tag == "RevoluteJoint" then
      def.referenceAngle = xml.find_number(j, "ReferenceAngle") or 0
      def.enableLimit = xml.find_boolean(j, "EnableLimit") or false
      def.lowerAngle = xml.find_number(j, "LowerAngle") or 0
      def.upperAngle = xml.find_number(j, "UpperAngle") or 0
      def.enableMotor = xml.find_boolean(j, "EnableMotor") or false
      def.motorSpeed = xml.find_number(j, "MotorSpeed") or 0
      def.maxMotorTorque = xml.find_number(j, "MaxMotorTorque") or 0
      if def.lowerAngle > def.upperAngle then
        error("lower limit > upper in joint: " .. jid)
      end
    elseif tag == "PrismaticJoint" then
      -- local axis
      def.localAxisA = xml.find_point(j, "LocalAxisA")
      def.referenceAngle = xml.find_number(j, "ReferenceAngle") or 0
      def.enableLimit = xml.find_boolean(j, "EnableLimit") or false
      def.lowerTranslation = xml.find_number(j, "LowerTranslation") or 0
      def.upperTranslation = xml.find_number(j, "UpperTranslation") or 0
      def.enableMotor = xml.find_boolean(j, "EnableMotor") or false
      def.motorSpeed = xml.find_number(j, "MotorSpeed") or 0
      def.maxMotorForce = xml.find_number(j, "MaxMotorForce") or 0
      if def.lowerTranslation > def.upperTranslation then
        error("lower limit > upper in joint: " .. jid)
      end
    elseif tag == "DistanceJoint" then
      def.length = xml.find_number(j, "Length") or 0
      def.frequencyHz = xml.find_number(j, "FrequencyHz") or 0
      def.dampingRatio = xml.find_number(j, "DampingRatio") or 0
      if def.length < b2.linearSlop then
        error("length < linearSlop in joint: " .. jid)
      end
    end
  end
  
  -- generic joint properties
  def.collideConnected = xml.find_boolean(j, "CollideConnected") or false
  local b1 = tonumber(j.body1)
  def.bodyA = b2world:GetBody(b1)
  local b2 = tonumber(j.body2)
  def.bodyB = b2world:GetBody(b2)
  if def.bodyA == def.bodyB then
    error("bodyA == bodyB in joint: " .. jid)
  end

  local b2joint = b2world:CreateJoint(def)
  if b2joint == nil then
    error("could not create joint: " .. jid)
  end
  b2joint.id = jid

  return b2joint
end