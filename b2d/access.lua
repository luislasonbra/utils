-- Access by reusing intermediate definitions

-- Vec2
local _vec = b2.Vec2(0, 0)
local _pt1 = b2.Vec2(0, 0)
local _pt2 = b2.Vec2(0, 0)
local _Vec2_Set = b2.Vec2.Set

-- Body
local _Body_GetPosition = b2.Body.GetPosition
function b2.Body:GetPositionXY()
  _Body_GetPosition(self, _vec)
  return _vec.x, _vec.y
end

local _Body_GetTransformXYCS = b2.Body.GetTransformXYCS
function b2.Body:GetTransformXY()
  return _Body_GetTransformXYCS(self, 0, 0, 0, 0)
end

local _Body_SetTransform = b2.Body.SetTransform
function b2.Body:SetTransformXY(x, y, a)
  _Vec2_Set(_pt1, x, y)
  _Body_SetTransform(self, _pt1, a)
end

function b2.Body:SetAngle(a)
  _Body_GetPosition(self, _pt1)
  _Body_SetTransform(self, _pt1, a)
end

local _Body_GetWorldPoint = b2.Body.GetWorldPoint
function b2.Body:GetWorldPointXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetWorldPoint(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_GetLocalPoint = b2.Body.GetLocalPoint
function b2.Body:GetLocalPointXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetLocalPoint(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_GetWorldVector = b2.Body.GetWorldVector
function b2.Body:GetWorldVectorXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetWorldVector(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_GetLocalVector = b2.Body.GetLocalVector
function b2.Body:GetLocalVectorXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetLocalVector(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_GetLinearVelocity = b2.Body.GetLinearVelocity
function b2.Body:GetLinearVelocityXY()
  _Body_GetLinearVelocity(self, _vec)
  return _vec.x, _vec.y
end

local _Body_GetLinearVelocityFromWorldPoint = b2.Body.GetLinearVelocityFromWorldPoint
function b2.Body:GetLinearVelocityFromWorldPointXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetLinearVelocityFromWorldPoint(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_GetLinearVelocityFromLocalPoint = b2.Body.GetLinearVelocityFromLocalPoint
function b2.Body:GetLinearVelocityFromLocalPointXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_GetLinearVelocityFromLocalPoint(self, _pt1, _vec)
  return _vec.x, _vec.y
end

local _Body_SetLinearVelocity = b2.Body.SetLinearVelocity
function b2.Body:SetLinearVelocityXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _Body_SetLinearVelocity(self, _pt1)
end

local _Body_ApplyLinearImpulse = b2.Body.ApplyLinearImpulse
function b2.Body:ApplyLinearImpulseXY(ix, iy, x, y, wake)
  _Vec2_Set(_pt1, ix, iy)
  _Vec2_Set(_pt2, x, y)
  _Body_ApplyLinearImpulse(self, _pt1, _pt2, wake)
end

local _Body_ApplyForce = b2.Body.ApplyForce
function b2.Body:ApplyForceXY(fx, fy, x, y, wake)
  _Vec2_Set(_pt1, fx, fy)
  _Vec2_Set(_pt2, x, y)
  _Body_ApplyForce(self, _pt1, _pt2, wake)
end

local _Body_ApplyForceToCenter = b2.Body.ApplyForceToCenter
function b2.Body:ApplyForceToCenterXY(fx, fy, wake)
  _Vec2_Set(_pt1, fx, fy)
  _Body_ApplyForceToCenter(self, _pt1, wake)
end

local _Body_GetLocalCenter = b2.Body.GetLocalCenter
function b2.Body:GetLocalCenterXY()
  _Body_GetLocalCenter(self, _vec)
  return _vec.x, _vec.y
end

local _Body_GetWorldCenter = b2.Body.GetWorldCenter
function b2.Body:GetWorldCenterXY()
  _Body_GetWorldCenter(self, _vec)
  return _vec.x, _vec.y
end

local md = b2.MassData()
local ms_center = md.center
local _Body_GetMassData = b2.Body.GetMassData
local _Body_SetMassData = b2.Body.SetMassData
function b2.Body:SetMass(m, mx, my, i)
  _Body_GetMassData(self, md)
  md.mass = m
  if mx and my then
    _Vec2_Set(ms_center, mx, my)
  end
  if i then
    md.I = i
  end
  _Body_SetMassData(self, md)
end

local _Body_GetFixtureList = b2.Body.GetFixtureList
local _Fixture_GetNext = b2.Fixture.GetNext
local fd = b2.Filter()
local _Fixture_SetFilterData = b2.Fixture.SetFilterData
function b2.Body:SetFilter(c, m, g)
  fd.categoryBits = c
  fd.maskBits = m
  fd.groupIndex = g
  local f = _Body_GetFixtureList(self)
  while f do
    _Fixture_SetFilterData(f, fd)
    f = _Fixture_GetNext(f)
  end
end

-- Fixture
function b2.Fixture:SetFilter(c, m, g)
  fd.categoryBits = c
  fd.maskBits = m
  fd.groupIndex = g
  _Fixture_SetFilterData(self, fd)
end

--[[
function b2.Fixture:GetFilterCMG()
  
end
]]

-- Shape
function b2.Shape:GetRadius()
  return self.radius
end
function b2.Shape:GetLocalPosition()
  return self.position
end
function b2.Shape:GetLocalPositionXY()
  local p = self.position
  return p.x, p.y
end
function b2.Shape:GetVertices()
  return self.vertices
end

-- Joint
local _Joint_GetAnchorA = b2.Joint.GetAnchorA
function b2.Joint:GetAnchorAXY()
  _Joint_GetAnchorA(self, _vec)
  return _vec.x, _vec.y
end

local _Joint_GetAnchorB = b2.Joint.GetAnchorB
function b2.Joint:GetAnchorBXY()
  _Joint_GetAnchorB(self, _vec)
  return _vec.x, _vec.y
end

--[[
local _Joint_GetLocalAxisA = b2.PrismaticJoint.GetLocalAxisA
function b2.Joint:GetLocalAxisAXY()
  _Joint_GetLocalAxisA(self, _vec)
  return _vec.x, _vec.y
end
]]

local _Joint_SetTarget = b2.MouseJoint.SetTarget
function b2.Joint:SetTargetXY(x, y)
  _Vec2_Set(_vec, x, y)
  _Joint_SetTarget(self, _vec)
end

local _Joint_ShiftOrigin = b2.Joint.ShiftOrigin
function b2.Joint:ShiftOriginXY(x, y)
  _Vec2_Set(_vec, x, y)
  _Joint_ShiftOrigin(self, _vec)
end

-- World
local _World_SetGravity = b2.World.SetGravity
function b2.World:SetGravityXY(x, y)
  _Vec2_Set(_pt1, x, y)
  _World_SetGravity(self, _pt1)
end

function b2.World:Destroy()
  -- destroy gear joints first
  local j = self:GetJointList()
  while j do
    local nj = j:GetNext()
    if j:GetType() == "gearJoint" then
      self:DestroyJoint(j)
    end
    j = nj
  end
  -- destroy non-gear joints
  local j = self:GetJointList()
  while j do
    local nj = j:GetNext()
    self:DestroyJoint(j)
    j = nj
  end
  -- destroy bodies
  local b = self:GetBodyList()
  while b do
    local nb = b:GetNext()
    self:DestroyBody(b)
    b = nb
  end
end