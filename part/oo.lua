local oo = {}

local meta = {}
local rmeta = {}
--local parent = {}

-- Creates a new class
function oo.class()
  local c = {}
  local mt = { __index = c }
  meta[c] = mt
  rmeta[mt] = c
  return c
end

-- Creates a subclass
function oo.subclass(p)
  assert(meta[p], "undefined parent class")
  local c = oo.class()
  --parent[c] = p
  return setmetatable(c, meta[p])
end

-- Creates a new instance
function oo.instance(c)
  assert(meta[c], "undefined class")
  return setmetatable({}, meta[c])
end

-- Gets the class of an instance
function oo.getclass(i)
  local mt = getmetatable(i)
  return rmeta[mt]
end

--[[
-- Inheritance:

local oo = require 'oo'

local Fruit = oo.class()
Fruit.sweetness_threshold = 5 -- sort of like "static"
function Fruit:initialize(sweetness)
  self.sweetness = sweetness
end
function Fruit:isSweet()
  return self.sweetness > Fruit.sweetness_threshold
end

local Lemon = oo.subclass(Fruit) -- subclassing
function Lemon:initialize()
  Fruit.initialize(self, 1) -- manually invoking the superclass' initializer
end

local lemon = oo.instance(Lemon)
lemon:initialize()
print(lemon:isSweet()) -- false
]]

return oo