local non_staggered =
{
  n = { 0, 1 }, ne = { 1, 1 }, e = { 1, 0 }, se = { 1, -1 },
  s = { 0, -1 }, sw = { -1, -1 }, w = { -1, 0 }, nw = { -1, 1 }
}

local staggered_odd =
{
  n = { 0, 2 }, ne = { 1, 1 }, e = { 1, 0 }, se = { 1, -1 },
  s = { 0, -2 }, sw = { 0, -1 }, w = { -1, 0 }, nw = { 0, 1 }
}

local staggered_even =
{
  n = { 0, 2 }, ne = { 0, 1 }, e = { 1, 0 }, se = { 0, -1 },
  s = { 0, -2 }, sw = { -1, -1 }, w = { -1, 0 }, nw = { -1, 1 }
}

local Grid = {}
local GridMT = { __index = Grid }

Grid.create = function(self, w, h, order)
  local self = {}
  setmetatable(self, GridMT)

  -- dimensions of the Grid
  self.w = w
  self.h = h
  self.order = order
  -- warp
  self.warpx = false
  self.warpy = false

  return self
end

Grid.destroy = function(self)
  self.w = nil
  self.h = nil
  self.order = nil
  self.wrapx = nil
  self.wrapy = nil
end

-- dimensions accessors
Grid.get_size = function(self)
  return self.w*self.h
end

Grid.get_dimensions = function(self)
  return self.w, self.h
end

Grid.set_dimensions = function(self, w, h)
  self.w = w
  self.h = h
end

Grid.get_adjacent_tile = function(self, tx, ty, d)
  local dir
  if self.order == 'oblique' then
    dir = non_staggered
  elseif self.order == 'diamond' then
    dir = non_staggered
  elseif self.order == 'stagger' then
    if ty%2 == 1 then
      dir = staggered_odd
    else
      dir = staggered_even
    end
  end
  local dx, dy = dir[1], dir[2]
  return tx + dx, ty + dy
end

Grid.get_adjacent_dir = function(self, tx, ty, tx2, ty2)
  local dir
  if self.order == 'oblique' then
    dir = non_staggered
  elseif self.order == 'diamond' then
    dir = non_staggered
  elseif self.order == 'stagger' then
    if ty%2 == 1 then
      dir = staggered_odd
    else
      dir = staggered_even
    end
  end
  local dx, dy = tx2 - tx, ty2 - ty
  -- todo wrap x and make constant time
  for i, v in pairs(dir) do
    if v[1] == dx and v[2] == dy then
      return i
    end
  end
  local sz = string.format("tiles are not adjacent: %d,%d and %d,%d", tx,ty, t2,y2)
  assert(false, sz)
end

-- converts node index to tile
Grid.index_to_tile = function(self, n)
  n = n - 1
  local x = n%self.w
  local y = (n - x)/self.w
  x, y = x + 1, y + 1
  if x < 1 or x > self.w or y < 1 or y > self.h then
    return
  end
  return x, y
end

-- converts tile to node index
Grid.tile_to_index = function(self, x, y)
  -- wrap
  if self.warpx then
    x = (x - 1)%self.w + 1
  elseif x < 1 or x > self.w then
    return
  end
  if self.warpy then
    y = (y - 1)%self.h + 1
  elseif y < 1 or y > self.h then
    return
  end
  return (y - 1)*self.w + x
end

-- converts tile to position
Grid.tile_to_pos = function(self, tx, ty, tw, th)
  -- make sure tile index starts from 0
  local ltx = tx - 1
  local lty = ty - 1  
  local x, y
  if self.order == 'oblique' then
    -- oblique ordering
    x = ltx*tw
    y = lty*th
  elseif self.order == 'stagger' then
    -- staggered ordering
    x = ltx*tw + ty%2*(tw/2)
    y = lty*(th/2)
  elseif self.order == 'diamond' then
    -- diamond ordering
    x = (ltx + lty)*(tw/2)
    y = (lty - ltx)*(th/2)
  end
  return x, y
end

Grid.tile_corners = function(self, tx, ty, tw, th)
  local x, y = self:tile_to_pos(tx, ty, tw, th)
  local l, t = x - tw/2, y - th/2
  local r, b = x + tw/2, y + th/2
  if self.order == 'oblique' then
    return l,t, r,t, r,b, l,b
  elseif self.order == 'stagger' or self.order == 'diamond' then
    return l,y, x,t, r,y, x,b
  end
end

-- converts position to tile
Grid.pos_to_tile = function(self, wx, wy, tw, th)
  local x, y
  if self.order == 'oblique' then
    -- oblique ordering
    x = math.ceil((wx + tw/2)/tw)
    y = math.ceil((wy + th/2)/th)
  elseif self.order == 'stagger' then
    -- staggered ordering (nees work!)
    local ty = -wy - wx/2 - th
    local tx = wx + ty
    ty = math.ceil(-ty/(tw/2))
    tx = math.ceil(tx/(tw/2)) + 1
    -- major todo here:
    x = math.floor((tx + ty)/2)
    y = (ty - tx) -- + 3
  elseif self.order == 'diamond' then
    -- diamond ordering
    local ty = -wy - wx/2 - th/2
    local tx = wx + ty
    x = math.ceil(tx/(tw/2)) + 1
    y = math.ceil(-ty/(tw/2))
  end
  return x, y
end

return Grid