--- Simple states manager, using a stack

local states = {}

local _top = nil
local _defined = {}
local _stack = {}

--- Defines or returns a state
-- @param n State name
-- @param s State object (optional)
-- @return State object
function states.state(n, s)
  assert(type(n) == "string", "state name must be a string")
  local s = _defined[n]
  if s then
    return s
  end
  assert(type(s) == "table", "state object must be a table")
  _defined[n] = s
  return s
end

--- Pushes a state on the stack
-- @param s State object
-- @param ... Arguments passed to "state.enter"
-- @return State object
function states.push(s, ...)
  assert(type(s) == "table", "state object must be a table")
  -- add to stack
  _stack[#_stack + 1] = s
  _top = s
  -- call "state.enter"
  if type(s.enter) == "function" then
    s.enter(...)
  end
  return s
end

--- Pushes a defined state on the stack
-- @param n State name
-- @param ... Arguments passed to "state.enter"
-- @return State object
function states.pushn(n, ...)
  assert(type(n) == "string", "state name must be a string")
  local s = _defined[n]
  if s == nil then
    error("undefined state: " .. n)
  end
  return states.push(s, ...)
end

--- Requires a state from file and pushes it on the stack
-- @param fn Filename
-- @param ... Arguments passed to "state.enter"
-- @return State
function states.pushr(fn, ...)
  local s = _defined[fn]
  -- require if undefined
  if s == nil then
    s = require(fn)
    if type(s) ~= "table" then
      error("state file must return a table: " .. fn)
    end
    _defined[fn] = s
  end
  return states.push(s, ...)
end

function states.top()
  return _top
end

--- Pops the last state from the stack
-- @param ... Arguments passed to "state.exit"
-- @return State
function states.pop(...)
  -- note: the state is removed from the stack
  -- before calling "state.exit" so that
  -- if an error occurs it's still removed
  local s = _top
  if s then
    -- remove from stack
    _stack[#_stack] = nil
    _top = _stack[#_stack]
    -- call "state.exit"
    if type(s.exit) == "function" then
      s.exit(...)
    end
  end
  return s
end

--- Calls a function in the last state on the stack
-- @param func Function name
-- @param ... Arguments
-- @return Return values of the function
function states.event(func, ...)
  assert(type(func) == "string", "function name must be a string")
  if _top then
    local f = _top[func]
    if type(f) == "function" then
      return f(...)
    end
  end
end

function states.gevent(func, ...)
  for i = #_stack, 1, -1 do
    local f = _stack[i][func]
    if type(f) == "function" then
      f(...)
    end
  end
end

return states