--
-- Coordinate system tree
--
local cst = {}

local cos = math.cos
local sin = math.sin
local insert = table.insert
local remove = table.remove

--
-- Structure
--

-- Sets a parent to the node
-- @param c node
-- @param p new parent node
function cst.set_parent(c, p)
  assert(c ~= p, "node cannot be parented by itself")
  -- remove from old parent
  if c.parent then
    for i, v in ipairs(c.parent) do
      if v == c then
        remove(c.parent, i)
        break
      end
    end
  end
  c.parent = p
  -- add to new parent
  if p ~= nil then
    insert(p, c)
  end
end

-- Gets the parent of a node
-- @param c node
-- @return parent node
function cst.get_parent(c)
  return c.parent
end

cst.create = {}
cst.destroy = {}

-- Creates a new node
-- @param p parent node
-- @param x, y position in parent coords
-- @param a angle in radians (optional)
-- @param sx, sy scale (optional)
-- @return the new node
function cst.create.node(p, x, y, a, sx, sy)
  local c = {}
  c.type = 'node'
  -- position
  c.x, c.y = x or 0, y or 0
  -- angle
  c.a = a or 0
  -- scale
  c.sx, c.sy = sx or 1, sy or 1
  cst.set_parent(c, p)
  return c
end

-- Destroys an existing node and its children
-- @param c node
function cst.destroy.node(c)
  cst.set_parent(c, nil)
  while #c > 0 do
    local l = remove(c)
    cst.destroy.node(l)
  end
  c.x, c.y = nil, nil
  c.a = nil
  c.sx, c.sy = nil, nil
  c.type = nil
end

-- Creates a new node of a given type
-- @param t node type
-- @param r root
function cst.add(t, r, ...)
  return cst.create[t](r, ...)
end

-- Destroys an existing node
-- @param c node
function cst.remove(c, ...)
  cst.destroy[c.type](c, ...)
end

--
-- Transformations
--

-- Gets the position of a node
-- @param c node
-- @return local position
function cst.get_position(c)
  return c.x, c.y
end

-- Sets the position of a node
-- @param c node
-- @param x, y local position
function cst.set_position(c, x, y)
  c.x, c.y = x, y
end

-- Gets the position of a node
-- @param c node
-- @return position in world coords
function cst.get_world_position(c)
  return cst.local_to_world(c, 0, 0)
end

-- Sets the world position of a node
-- @param c node
-- @param x, y position in world coords
function cst.set_world_position(c, x, y)
  if c.parent then
    x, y = cst.world_to_local(c.parent, x, y)
  end
  cst.set_position(c, x, y)
end

-- Gets the scale of a node
-- @param c node
-- @return scale
function cst.get_scale(c)
  return c.sx, c.sy
end

-- Sets the scale of a node
-- @param c node
-- @param sx, sy scale
function cst.set_scale(c, sx, sy)
  c.sx, c.sy = sx, sy
end

-- Gets the angle of a node
-- @param c node
-- @return angle in radians
function cst.get_angle(c)
  return c.a
end

-- Sets the angle of a node
-- @param c node
-- @param a angle in radians
function cst.set_angle(c, a)
  c.a = a
end

-- Gets the transform of a node
-- @param c node
-- @return local position, angle and scale
function cst.get_transform(c)
  return c.x, c.y, c.a, c.sx, s.sy
end

-- Gets the transform of a node
-- @param c node
-- @return world position, angle and scale
function cst.get_world_transform(c)
  local wx, wy = cst.get_world_position(c)
  local a = c.a
  local sx, sy = c.sx, c.sy
  local p = c.parent
  while p do
    a = a + p.a
    sx, sy = sx*p.sx, sy*p.sy
    p = p.parent
  end
  return wx, wy, a, sx, sy
end

-- Transforms point to local coords
-- @param c node
-- @param x, y point in parent coords
-- @return point in local coords
function cst.parent_to_local(c, x, y)
  local px, py = x, y
  -- translate
  px, py = px - c.x, py - c.y
  -- rotate
  local ca = cos(c.a)
  local sa = sin(c.a)
  local tx = ca*px - sa*py
  local ty = sa*px + ca*py
  px, py = tx, ty
  -- scale
  -- todo: possible division by zero
  px, py = px/c.sx, py/c.sy
  return px, py
end

-- Transforms point to parent coords
-- @param c node
-- @param x, y point in local coords
-- @return point in parent coords
function cst.local_to_parent(c, x, y)
  local px, py = x, y
  -- scale
  px, py = px*c.sx, py*c.sy
  -- rotate
  local ca = cos(-c.a)
  local sa = sin(-c.a)
  local tx = ca*px - sa*py
  local ty = sa*px + ca*py
  px, py = tx, ty
  -- translate
  px, py = px + c.x, py + c.y
  return px, py
end

-- Transforms point to world coords
-- @param c node
-- @param x, y point in local coords
-- @return point in world coords
function cst.local_to_world(c, x, y)
  x, y = cst.local_to_parent(c, x, y)
  if c.parent then
    x, y = cst.local_to_world(c.parent, x, y)
  end
  return x, y
end

-- Transforms point to local coords
-- @param c node
-- @param x, y point in world coords
-- @return point in local coords
function cst.world_to_local(c, x, y)
  if c.parent then
    x, y = cst.world_to_local(c.parent, x, y)
  end
  x, y = cst.parent_to_local(c, x, y)
  return x, y
end

--
-- Point queries
--

cst.test = {}

-- Tests point against a node
-- @param x, y local point
-- @return true
function cst.test.node(c, x, y)
  return true
end

-- Checks if a point is inside the node
-- @param c node
-- @param x, y point in local coords
function cst.test_point(c, x, y)
  return cst.test[c.type](c, x, y)
end

-- Checks if a point is inside the node
-- @param c node
-- @param x, y point in world coords
function cst.test_world_point(c, x, y)
  x, y = cst.world_to_local(c, x, y)
  return cst.test_point(x, y)
end

-- Queries a point (depth first)
-- @param c node
-- @param x, y point in local coords
-- @param q result table (optional)
-- @param return last added element or result table
function cst.query_point(c, x, y, q)
  if cst.test_point(c, x, y) == false then
    return
  end
  if q then
    insert(q, c)
  end
  -- reverse iteration returns the last added elements first
  for i = #c, 1, -1 do
    local v = c[i]
    local lx, ly = cst.parent_to_local(v, x, y)
    local r = cst.query_point(v, lx, ly, q)
    if r and q == nil then
      return r
    end
  end
  return c
end

-- Queries a point
-- @param c node
-- @param x, y point in local coords
-- @param q result table (optional)
-- @param return first detected element or result table
function cst.query_world_point(c, x, y, q)
  x, y = cst.world_to_local(c, x, y)
  return cst.query_point(c, x, y, q)
end

--
-- Geometric shapes
--

-- Creates a new rect
-- @param p parent node
-- @param x, y position in parent coords
-- @param hw, hw half-width and height extents
-- @param a angle in radians (optional)
-- @param sx, sy scale (optional)
-- @param ox, oy origin (optional)
-- @return the new node
function cst.create.rect(p, x, y, hw, hh, a, sx, sy, ox, oy)
  local c = cst.create.node(p, x, y, a, sx, sy)
  c.type = 'rect'
  -- extents
  c.hw, c.hh = hw, hh
  -- origin
  c.ox, c.oy = ox or 0, oy or 0
  return c
end

-- Destroys an existing rect
-- @param c node
function cst.destroy.rect(c)
  c.hw, c.hh = nil, nil
  cst.destroy.node(c)
end

-- Tests point against a rect
-- @param x, y local point
-- @return true
function cst.test.rect(c, x, y)
  x, y = x - c.ox, y - c.oy
  if x <= -c.hw or x >= c.hw then
    return false
  end
  if y <= -c.hh or y >= c.hh then
    return false
  end
  return true
end

-- Creates a new ellipse
-- @param p parent node
-- @param x, y position in parent coords
-- @param rx, ry horizontal and vertical radius
-- @param a angle in radians (optional)
-- @param sx, sy scale (optional)
-- @param ox, oy origin (optional)
-- @return the new node
function cst.create.ellipse(p, x, y, rx, ry, a, sx, sy, ox, oy)
  local c = cst.create.node(p, x, y, a, sx, sy)
  c.type = 'ellipse'
  -- extents
  c.hw, c.hh = rx, ry
  -- origin
  c.ox, c.oy = ox or 0, oy or 0
  return c
end

-- Destroys an existing rect
-- @param c node
function cst.destroy.ellipse(c)
  c.hw, c.hh = nil, nil
  cst.destroy.node(c)
end

-- Tests point against an ellipse
-- @param x, y local point
-- @return true
function cst.test.ellipse(c, x, y)
  x, y = x - c.ox, y - c.oy
  local x2 = x*x
  local y2 = y*y
  local ex = c.hw*c.hw
  local ey = c.hh*c.hh
  if ex == 0 or ey == 0 then
    return
  end
  return x2/ex + y2/ey < 1
end

return cst