-- locale pluralization rules
-- adapted from kikito: https://github.com/kikito/i18n.lua

local floor = math.floor
local gmatch = string.gmatch

local function integer(n)
  return n == floor(n)
end
local function between(v, lo, hi)
  return v >= lo and v <= hi
end

local rules = {}

local f1 =
[[af asa bem bez bg bn brx ca cgg chr da de dv ee
el en eo es et eu fi fo fur fy gl gsw gu ha haw he
is it jmc kaj kcg kk kl ksb ku lb lg mas ml mn mr
nah nb nd ne nl nn no nr ny nyn om or pa pap ps pt
rm rof rwk saq seh sn so sq ss ssy st sv sw syr ta
te teo tig tk tn ts ur ve vun wae xh xog zu]]
rules[f1] = function(n)
  if n == 1 then
    return 'one'
  end
  return 'other'
end

local f2 =
[[ak am bh fil guw hi ln mg nso ti tl wa]]
rules[f2] = function(n)
  if n == 0 or n == 1 then
    return 'one'
  end
  return 'other'
end

local f3 =
[[ar]]
rules[f3] = function(n)
  if not integer(n) then
    return 'other'
  end
  if n == 0 then
    return 'zero'
  elseif n == 1 then
    return 'one'
  elseif n == 2 then
    return 'two'
  end
  local n100 = n%100
  if between(n100, 3, 10) then
    return 'few'
  elseif between(n100, 11, 99) then
    return 'many'
  end
  return 'other'
end

local f4 =
[[az bm bo dz fa hu id ig ii ja jv ka kde kea km
kn ko lo ms my root sah ses sg th to tr vi wo yo zh]]
rules[f4] = function(n)
  return 'other'
end

local f5 =
[[be bs hr ru sh sr uk]]
rules[f5] = function(n)
  if not integer(n) then
    return 'other'
  end
  local n10, n100 = n%10, n%100
  if n10 == 1 and n100 ~= 11 then
    return 'one'
  elseif between(n10, 2, 4) and not between(n100, 12, 14) then
    return 'few'
  elseif n10 == 0 or between(n10, 5, 9) or between(n100, 11, 14) then
    return 'many'
  end
  return 'other'
end

local f6 =
[[br]]
rules[f6] = function(n)
  if not integer(n) then
    return 'other'
  end
  local n10, n100 = n%10, n%100
  if n10 == 1 and n100 ~= 11 and n100 ~= 71 and n100 ~= 91 then
    return 'one'
  end
  if n10 == 2 and n100 ~= 12 and n100 ~= 72 and n100 ~= 92 then
    return 'two'
  end
  if n10 ~= 3 and n10 ~= 4 and n10 ~= 9 and not between(n100, 10, 19) and not between(n100, 70, 79) and not between(n100, 90, 99) then
    return 'few'
  end
  if n ~= 0 and n%1000000 == 0 then
    return 'many'
  end
  return 'other'
end

local f7 =
[[cz sk]]
rules[f7] = function(n)
  if n == 1 then
    return 'one'
  elseif n == 2 or n == 3 or n == 4 then
    return 'few'
  end
  return 'other'
end

local f8 =
[[cy]]
rules[f8] = function(n)
  if n == 0 then
    return 'zero'
  elseif n == 1 then
    return 'one'
  elseif n == 2 then
    return 'two' 
  elseif n == 3 then
    return 'few'
  elseif n == 6 then
    return 'many'
  end
  return 'other'
end

local f9 =
[[ff fr hy kab]]
rules[f9] = function(n)
  if n >= 0 and n < 2 then
    return 'one'
  end
  return 'other'
end

local f10 =
[[ga]]
rules[f10] = function(n)
  if n == 1 then
    return 'one'
  elseif n == 2 then
    return 'two'
  elseif n == 3 or n == 4 or n == 5 or n == 6 then
    return 'few'
  elseif n == 7 or n == 8 or n == 9 or n == 10 then
    return 'many'
  end
  return 'other'
end

local f11 =
[[gd]]
rules[f11] = function(n)
  if n == 1 or n == 11 then
    return 'one'
  elseif n == 2 or n == 12 then
    return 'two'
  end
  if integer(n) and (between(n, 3, 10) or between(n, 13, 19)) then
    return 'few'
  end
  return 'other'
end

local f12 =
[[gv]]
rules[f12] = function(n)
  local n10 = n%10
  if n10 == 1 or n10 == 2 or n%20 == 0 then
    return 'one'
  end
  return 'other'
end

local f13 =
[[iu kw naq se sma smi smj smn sms]]
rules[f13] = function(n)
  if n == 1 then
    return 'one'
  elseif n == 2 then
    return 'two'
  end
  return 'other'
end

local f14 =
[[ksh]]
rules[f14] = function(n)
  if n == 0 then
    return 'zero'
  elseif n == 1 then
    return 'one'
  end
  return 'other'
end

local f15 =
[[lag]]
rules[f15] = function(n)
  if n == 0 then
    return 'zero'
  elseif n > 0 and n < 2 then
    return 'one'
  end
  return 'other'
end

local f16 =
[[lt]]
rules[f16] = function(n)
  if not integer(n) then
    return 'other'
  end
  local n100 = n%100
  if between(n100, 11, 19) then
    return 'other'
  end
  local n10 = n%10
  if n10 == 1 then
    return 'one'
  end
  if between(n10, 2, 9) then
    return 'few'
  end
  return 'other'
end

local f17 =
[[lv]]
rules[f17] = function(n)
  if n == 0 then
    return 'zero'
  elseif n%10 == 1 and n%100 ~= 11 then
    return 'one'
  end
  return 'other'
end

local f18 =
[[mk]]
rules[f18] = function(n)
  if n%10 == 1 and n ~= 11 then
    return 'one'
  end
  return 'other'
end

local f19 =
[[mo ro]]
rules[f19] = function(n)
  if n == 1 then
    return 'one'
  elseif n == 0 or (n ~= 1 and integer(n) and between(n%100, 1, 19)) then
    return 'few'
  end
  return 'other'
end

local f20 =
[[mt]]
rules[f20] = function(n)
  if n == 1 then
    return 'one'
  end
  if not integer(n) then
    return 'other'
  end
  local n100 = n%100
  if n == 0 or between(n100, 2, 10) then
    return 'few'
  elseif between(n100, 11, 19) then
    return 'many'
  end
  return 'other'
end

local f21 =
[[cy]]
rules[f21] = function(n)
  if n == 1 then
    return 'one'
  end
  if not integer(n) then
    return 'other'
  end
  local n10, n100 = n%10, n%100
  if between(n10, 2, 4) and not between(n100, 12, 14) then
    return 'few'
  elseif n10 == 0 or n10 == 1 or between(n10, 5, 9) or between(n100, 12, 14) then
    return 'many'
  end
  return 'other'
end

local f22 =
[[shi]]
rules[f22] = function(n)
  if n == 0 or n == 1 then
    return 'one'
  end
  return 'other'
end

local f23 =
[[sl]]
rules[f23] = function(n)
  local n100 = n%100
  if n100 == 1 then
    return 'one'
  elseif n100 == 2 then
    return 'two'
  elseif n100 == 3 or n100 == 4 then
    return 'few'
  end
  return 'other'
end

local f24 =
[[tzm]]
rules[f24] = function(n)
  if integer(n) and (n == 0 or n == 1 or between(n, 11, 99)) then
    return 'one'
  end
  return 'other'
end

-- lookup table
local r = {}
for l, f in pairs(rules) do
  for l2 in gmatch(l, "([%w_]+)") do
    r[l2] = f
  end
end

--- Returns the plural form
--- ex: "one", "two", "few", "many", "other"
-- @param l locale
-- @param n number
-- @return string
return function(l, n)
  local f = r[l]
  if f then
    return f(n)
  end
end