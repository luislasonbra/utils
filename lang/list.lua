local gsub = string.gsub

local _args = { "{0}","{1}","{2}","{3}","{4}","{5}" }
local function _format(sz, ...)
  local args = {...}
  for i = 1, #args do
    sz = gsub(sz, _args[i], args[i])
  end
  return sz
end

--- Formats a list of items
--- ex: "1 and 2", "1, 2, and 3"
-- @param l locale
-- @param items table of items
-- @return string
local _data = require("utils.lang.data.list")
return function(l, items)
  local _l = _data[l]
  if _l then
    local n = #items
    if n == 1 then
      -- one item
      return items[1]
    elseif n == 2 then
      -- two items
      return _format(_l.t, items[1], items[2])
    elseif n > 2 then
      -- many items
      -- start
      local sz = _format(_l.s, items[1], items[2])
      -- middle
      for i = 3, n - 1 do
        sz = _format(_l.m, sz, items[i])
      end
      -- end
      return _format(_l.e, sz, items[#items])
    end
  end
end