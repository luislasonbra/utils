local floor = math.floor
local sub = string.sub
local gsub = string.gsub
local lower = string.lower

--- Dependencies
-- Kikito pluralization
local _plural = require("utils.lang.plural")
-- Unicode relative date units, ex: "1 year ago"
local _data = require("utils.lang.data.period")

-- units
local _seconds =
{
  s = 1,
  m = 60,
  h = 60*60,
  d = 60*60*24,
  w = 60*60*24*7,
  M = 60*60*24*30,
  y = 60*60*24*365
}

local _args = { "{0}", "{1}", "{2}", "{3}", "{4}", "{5}" }
local function _format(sz, ...)
  local args = {...}
  for i = 1, #args do
    sz = gsub(sz, _args[i], args[i])
  end
  return sz
end

--- Time period from seconds to years in the past or future
--- ex: "5 years ago", "2 hours ago", "in 5 minutes", "in 3 weeks"
-- @param l locale
-- @param t time difference in seconds, could be negative
-- @param u display unit: "second", "hour", "minute", "hour", "day", "week", "month" or "year"
-- @return string
return function(l, t, u)
  if _data[l] then
    -- future or past
    local l2 = 'f'
    if t < 0 then
      l2 = 'p'
      t = -t
    end
    if _data[l][l2] then
      -- display units
      local l3 = u
      if l3 then
        local _l3 = lower(l3)
        if l3 == 'M' or sub(_l3, 1, 2) == 'mo' then
          l3 = 'M'
        else
          l3 = sub(_l3, 1, 1)
        end
      end
      if l3 == nil or _seconds[l3] == nil then
        if t < _seconds.m then
          l3 = "s"
        elseif t < _seconds.h then
          l3 = "m"
        elseif t < _seconds.d then
          l3 = "h"
        elseif t < _seconds.M then
          l3 = "d"
        elseif t < _seconds.y then
          l3 = "M"
        else
          l3 = "y"
        end
      end
      if _data[l][l2][l3] then
        -- pluralization
        local n = floor(t/_seconds[l3])
        local l4 = _plural(l, n) or "other"
        if _data[l][l2][l3][l4] then
          return _format(_data[l][l2][l3][l4], n)
        end
      end
    end
  end
end