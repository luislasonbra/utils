-- first day of the week based on territory
-- takes about 14 kilobytes of memory when loaded
local first = {}

first["mon"] =
[[001 ad ai al am an at ax az ba be bg bm bn ch cl cm cr
cy cz de dk ec ee es fi fj fo fr gb ge gf gp gr hr hu is
it kg kz lb li lk lt lu lv mc md me mk mn mq my nl no pl
pt re ro rs ru se si sk sm tj tm tr ua uy uz va vn xk]]

first["fri"] =
[[bd mv]]

first["sat"] =
[[ae af bh dj dz eg iq ir jo kw ly ma om qa sd sy]]

first["sun"] =
[[ag ar as au br bs bt bw by bz ca cn co dm do et gt gu
hk hn id ie il in jm jp ke kh kr la mh mm mo mt mx mz ni
np nz pa pe ph pk pr py sa sg sv th tn tt tw um us ve vi
ws ye za zw]]

local t = {}

-- lookup table
for d, l in pairs(first) do
  for l2 in string.gmatch(l, "([%w_]+)") do
    t[l2] = d
  end
end

return t