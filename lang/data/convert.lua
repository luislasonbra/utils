local eras = { year='y', month='M', week='w', day='d', hour='h', minute='m', second='s' }

function relativeTimeFields(x)
  local out = {}
  for _, field in ipairs(x) do
    if eras[field.type] then
      for _, relativeTime in ipairs(field) do
        if xml.tag(relativeTime) == "relativeTime" then
          local out2 = out[relativeTime.type] or {}
          out[relativeTime.type] = out2
          local out3 = out2[field.type] or {}
          out2[field.type] = out3
          for i, relativeTimePattern in ipairs(relativeTime) do
            out3[relativeTimePattern.count] = relativeTimePattern[1]
          end
        end
      end
    end
  end
  return out
end

function relativeTimeFields2(x)
  local out = {}
  for _, field in ipairs(x) do
    if eras[field.type] then
      for _, relativeTime in ipairs(field) do
        if xml.tag(relativeTime) == "relative" then
          local n = relativeTime.type
          n = tonumber(n)
          local out2 = out[field.type] or {}
          out[field.type] = out2
          out2[n] = relativeTime[1]
--[[
          local n = relativeTime.type
          n = tonumber(n)
          local t = "present"
          if n < 0 then
            t = "past"
          elseif n > 0 then
            t = "future"
          end
          local out2 = out[t] or {}
          out[t] = out2
          if t == "present" then
            out2[field.type] = relativeTime[1]
          else
            local out3 = out2[field.type] or {}
            out2[field.type] = out3            
            n = math.abs(n)
            out3[n] = relativeTime[1]
          end
]]
        end
      end
    end
  end
  return out
end

function monthNames(x, f, w)
  local out = {}
  for _, monthContext in ipairs(x) do
    if monthContext.type == f then
      for _, monthWidth in ipairs(monthContext) do
        for _, month in ipairs(monthWidth) do
          local n = month.type
          n = tonumber(n)
          if w == nil then
            local out2 = out[monthWidth.type] or {}
            out[monthWidth.type] = out2
            out2[n] = month[1]
          elseif monthWidth.type == w then
            out[n] = month[1]
          end
        end
      end
    end
  end
  return out
end

local dayn = { sun = 1, mon = 2, tue = 3, wed = 4, thu = 5, fri = 6, sat = 7 }

function dayNames(x, f, w)
  local out = {}
  for _, dayContext in ipairs(x) do
    if dayContext.type == f then
      for _, dayWidth in ipairs(dayContext) do
        for _, day in ipairs(dayWidth) do
          if w == nil then
            local out2 = out[dayWidth.type] or {}
            out[dayWidth.type] = out2
            out2[dayn[day.type] ] = day[1]
          elseif dayWidth.type == w then
            out[dayn[day.type] ] = day[1]
          end
        end
      end
    end
  end
  return out
end

function dayPeriods(x, f, w)
  local out = {}
  for _, dayPeriodContext in ipairs(x) do
    if dayPeriodContext.type == f then
      for _, dayPeriodWidth in ipairs(dayPeriodContext) do
        for _, dayPeriod in ipairs(dayPeriodWidth) do
          if w == nil then
            local out2 = out[dayPeriodWidth.type] or {}
            out[dayPeriodWidth.type] = out2
            out2[dayPeriod.type] = dayPeriod[1]
          elseif dayPeriodWidth.type == w then
            out[dayPeriod.type] = dayPeriod[1]
          end
        end
      end
    end
  end
  return out
end

local parts = { ["2"] = "t", ["start"] = "s", ["end"] = "e", ["middle"] = "m" }

function listPatterns(x)
  local out = {}
  for _, listPattern in ipairs(x) do
    if listPattern.type == nil then
      for _, listPatternPart in ipairs(listPattern) do
        local t = listPatternPart.type or _
        t = parts[t] or t
        out[t] = listPatternPart[1]
      end
    end
  end
  return out
end

require("lfs")
require("LuaXml")
require("utils.io.table")

local f = io.open("out.txt", "w")

for file in lfs.dir("utils/clrd/main/") do
  if file ~= "." and file ~= ".." then
    local x = xml.load("utils/clrd/main/" .. file)

    local locale = string.match(file, "(.*)%..*$")
    locale = string.lower(locale)
    
    --[[
    local fields = xml.find(x, "fields")
    if fields then
      local o = relativeTimeFields(fields)
      local c = 0
      for _ in pairs(o) do
        c = c + 1
      end
      if c > 0 then
        f:write('["')
        f:write(locale)
        f:write('"] = ')
        f:write(table.tostring(o))
        f:write(',\n')
      end
    end
    ]]
    --[[
    local patterns = xml.find(x, "listPatterns")
    if patterns then
      local o = listPatterns(patterns)
      local c = 0
      for _ in pairs(o) do
        c = c + 1
      end
      if c > 0 then
        f:write('["')
        f:write(locale)
        f:write('"] = ')
        f:write(table.tostring(o))
        f:write(',\n')
      end
    end
]]

    local c = xml.find(x, "calendars")
    if c then
      for _, calendar in ipairs(c) do
        if calendar.type == "gregorian" then
          local t = xml.find(x, "dayPeriods")
          if t then
            local o = dayPeriods(t, 'format', 'abbreviated')
            local c = 0
            for _ in pairs(o) do
              c = c + 1
            end
            if c > 0 then
              f:write('["')
              f:write(locale)
              f:write('"] = ')
              f:write(table.tostring(o))
              f:write(',\n')
            end
          end
        end
      end
    end
  end
end