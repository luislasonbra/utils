local floor = math.floor

local c = {}

--- Validates a Gregorian calendar date
-- @param y year (4 digits)
-- @param m month (1-12)
-- @param d day (1-31)
function c.validate(y, m, d)
  if d < 1 or d > 31 or m < 1 or m > 12 then
    -- month or day out of range
    return false
  end
  if m == 4 or m == 6 or m == 9 or m == 11 then 
    -- April, June, September, November
    -- can have at most 30 days
    return d <= 30
  elseif m == 2 then
    -- February
    if y%400 == 0 or (y%100 ~= 0 and y%4 == 0) then
      -- leap year
      return d <= 29
    else
      -- non-leap year
      return d <= 28
    end
  end
  -- all other months can have at most 31 days
  return true
end

--- Gregorian calendar date to Julian
-- Julian days begin on January 1, 4713 BCE noon Universal Time
-- the most recent year beginning with a full moon
-- @y year (4 digits)
-- @m month (1-12)
-- @d day (1-31)
function c.jdn(Y, M, D, h, m, s)
  if not c.validate(Y, M, D) then
    error(Y .. '/' .. M .. '/' .. D)
  end
  --[[
  if M < 3 then
    M = M + 12
    Y = Y - 1
  end
  local a = floor((153*M - 457)/5)
  local b = floor(Y/4)
  local c = floor(Y/100)
  local e = floor(Y/400)
  return D + a + 365*Y + b - c + e + 1721118.5
  ]]

  h = h or 12
  m = m or 0
  s = s or 0
  
  local _A = floor((14 - M)/12)
  local _Y = Y + 4800 - _A
  local _M = M + 12*_A - 3
  
  local _1 = floor((153*_M + 2)/5)
  local _2 = floor(_Y/4)
  local _3 = floor(_Y/100)
  local _4 = floor(_Y/400)
  
  local JDN = D + _1 + 365*_Y + _2 - _3 + _4 - 32045
  return JDN + (h - 12)/24 + m/1440 + s/86400
end

--- Julian calendar date to Gregorian
-- @JDN Julian day number
-- @return year, month and date
function c.gd(JDN)
  local _f = JDN + 1401
  _f = _f + floor((4*JDN + 274277)/146097*3/4) - 38
  local _e = 4*_f + 3
  local _g = _e%1461/4
  local _h = 5*_g + 2
  local D = _h%153/5 + 1
  local M = (_h/153 + 2)%12 + 1
  local Y = _e/1461 - 4716 + (12 + 2 - M)/12
  return Y, M, D
end

return c