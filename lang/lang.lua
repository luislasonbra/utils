-- C-library
unicode = unicode or require("unicode")
local utf8 = unicode.utf8
-- Lua implementation
--local utf8 = require("utils.lang.utf8")

local text = {}
local locale = nil
local mappings = {}

local lang = {}

-- Gets the current locale
-- @return Current locale name or nil
function lang.get_locale()
  return locale
end

-- Sets the current locale
-- @param l Locale name or nil
-- @return True if available or false otherwise
function lang.set_locale(l)
  assert(type(l) == "string", "locale name must be a string")
  locale = l
  return lang.available(l)
end

-- Checks if the locale is available
-- @param l Locale name
-- @return True if available or false otherwise
function lang.available(l)
  assert(type(l) == "string", "locale name must be a string")
  return text[l] ~= nil
end

-- Gets the list of all available locales
-- @param out Locales list (optional)
-- @return Locales list
function lang.get_available(out)
  out = out or {}
  for n in pairs(text) do
    table.insert(out, n)
  end
  table.sort(out)
  return out
end

-- Replaces format flags ($1, $2, etc) in the string
-- @param sz Text string
-- @param ... List of arguments
function lang.format(sz, ...)
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      sz = string.gsub(sz, "$" .. i, a)
    end
  end
  return sz
end

-- Replaces characters in the string
-- @param sz Text string
-- @param map Mapping table
-- @return String
function lang.replace(sz, map)
  for i = 1, #map do
    local _p = map[i]
    sz = string.gsub(sz, _p[1], _p[2])
  end
  local sz2 = ''
  for i = 1, utf8.len(sz) do
    local c = utf8.sub(sz, i, i)
    sz2 = sz2 .. (map[c] or c)
  end
  return sz2
end

function lang.set_mapping_locale(l, map)
  mappings[l] = map
end

function lang.set_mapping(map)
  lang.set_mapping_locale(locale, map)
end

function lang.get_mapping_locale(l)
  return mappings[l]
end

function lang.get_mapping()
  return mappings[locale]
end

-- Creates a new mapping table from two strings
-- @param s Source string
-- @param d Destination string
-- @param map Mapping table (optional)
-- @return Mapping table
function lang.mapping(s, d, map)
  map = map or {}
  local l1, l2 = utf8.len(s), utf8.len(d)
  assert(l1 == l2, "mapping strings must be of equal length")
  local map = {}
  for i = 1, utf8.len(s) do
    local c1 = utf8.sub(s, i, i)
    local c2 = utf8.sub(d, i, i)
    map[c1] = c2
  end
  return map
end

-- Adds a phoneme to the mapping table
-- @param s Source string
-- @param d Destination string
-- @param map Mapping table (optional)
-- @return Mapping table
function lang.phoneme(s, d, map)
  map = map or {}
  if utf8.len(s) == 1 then
    map[s] = d
    return
  end
  local j = 1
  for i = 1, #map do
    local _s = map[i][1]
    if utf8.len(_s) >= utf8.len(d) then
      j = i
      break
    end
  end
  table.insert(map, j, {s, d})
end

-- Sets the text for a key and locale
-- @param l Locale name
-- @param k Key name
-- @param sz Text string
function lang.set_text_locale(l, k, sz)
  assert(type(k) == "string", "text key must be a string")
  assert(type(sz) == "string", "text must be string")
  if text[l] == nil then
    text[l] = {}
  end
  text[l][k] = sz
end

-- Sets the text for a key
-- @param k Key name
-- @param sz Text string
function lang.set_text(k, sz)
  lang.set_text_locale(locale, k, sz)
end

-- Gets the text for a key and locale
-- @param l Locale name
-- @param k Key name
-- @param ... Format arguments
function lang.get_text_locale(l, k, ...)
  assert(type(k) == "string", "text key must be a string")
  if text[l] == nil then
    return ""
  end
  local sz = text[l][k]
  if sz == nil then
    return ""
  end
  -- replace $1, $2, etc
  sz = lang.format(sz, ...)
  -- replace special characters
  if mappings[l] then
    sz = lang.replace(sz, mappings[l])
  end
  return sz
end

-- Gets the text for a key
-- @param k Key name
-- @param ... Format arguments
function lang.get_text(k, ...)
  return lang.get_text_locale(locale, k, ...)
end

-- Sets keys and text from a table for a locale
-- @param l Locale
-- @param t Table of keys and strings
function lang.import_locale(l, t)
  for k, v in pairs(t) do
    lang.set_text_locale(l, k, v)
  end
end

-- Sets keys and text from a table
-- @param t Table of keys and strings
function lang.import(t)
  lang.import_locale(locale, t)
end

return lang