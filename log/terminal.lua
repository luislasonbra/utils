-- commonly used functions
local remove = table.remove
local insert = table.insert
local concat = table.concat
local floor = math.floor
local find = string.find
local format = string.format
local gmatch = string.gmatch
local match = string.match
local sub = string.sub
local gsub = string.gsub
local char = string.char
local byte = string.byte
local len = string.len

-- messages
local prints = {}
-- traces
local traces = {}
-- sprite and canvas
local sprite = Sprite()
local canvas = sprite.canvas

local log = {}

-- visible flag
log.visible = true
-- modified flag
log.modified = false
-- redraw when modified flag
log.immediate = true
-- background overlay color
log.bg = Color(0, 0, 0)
-- foreground overlay color
log.fg = Color(255, 255, 255)

--- Clear all messages and traces
function log.clear()
  for i = #prints, 1, -1 do
    prints[i] = nil
  end
  for k in pairs(traces) do
    traces[k] = nil
  end
  log.modified = true
  if log.immediate then
    canvas:clear()
  end
end

--- Print one or more arguments, comma separated
-- @param sz First argument
-- @param ... Following arguments (optional)
local args = {}
function log.print(sz, ...)
  -- handle first argument
  sz = tostring(sz)
  -- handle following arguments
  local nargs = select('#', ...)
  if nargs > 0 then
    args[1] = sz
    for i = 1, nargs do
      local a = select(i, ...)
      args[i + 1] = tostring(a)
    end
    sz = concat(args, ", ")
    for i = nargs + 1, 1, -1 do
      args[i] = nil
    end
  end
  -- handle new lines
  if find(sz, "[\n]", 1, false) then
    if sub(sz, -1) ~= "\n" then
      sz = sz .. "\n"
    end
    for sz2 in gmatch(sz, "(.-)\n") do
      prints[#prints + 1] = sz2
    end
  else
    prints[#prints + 1] = sz
  end
  -- keep the log size to a reasonable size
  while #prints > 128 do
    remove(prints, 1)
  end
  log.modified = true
  if log.immediate then
    log.redraw()
  end
end

--- Print a formatted string
-- @param sz Format string
-- @param ... Format arguments
function log.printf(sz, ...)
  -- handle non-string values
  sz = tostring(sz)
  -- handle format parameters
  if select('#', ...) > 0 then
    sz = format(sz, ...)
  end
  log.print(sz)
end

--- Trace a variable
-- @param k Key
-- @param v Value
function log.trace(k, v)
  k = tostring(k)
  if v ~= nil then
    traces[k] = tostring(v)
  else
    traces[k] = nil
  end
  log.modified = true
  if log.immediate then
    log.redraw()
  end
end

function log.execute(sz)
  local r, e = loadstring(sz)
  if r then
    r, e = pcall(r)
  end
  return r, e
end

--- Executes a command
-- @param sz Lua code
-- @return Return value
local _cmds = {}
function log.command(sz)
  log.print('>' .. sz)
  if sz == '' then
    return
  end
  local r, e = log.execute(sz)
  if e ~= nil then
    log.print(e)
  end
  for i = #_cmds, 1, -1 do
    if _cmds[i] == sz then
      remove(_cmds, i)
      break
    end
  end
  _cmds[#_cmds + 1] = sz
  while #_cmds > 100 do
    remove(_cmds, 1)
  end
  return e
end

--- Input a single byte
-- @param b Ansi byte code
local _cmd = { i = 0 }
function log.inputc(b)
  if b == nil then
    return
  end
  if b == 8 or b == 127 then
    -- backspace
    --_cmd[#_cmd] = nil
    remove(_cmd, _cmd.i)
    _cmd.i = _cmd.i - 1
  elseif b == 9 then
    -- tab
    --_cmd[#_cmd + 1] = '\t'
    _cmd.i = _cmd.i + 1
    insert(_cmd, _cmd.i, '\t')
  elseif b >= 10 and b <= 13 then
    -- enter
    local sz = concat(_cmd)
    log.command(sz)
    _cmd = { i = 0 }
  elseif b == 18656 or b == 20704 then
    -- up or down
    local sz = concat(_cmd)
    local j
    for i = #_cmds, 1, -1 do
      if _cmds[i] == sz then
        j = i
        break
      end
    end
    local d = 1
    if b == 18656 then
      d = -1
    end
    j = j and (j + d) or #_cmds
    if _cmds[j] then
      _cmd = { i = 0 }
      log.input(_cmds[j])
    end
  elseif b == 19424 or b == 19936 then
    -- left or right
    local d = 1
    if b == 19424 then
      d = -1
    end
    _cmd.i = _cmd.i + d
    if _cmd.i < 0 then
      _cmd.i = 0
    elseif _cmd.i > #_cmd then
      _cmd.i = #_cmd
    end
  elseif b == 22 then
    log.input(system.get_clipboard())
  elseif b <= 126 then
    -- printable
    local c = char(b)
    if b < 32 then
      c = char(94, b + 64)
    end
    --_cmd[#_cmd + 1] = c
    _cmd.i = _cmd.i + 1
    insert(_cmd, _cmd.i, c)
  end
  log.modified = true
  if log.immediate then
    log.redraw()
  end
end

--- Input an ANSI character or string
-- @param sz String or ANSI character
function log.input(sz)
  if sz == nil then
    return
  end
  if len(sz) == 1 then
    local b = byte(sz)
    log.inputc(b)
  else
    sz = gsub(sz, "\n", "\\n")
    for c in gmatch(sz, '(.)') do
      _cmd[#_cmd + 1] = c
    end
    _cmd.i = _cmd.i + len(sz)
    log.modified = true
    if log.immediate then
      log.redraw()
    end
  end
end

--- Build all prints and traces in a string (internal)
-- @param rows Number of rows
-- @return String
local out = {}
function log.build(rows)
  -- clean up output buffer
  for i = #out, 1, -1 do
    out[i] = nil
  end
  rows = rows - 1
  local i = 1
  local j = 1
  -- traces
  -- shown on top
  for k, v in pairs(traces) do
    if j > rows then
      break
    end
    out[i] = format("%s:%s", k, v)
    i = i + 1
    local _, count = gsub(v, "\n", "")
    j = j + 1 + count
  end
  -- log messages
  -- shown below, in order of "printing"
  local i2 = #prints - rows + j
  if i2 < 1 then
    i2 = 1
  end
  for k = i2, #prints do
    if j > rows then
      break
    end
    local s = prints[k]
    out[i] = s
    i = i + 1
    local _, count = gsub(s, "\n", "")
    j = j + 1 + count
  end
  out[#out + 1] = '>' .. concat(_cmd)
  -- todo: break long lines
  return concat(out, "\n")
end

local text = ""
local font = Font()

-- font file and size
--log.font = "utils/fonts/ttf/FreeMonoBold.ttf"
log.font = "utils/fonts/fnt/FreeMonoBold.14.fnt"
log.size = 14

--- Reset log
function log.reset()
  if font.status == 'loaded' then
    canvas:clear()
    font:unload()
  end
end

--- Redraw log
local mode = DisplayMode()
function log.redraw()
  -- make sure the display is initialized
  if not display:get_mode(mode) then
    return
  end
  -- transform according to the default viewport
  local viewport = display.viewport
  if viewport == nil then
    return
  end
  viewport:add_child(sprite)
  sprite:set_scale(viewport.scalex, viewport.scaley)
  --sprite:set_rotation(viewport.rotation)
  sprite.depth = -math.huge
  -- make sure the font is loaded
  if font.status ~= "loaded" then
    log.reset()
    local path, file, ext = match(log.font, "(.-)([^\\/]-%.?([^%.\\/]*))$")
    if ext == "fnt" then
      -- remove trailing slash
      path = match(path, "(.*)/$") or path
      font:load_bmfont(log.font, path)
    else
      font:load_file(log.font, log.size)
    end
    if font.status ~= 'loaded' then
      return
    end
  end
  -- redraw
  canvas:clear()
  if not log.visible then
    return
  end
  -- figure out extents
  local width, height = mode.width, mode.height
  local fheight = font:get_height()
  local fsize = font:get_size()
  local rows = floor(height/fheight)
  local rheight = height/rows
  -- add a dark background
  canvas:rectangle(width, height)
  canvas:set_fill_style(log.bg, 0.25)
  canvas:fill()
  -- print everything in one call
  if log.modified then
    text = log.build(rows)
    log.modified = false
  end
  -- todo: handle non-loaded UTF-8 characters
  canvas:set_font(font, log.fg, 1)
  canvas:move_to(-width/2, height/2 - rheight)
  canvas:write(text)

  canvas:move_to(-width/2, height/2 - fheight*#out)
  local s = sub(">" .. concat(_cmd), 0, _cmd.i + 1)
  canvas:rel_move_to(font:get_width(s), 0)
  canvas:write("_")
end

return log