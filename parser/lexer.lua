local len = string.len
local match = string.match
local byte = string.byte
local char = string.char
local gsub = string.gsub

local insert = table.insert

local lexer = {}

-- Tries a number of patterns and returns the longest match
-- The "longest match" is not necessarily the "longest capture" but the longest lexeme
-- @param source Source string
-- @param pattern Table of token types to Lua patterns
-- @param offset Offset index
-- @param return Token type, capture and string
function lexer.match(source, patterns, offset)
  local token
  local capture
  local lexeme
  -- try each pattern and find the longest match
  for t, p in pairs(patterns) do
    -- all patterns must begin with '^'
    if byte(p, 1) ~= 94 then
      p = '^' .. p
    end
    local c = match(source, p, offset)
    if c then
      -- full lexeme, including ignored characters around captures
      local p2 = gsub(p, "([^%%])([%(%)])", "%1")
      local l = match(source, p2, offset)
      assert(l, "pattern error")
      -- todo: two or more matches of the same length
      if lexeme == nil or len(l) > len(lexeme) then
        capture = c
        token = t
        lexeme = l
      end
    end
  end
	return token, capture, lexeme
end

-- Converts source string to a list of tokens
-- @param source Source string
-- @param patterns Table of token types to Lua patterns
-- @param any Include non-captures
-- @param return List of tokens
function lexer.tokenize(source, patterns, any)
  local tokens = {}
  local offset = 1
  while offset <= len(source) do
    -- get the next lexeme
    local t, c, l = lexer.match(source, patterns, offset)
    if t == nil then
      -- no match, probably an unknown character in the stream
      local b = byte(source, offset)
      c = char(b)
      l = c
    end
    -- add new token or append to last
    local last = tokens[#tokens]
    if t or (any and (not last or last.t)) then
      insert(tokens, { t = t, c = c, l = l })
    elseif any and not last.t then
      last.l = last.l .. l
      last.c = last.l
    end
    -- advance the character stream
    offset = offset + len(l)
  end
  return tokens
end

return lexer