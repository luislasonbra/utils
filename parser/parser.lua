parser = {}

-- transitions
parser.table = {}
parser.maxerrors = 100
parser.errors = {}

f = io.open("out.txt", "w")

function parser.log(sz, ...)
  sz = string.format(sz, ...)
  f:write(sz)
  f:write("\n")
  f:flush()
end

function parser.error(sz, ...)
  sz = string.format(sz, ...)
  table.insert(parser.errors, sz)
  error(sz)
end

-- Returns true if n is terminal
function parser.terminal(g, n)
  return g[n] == nil
end

-- Returns true if n is null-deriving
function parser.null(g, n, set)
  set = set or {}
  if set[n] ~= nil then
    return set[n]
  end
  set[n] = false
  if parser.terminal(g, n) then
    if n ~= "*" then
      return false
    end
  else
    for _, p in ipairs(g[n]) do
      for _, s in ipairs(p) do
        if not parser.null(g, s, set) then
          return false
        end
      end
    end
  end
  set[n] = true
  return true
end

-- Returns the first set for n
function parser.first(g, n, set)
  set = set or {}
  if set[n] ~= nil then
    return set[n]
  end
  set[n] = {}
  if parser.terminal(g, n) then
    table.insert(set[n], n)
  else
    for _, p in ipairs(g[n]) do
      for _, s in ipairs(p) do
        local set2 = parser.first(g, s, set)
        for _, s2 in ipairs(set2) do
          table.insert(set[n], s2)
        end
        if not parser.null(g, s) then
          break
        end
      end
    end
  end
  assert(#set[n] > 0, "empty set: " .. n)
  for _, s in ipairs(set[n]) do
    assert(parser.terminal(g, s), s)
  end
  return set[n]
end

--[[
Algorithm for Generating the Follow Set
CHANGES=1
while CHANGES
	CHANGES=0
	for I = each rule
		for J = each rhs symbol
			for K = J + 1 to RHSLEN(I) + 1
				if K > RHSLEN(I) then
					add Follow(LHS(I)) to Follow(RHS(I,J))
				else
					add RHS(I,K)  to Follow(RHS(I,J))
					add FIRST(RHS(I,K)) to Follow(RHS(I,J))
					if !NULL(RHS(I,K)) then break to next J
			next K
		next J
	next I
next while
]]

function parser.follow(g, n, set)
  set = set or {}
  for I, rule in pairs(g) do
    set[I] = set[I] or {}
    for _, p in ipairs(rule) do
      for j = 1, #p do
        local J = p[j]
        set[J] = set[J] or {}
        for k = j + 1, #p + 1 do
          local K = p[k]
          assert(J ~= K)
          if k > #p then
            for _, s in ipairs(set[I]) do
              table.insert(set[J], s)
            end
          else
            set[K] = set[K] or {}
            for _, s in ipairs(set[K]) do
              table.insert(set[J], s)
            end
            --table.insert(set[J], K)
            local f = parser.first(g, K)
            for _, s in ipairs(f) do
              table.insert(set[J], s)
            end
            if not parser.null(g, K) then
              break
            end
          end
        end
      end
    end
  end
  --for _, s in ipairs(set[n]) do
    --assert(s ~= "*")
  --end
  return set[n]
end

-- symbol types: terminal, non-terminal, action
-- in LL(1) grammar
function parser.validate(g)
  assert(g, "grammar definition is nil")
  
  -- left recursion check:
  -- A => A
  for n, rule in pairs(g) do
    if #rule == 0 then
      parser.error("grammar error: '%s' (empty rule)", n)
    end
    for _, p in ipairs(rule) do
      local s = p[1]
      if s == nil then
        parser.error("grammar error: '%s' (empty production)", n)
      end
      if s == n then
        parser.error("grammar error: '%s' (left recursion not allowed)", n)
      end
    end
  end

  local symbols = {}
  for n, r in pairs(g) do
    for _, p in ipairs(r) do
      for _, s in ipairs(p) do
        symbols[s] = (g[s] == nil)
      end
    end
  end
  local terminals = {}
  local nonterminals = {}
  for s, t in pairs(symbols) do
    if t then
      table.insert(terminals, s)
    else
      table.insert(nonterminals, s)
    end
  end
  
  local nullable = {}
  local first = {}
  local follow = {}

  for _, x in ipairs(nonterminals) do
    first[x] = {}
    follow[x] = {}
  end
  for _, y in ipairs(terminals) do
    first[y] = { y }
    follow[y] = {}
  end
  
  repeat
    local changes = 0
    -- foreach grammar rule X ::= Y(1) ... Y(k)
    for n, r in pairs(g) do
      for _, x in ipairs(r) do
        local k = #x
        --if k == 0 or subset(y
      end
    end
  until changes == 0
  --[[
repeat
  foreach grammar rule X ::= Y(1) ... Y(k)
  if k=0 or {Y(1),...,Y(k)} subset of nullable then
    nullable = nullable union {X}
  for i = 1 to k
    if i=1 or {Y(1),...,Y(i-1)} subset of nullable then
      first(X) = first(X) union first(Y(i))
    for j = i+1 to k
      if i=k or {Y(i+1),...Y(k)} subset of nullable then
        follow(Y(i)) = follow(Y(i)) union follow(X)
      if i+1=j or {Y(i+1),...,Y(j-1)} subset of nullable then
        follow(Y(i)) = follow(Y(i)) union first(Y(j))
until none of nullable,first,follow changed in last iteration
]]

  
  parser.log("Terminals:")
  parser.log(table.concat(terminals, ", "))
  parser.log("Non-Terminals:")
  parser.log(table.concat(nonterminals, ", "))

  parser.log("First sets:")
  for _, n in ipairs(nonterminals) do
    parser.log("%s => %s", n, table.concat(first[n], ", "))
  end
  parser.log("Follow sets:")
  for _, n in ipairs(nonterminals) do
    parser.log("%s => %s", n, table.concat(follow[n], ", "))
  end
    
    --[[
  -- first/first conflict check
  for _, n in ipairs(ntlist) do
    local set = fset[n]
    for i = 1, #set do
      for j = i + 1, #set do
        if set[i] == set[j] then
          parser.error("grammar error: first/first conflict '%s' ('%s')", n, set[i])
        end
      end
    end
  end

  -- first/follow conflict check:
  for _, n in ipairs(ntlist) do
    local set = lset[n]
    local set2 = fset[n]
    for i = 1, #set do
      for j = 1, #set2 do
        if set[i] == set2[j] then
          --parser.error("grammar error: first/follow conflict '%s' ('%s')", n, set[i])
        end
      end
    end
  end
  ]]
end


--[[
	for a = 1, #parser.rules do
		if parser.rules[a] == nt then
      -- symbol type
      local fp = parser.tsets[a]
			if fp.t == 'terminal' then
        local i
        for j, v in ipairs(p) do
          if v == fp.p then
            i = j
            break
          end
        end
				if i == #p then
					table.insert(p, fp.p)
				else
					error("grammar error: ambiguous transition in rule " .. nt .. " and symbol " .. fp.p)
				end
			end
			if fp.t == 'nonterminal' then
				parser.first(fp.p, p)
			end
		end
	end
  ]]

--[[
function parser.assemble()
  for i = 1, #parser.rules do
    -- non-terminal type
    local rule = parser.rules[i]
    if parser.table[rule] == nil then
      parser.table[rule] = {}
    end

    -- terminal type
    local first = {}
    if parser.psets[i][1].t == 'terminal' then
      table.insert(first, parser.psets[i][1].p)
    else
      parser.first(parser.psets[i][1].p, first)
    end

  --		Log::Output("\nRULE:" + Dictionary::Find(rule))
    for j = 1, #first do
  --			Log::Output("TRANS:" + Dictionary::Find((TERMINAL_TYPE)first[j]) +" => " + IO::ToString(i + 1))
      local f = first[j]
      parser.table[rule][f] = i
    end
  end
  --Log::Output("Grammar Assembly Complete. " + String(temp_psets.size()) +" production(s) + EPSILON")
end
]]

function parser.parse(stack, tokens)
	local last_token

	--while (pop.t == 'terminal' and pop.p == 'end') == false do
  repeat
		local pop = table.remove(stack)
		local input = tokens[1]
    
		if pop.t == "action" then
    --[[
      if (pop.p > AS_DATA and pop.p < AS_NON_DATA) then
        table.insert(stack, { t = pop.p, v = last_token.v})
        break
      end
      if (pop.p > AS_UNARY and pop.p < AS_NON_UNARY or
        pop.p > AS_DELIM and pop.p < AS_NON_DELIM) then
        local unary = table.remove(stack)
        table.insert(stack, { t = pop.p, v = unary})
        break
      end
      if (pop.p > AS_BINARY and pop.p < AS_NON_BINARY) then
        local right = table.remove(stack)
        local left = table.remove(stack)
        table.insert(stack, { t = pop.p, l = left, r = right})
        break
      end
      ]]
      parser.error("semantic translation error: unknown action " .. pop.p)
      return false
		elseif pop.t == "terminal" then
      if pop.p ~= TT_EPSILON then
        if pop.p == input.n then
          last_token = table.remove(stack, 1)
        else
          parser.error("syntax error: expected terminal " .. pop.p .. " but got " .. input.n)
          return false
        end
      end
      break
		elseif pop.t == "nonterminal" then
      local pid = parser.table[pop.p][input.n]
      if pid == 0 then
        -- all transitions have failed, attempt epsilon
        pid = parser.table[pop.p][TT_EPSILON]
      end
      if pid ~= 0 then
        local p = PSETS[pid]
        local size = p.size
        for i = size, 1, -1 do
          stack.push_back(p.set[size - 1])
        end
      else
        parser.error("syntax error: expected " .. pop.p .. " but got " .. input.l)
        return false
      end
      break
    else
      parser.error("internal parser error: possibly a bad syntax table")
      return false
    end
	--end
	until (pop.t == 'terminal' and pop.p == 'end')
  
	return true
end


function parser.begin(tokens)
	-- push the starting non-terminal and ending terminal
	-- onto the temporary stack (in reverse order)
  local stack = {}
	local E = { t = "terminal", p = "end" }
	table.insert(stack, E)
	local S = { t = "nonterminal", p = "start" }
	table.insert(stack, S)

	parser.errors = {}

	while parser.parse(stack, tokens) == false do
		-- parsing error, panic
		if #parser.errors >= parser.maxerrors then
			error("parsing interrupted: error count exceeds " .. parser.maxerrors)
			break
		end
		--Release()
    while #tokens > 0 and tokens[1].t ~= "semicolon" do
      table.remove(stack, 1)
		end
    if #tokens == 0 then
      break
    end
		local S = { t = 'nonterminal', 'start' }
		table.insert(stack, S)
	end

	--Log::Output("Parsing Complete. " + IO::ToString(errors) + " error(s)")

	return errors == 0
end

return parser