display:create('temp', 800, 600, 32, true)

terminal = require('utils.log.terminal')
lexer = require("utils.parser.lexer")

local tokens =
{
  ident = "[%w_]+",
  alt = "|",
  def = "=",
  epsilon = "e",
  lit = "'[^%\\]'",
  lit2 = '"[^%\\]"',
  eol = ";",
  comment = "<!%-%-(.-)%-%->"
}

terminal.print("symbols:")
for k, v in pairs(tokens) do
  terminal.printf("%s=%s", k, v)
end

local f = io.open("utils/parser/tests/lex.txt")
local sz = f:read("*a")
terminal.print("input:")
terminal.print(sz)

terminal.print("output:")
local t = lexer.tokenize(sz, tokens, true)
for i, v in ipairs(t) do
  terminal.printf("%d.'%s' (%s) ('%s')", i, v.c, v.t or "?", v.l)
end

def =
{
  rule = { { 'ident', 'def', 'list', 'eol' } },
  list = { { 'term', 'optlist' } },
  optlist = { { 'list' }, { 'epsilon' } },
  term = { { 'ident' }, { 'literal' } },
  literal = { { 'lit' }, { 'lit2' } }
}

--[[
  rule ->  ident def list eol 
  list -> term optlist 
  optlist -> list | eps 
  term -> ident | literal 
  literal -> lit | lit2 
]]

parser = require("utils.parser.parser")

parser.validate(def)

--parser.assemble()
--parser.begin(t)

--parser.begin(t)