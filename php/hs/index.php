<?php
  // Bailout if any asserts fail
  assert_options(ASSERT_ACTIVE, true);
  assert_options(ASSERT_BAIL, true);
  assert_options(ASSERT_WARNING, true);
  
  // XML output
  function echo_header()
  {
    echo "<?xml version=\"1.0\"?>\n";
  }
  function echo_footer()
  {
    echo "<!-- end of xml -->\n";
  }
  function echo_message($type, $sz)
  {
    echo "<Message type=\"$type\">";
    echo $sz;
    echo "</Message>\n";
  }
  function echo_theader_set($board, $s, $e, $t)
  {
    echo "<Board name=\"$board\" start=\"$s\" end=\"$e\" total=\"$t\">\n";
  }
  function echo_theader($board)
  {
    echo "<Board name=\"$board\">\n";
  }
  function echo_row($row, $rank)
  {
    global $gi;
    $t = strtotime($row['time']);
    $c = geoip_country_code_by_addr($gi, $row["ip"]);
    echo "<Entry";
    echo ' rank="', $rank, '"';
    echo ' time="', $t, '"';
    echo ' name="', $row['name'], '"';
    echo ' userid="', $row['ip'], '"';
    echo ' score="', $row['score'], '"';
    echo ' time="', $row['time'], '"';
    echo ' country="', $c, '"';
    if ($row['comment'])
    {
      echo ' comment="', $row['comment'], '"';
    }
    echo " />\n";
  }
  function echo_tfooter($board)
  {
    echo "</Board>\n";
  }
  
  // Generic error
  function message($type, $sz)
  {
    echo_header();
    echo_message($type, $sz);
    echo_footer();
  }
  
  // Database functions
  function table_exists($board, $db)
  {
    $result = mysql_query("SHOW TABLES LIKE '$board'", $db);
    $exists = mysql_num_rows($result) > 0;
    mysql_free_result($result);
    return $exists;
  }
  function table_create($board, $db)
  {
    // todo: sanitize board name
    // todo: prevent flooding
    return mysql_query(
    "CREATE TABLE `$board`
    (
      id int NOT NULL AUTO_INCREMENT,
      name TEXT,
      ip TEXT,
      score INT(11),
      time TIMESTAMP,
      country INT(11),
      comment TEXT
    )", $db);
  }

  // Database configuration
  $host = 'localhost';
  $user = 'dengine_hsadmin';
  $pass = 'zDIrGg_0*n8#';
  $database = 'dengine_commando';
  
  // Hiscore configuration
  // allow creation of new boards
  $newboards = false;
  // allow multiple entries per user (or just overwrite the last/best entry)
  $insert = false;
  // upload password (optional)
  $upass = NULL;
  // timeout period between uploads
  $timeout = 1; //*60;
  
  // fetch ip
  $ip = $_SERVER['REMOTE_ADDR'];
  
  // initialize geo ip location
  include("geoip.inc");
  $gi = geoip_open('GeoIP.dat', GEOIP_MEMORY_CACHE);

  // connect to db
  $db = mysql_connect($host, $user, $pass);
  if (!$db)
  {
    message("error", mysql_error());
    die();
  }
  if (!mysql_select_db($database, $db))
  {
    message("error", mysql_error());
    die();
  }
  
  // Get the rank for an entry
  function get_rank($board, $eid)
  {
    // todo: optimize query using count
    $query = "SELECT * FROM `$board` ORDER BY score DESC";
    $result = mysql_query($query);
    $r = 1;
    while ($row = mysql_fetch_assoc($result))
    {
      if ($row['id'] == $eid)
      {
        return $r;
      }
      $r = $r + 1;
    }
    return $r;
  }
  
  // Upload new entry
  function upload($board, $user, $score, $comment, $method, $create)
  {
    assert($board and $user and $score and $method);
    global $db, $ip, $gi, $timeout, $newboards, $insert;
    
    if (!table_exists($board, $db))
    {
      if ($create && $newboards)
      {
        if (!table_create($board, $db))
        {
          message("error", "Cannot create board: " . $board);
          return;
        }
      }
      else
      {
        message("error", "Board not found: " . $board);
        return;
      }
    }
    
    // use replace if insert is unsupported
    if ($method == "insert" && !$insert)
    {
      $method = "best";
    }

    // get the player's last entry
    $last = NULL;
    $query = "SELECT * FROM `$board` WHERE ip='$ip' ORDER BY time DESC LIMIT 1";
    $result = mysql_query($query, $db);
    if (!$result)
    {
      message("error", mysql_error());
      return;
    }
    if (mysql_num_rows($result) > 0)
    {
      // to prevent flooding make sure that the same IP has 
      // not submitted a high score less than a minute ago
      $last = mysql_fetch_assoc($result);
      $t = time() - strtotime($last['time']);
      if ($t >= 0 and $t <= $timeout)
      {
        message("error", "Timeout exceeded " . $timeout);
        return;
      }
    }
    mysql_free_result($result);

    // get the player's best entry
    $result = mysql_query("SELECT * FROM `$board` WHERE ip='$ip' AND name='$user' ORDER BY score DESC LIMIT 1", $db);
    $best = NULL;
    if ($result)
    {
      // get the first row result
      $best = mysql_fetch_assoc($result);
    }
    mysql_free_result($result);
    
    $eid = NULL;
    $time = date('Y-m-d H:i:s');
    if ($best and $method != "insert")
    {
      // is the new score higher the the user's best entry?
      if ($best['score'] <= $score or $method == "replace")
      {
        $eid = $best['id'];
        // update existing entry
        $query = "UPDATE `$board` SET score=$score, time='$time' WHERE id='$eid'";
        mysql_query($query, $db);
      }
      if ($method != "replace")
      {
        $score = $best['score'];
      }
    }
    else
    {
      // insert new entry
      $country = geoip_country_id_by_addr($gi, $ip);
      $query = "INSERT INTO `$board`(name, ip, score, time, country, comment)
      VALUES('$user', '$ip', $score, '$time', $country, '$comment')";
      mysql_query($query, $db);
      $eid = mysql_insert_id();
    }
    
    $row = array();
    $row['name'] = $user;
    $row['ip'] = $ip;
    $row['time'] = $time;
    $row['score'] = $score;
    $row['comment'] = $comment;
    $rank = get_rank($board, $eid);
    
    echo_header();
    echo_theader($board);
    echo_row($row, $rank);
    echo_tfooter($board);
    echo_footer();
  }
  
  // Download all entries in a given range
  function download($board, $s, $e, $f)
  {
    global $db, $gi, $ip;
    
    if (!table_exists($board, $db))
    {
      message("error", "Board not found: " . $board);
      return;
    }
    // query the database
    $result = mysql_query("SELECT COUNT(*) FROM `$board`");
    if (!$result)
    {
      message("error", mysql_error());
      return;
    }
    $t = mysql_result($result, 0);
    
    $s = max($s, 1);
    $e = max($e, $s);
    $e = min($e, $s + 999);
    $query = sprintf("SELECT * FROM `$board` ORDER BY score DESC LIMIT %s, %s", $s - 1, $e - $s + 1);
    if ($f == "friends")
    {
      $country = geoip_country_id_by_addr($gi, $ip);
      $query = sprintf("SELECT * FROM `$board` WHERE country=%s ORDER BY score DESC LIMIT %s, %s", $country, $s - 1, $e - $s + 1);
    }
    $result = mysql_query($query, $db);
    if (!$result)
    {
      message("error", mysql_error());
      return;
    }
    
    // echo results
    echo_header();
    echo_theader_set($board, $s, $e, $t);
    while ($row = mysql_fetch_assoc($result))
    {
      // ranks are sorted
      $rank = get_rank($board, $row['id']);
      echo_row($row, $rank);
    }
    echo_tfooter($board);
    echo_footer();
    
    mysql_free_result($result);
  }

  // get the entry for a particular user
  function download_entry($board, $name)
  {
    assert($board and $name);
    global $db;
    
    if (!table_exists($board, $db))
    {
      message("error", "Board not found: " . $board);
      return;
    }

    // should return a 'rank' position based on the total score
    $query = "SELECT * FROM `$board` WHERE name='$name' ORDER BY score DESC LIMIT 10000";
    //$query = "SELECT * FROM $board WHERE ip='$userid' ORDER BY score DESC LIMIT 10000";
    $result = mysql_query($query, $db);
    if (!$result)
    {
      message("error", mysql_error());
      return;
    }

    // echo results
    echo_header();
    echo_theader($board);
    while ($row = mysql_fetch_assoc($result))
    {
      // each entry may have a different rank
      $rank = get_rank($board, $row['id']);
      echo_row($row, $rank);
    }
    echo_tfooter($board);
    echo_footer();
    
    mysql_free_result($result);
  }

  if (isset($_GET["s"]) && isset($_GET["e"]))
  {
    // download entries from s to e
    // board
    $b = mysql_real_escape_string($_GET["b"]);
    assert(strlen($b) < 256);
    // start
    $s = (int)$_GET["s"];
    // end
    $e = (int)$_GET["e"];
    // filter
    $f = "global";
    if (isset($_GET["f"]))
    {
      $f = mysql_real_escape_string($_GET["f"]);
      assert($f == "global" or $f == "friends");
    }
    download($b, $s, $e, $f);
  }
  elseif (isset($_POST["v"]))
  {
    // upload entry
    // board
    $b = mysql_real_escape_string($_POST["b"]);
    assert(strlen($b) < 256);
    // username
    $n = mysql_real_escape_string($_POST["n"]);
    assert(strlen($n) < 256);
    // score
    $v = (int)$_POST["v"];
    // comment
    $c = NULL;
    if (isset($_POST["c"]))
    {
      // todo: sanitize comment
      $c = mysql_real_escape_string($_POST["c"]);
      assert(strlen($c) < 256);
    }
    // upload method
    $m = "best";
    if (isset($_POST["m"]))
    {
      $m = mysql_real_escape_string($_POST["m"]);
      // best - keeps the best score
      // replace - replaces with the latest score
      // insert - allows multiple entries
      assert($m == "best" or $m == "replace" or $m == "insert");
    }
    
    // create board
    $nb = (bool)$_POST["nb"];

    // checksum verification
    if ($upass)
    {
      $cs = $_POST["cs"];
      assert(md5($upass) === $cs);
    }

    upload($b, $n, $v, $m, $nb);
  }
  elseif (isset($_GET["n"]))
  {
    // download user entry
    // board
    $b = mysql_real_escape_string($_GET["b"]);
    assert(strlen($b) < 256);
    // user
    $n = mysql_real_escape_string($_GET["n"]);
    assert(strlen($n) < 256);
    
    download_entry($b, $n);
  }
  else
  {
    message("ip", $ip);
    //message("warning", "Nothing selected");
  }

  mysql_close($db);
?>