<?php
  // Bailout if any asserts fail
  assert_options(ASSERT_ACTIVE, true);
  assert_options(ASSERT_BAIL, true);
  assert_options(ASSERT_WARNING, true);
  
  // XML output
  function echo_header()
  {
    echo "<?xml version=\"1.0\"?>\n";
  }
  function echo_footer()
  {
    echo "<!-- end of xml -->\n";
  }
  function echo_message($type, $sz)
  {
    echo "<Message type=\"$type\">";
    echo $sz;
    echo "</Message>\n";
  }
  
  // Generic error
  function message($type, $sz)
  {
    echo_header();
    echo_message($type, $sz);
    echo_footer();
  }

  // Database configuration
  $host = 'localhost';
  $user = 'dengine_hsadmin';
  $pass = 'zDIrGg_0*n8#';
  $database = 'dengine_commando';
  
  // Hiscore configuration
  // allow multiple entries per user (or just overwrite the last/best entry)
  $insert = false;
  // upload password (optional)
  $upass = NULL;
  
  // fetch ip
  $ip = $_SERVER['REMOTE_ADDR'];
  
  // initialize geo ip location
  include("geoip.inc");
  $gi = geoip_open('GeoIP.dat', GEOIP_MEMORY_CACHE);

  // connect to db
  $db = mysql_connect($host, $user, $pass);
  if (!$db)
  {
    message("error", mysql_error());
    die();
  }
  
  
  // Upload new entry
  function upload($board, $user, $ip, $score, $date, $comment, $method)
  {
    assert($board and $user and $score and $method);
    global $database, $gi, $db, $insert;
    if (!mysql_select_db($database, $db))
    {
      message("error", mysql_error());
      die();
    }

    // use replace if insert is unsupported
    if ($method == "insert" && !$insert)
    {
      $method = "best";
    }

    // get the player's best entry
    $result = mysql_query("SELECT * FROM `$board` WHERE ip='$ip' AND name='$user' ORDER BY score DESC LIMIT 1", $db);
    $best = NULL;
    if ($result)
    {
      // get the first row result
      $best = mysql_fetch_assoc($result);
    }
    mysql_free_result($result);
    
    $eid = NULL;
    $time = $date; //date('Y-m-d H:i:s');
    if ($best and $method != "insert")
    {
      // is the new score higher the the user's best entry?
      if ($best['score'] <= $score or $method == "replace")
      {
        $eid = $best['id'];
        // update existing entry
        $query = "UPDATE `$board` SET score=$score, time='$time' WHERE id='$eid'";
        mysql_query($query, $db);
      }
      if ($method != "replace")
      {
        $score = $best['score'];
      }
    }
    else
    {
      // insert new entry
      $country = geoip_country_id_by_addr($gi, $ip);
      $query = "INSERT INTO `$board`(name, ip, score, time, country, comment)
      VALUES('$user', '$ip', $score, '$time', $country, '$comment')";
      mysql_query($query, $db);
      $eid = mysql_insert_id();
    }
  }
  
  
  $dbh = mysql_connect('localhost', 'dengine_hsadmin', 'zDIrGg_0*n8#');
  if (!$dbh)
  {
    die("Cannot connect: " . mysql_error());
  }

  for ($mode = 1; $mode <= 2; $mode ++)
  {
    for ($lvl = 1; $lvl <= 6; $lvl ++)
    {
      $board = "Nuclear Threats";
      if ($lvl == 2) $board = "Reactor 7";
      elseif ($lvl == 3) $board = "Train Ride";
      elseif ($lvl == 4) $board = "Mountain Hideout";
      elseif ($lvl == 5) $board = "Pursuit";
      elseif ($lvl == 6) $board = "Showdown";
      
      if ($mode == 2) $board = $board . " (coop)";
      else $board = $board . " (single)";
      
      if (!mysql_select_db('dengine_hs_commando', $dbh))
      {
        die("Cannot select db: " . mysql_error());
      }
      $q = "SELECT * FROM scores WHERE mode=$mode AND level=$lvl ORDER BY addedat DESC";
      $res = mysql_query($q, $dbh);
      while ($row = mysql_fetch_assoc($res))
      {
        //date('Y-m-d H:i:s')
        upload($board, $row["username"], $row["ip"], floor($row['total']*10000), $row["addedat"], "", "best");
      }
    }
  }
?>
