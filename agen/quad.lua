local cos = math.cos
local sin = math.sin
local min = math.min
local max = math.max

local paint = {}

local vx = array.floats { 0,0, 1,0, 0,-1, 1,-1 }
local uv = array.floats { 0,0, 1,0, 0,-1, 1,-1 }

local shape = {}
shape.type = 'triStrip'
shape.vertices = vx
shape.uv = uv

--- Sets the source texture
-- @param i image
-- @param x, y position on the source texture
-- @param w, h dimensions on the source texture
function paint.source(i, x, y, w, h)
  local iw, ih = i.width, i.height
  local sx, sy = x or 0, y or 0
  local sw, sh = w or iw, h or iw
  local tw, th = iw, ih
	shape.texture = i
  -- for spritesheets or atlases, paint sub-section
  if i.texture then
    shape.texture = i.texture
    sx, sy = sx + i.x, sy + i.y
    tw, th = i.texture.width, i.texture.height
  end
  -- mapping
  local ux, uy = sx/tw, sy/th
  local ux2, uy2 = sw/tw + ux, sh/th + uy
  uv[1], uv[2] = ux, uy
  uv[3], uv[4] = ux2, uy
  uv[5], uv[6] = ux, uy2
  uv[7], uv[8] = ux2, uy2
  -- vertices
  vx[1], vx[2] = 0, 0
  vx[3], vx[4] = iw, 0
  vx[5], vx[6] = 0, -ih
  vx[7], vx[8] = iw, -ih
end
  
--- Paints the source texture in a quad
-- @param c canvas
-- @param x, y top-left position
-- @param w, h width and height
-- @param r angle in radians (optional)
-- @param ox, oy rotation offset (optional)
function paint.quad(c, x, y, w, h, r, ox, oy)
  local n = #vx
  -- scale
  if w and h then
    local i = shape.texture
    local iw, ih = i.width, i.height
    local sx, sy = w/iw, h/ih
    for i = 1, n, 2 do
      vx[i] = vx[i]*sx
      vx[i + 1] = vx[i + 1]*sy
    end
  end
  -- offset
  if ox and oy then
    for i = 1, n, 2 do
      vx[i] = vx[i] - ox
      vx[i + 1] = vx[i + 1] + oy
    end
  end
  -- rotation
  if r then
    local c = cos(a)
    local s = sin(a)
    for i = 1, n, 2 do
      local x, y = vx[i], vx[i + 1]
      vx[i] = c*x - s*y
      vx[i + 1] = s*x + c*y
    end
  end
  -- translation
  for i = 1, n, 2 do
    vx[i] = vx[i] + x
    vx[i + 1] = vx[i + 1] + y
  end
  c:add_shape(shape)
  
  -- todo: will not work when called twice in a row
  shape.texture = nil
end

--- Paints the source texture in a centered quad
-- @param c canvas
-- @param x, y center position
-- @param w, h width and height
-- @param r angle in radians (optional)
function paint.quadc(c, x, y, w, h, r)
  paint.quad(c, x, y, w, h, r, w/2, h/2)
end

--[[
  -- general purpose mapping for any polygon
  local l, r, t, b = extents(vx)
  local pw, ph = r - l, b - t
  -- mapping
  local ux, uy = sx/iw, sy/ih
  local uw, uh = sw/iw, sh/ih
  for i = 1, #vx, 2 do
    uv[i] = (vx[i] - l)/pw*uw + ux
    uv[i + 1] = (vx[i + 1] - t)/ph*uh + uy
  end
]]

return paint