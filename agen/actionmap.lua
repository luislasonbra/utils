local _keys = require("utils.agen.keys")

-- reverse lookup
local _rkeys = {}
for id, label in pairs(_keys) do
  assert(_rkeys[label] == nil)
  _rkeys[label] = id
end

local actionmap = {}

local _attached = {}
local _mapped = {}
local _active = {}

actionmap.attached = _attached
actionmap.assigned = _mapped

--- Devices

--- Checks if the device is atatched
-- @param id device id
function actionmap.isAttached(id)
  return _attached[id] ~= nil
end

--- Adds attached device
-- @param id device id
-- @param d device object
function actionmap.addDevice(id, d)
  assert(not _attached[id], "device id already exists")
  _attached[id] = d
  _mapped[id] = _mapped[id] or {}
  _active[id] = _active[id] or {}
end

--- Gets attached device
-- @param id device id
-- @return device object
function actionmap.getDevice(id)
  return _attached[id]
end

--- Removes attached device
-- @param id device id
function actionmap.removeDevice(id)
  _attached[id] = nil
end

--- Removes attached devices
function actionmap.removeDevices()
  for id in pairs(_attached) do
    _attached[id] = nil
  end
end

--- Assignments

--- Adds new action
-- @param id device id
-- @param action action id
-- @param string of keys
function actionmap.addAction(id, action, sequence)
  local device = _mapped[id]
  -- alternatives, separated by "|"
  for s in string.gmatch(sequence, "[^|]+") do
    -- sequences, separated by "+"
    local map = {}
    for key in string.gmatch(s, "[^%+]+") do
      map[#map + 1] = _rkeys[key]
    end
    if #map > 0 then
      map.action = action
      device[#device + 1] = map
    end
  end
end

--- Gets existing action
-- @param id device id
-- @param action action id
-- @return string of keys
function actionmap.getAction(id, action)
  local t = {}
  local device = _mapped[id]
  for i = 1, #device do
    local map = device[i]
    if map.action == action then
      local t2 = {}
      for i = 1, #map do
        t2[#t2 + 1] = _keys[ map[i] ]
      end
      t[#t + 1] = table.concat(t2, '+')
    end
  end
  return table.concat(t, '|')
end

--- Removes existing action
-- @param id device id
-- @param action action id
function actionmap.removeAction(id, action)
  local device = _mapped[id]
  for i = #device, 1, -1 do
    local map = device[i]
    if map.action == action then
      table.remove(device, i)
    end
  end
end

--- Mappings

--- Checks if mappings are loaded for the device
-- @param id device id
function actionmap.isMapped(id, key)
  local mappings = _mapped[id]
  if mappings == nil then
    return false
  end
  if key == nil then
    return #mappings > 0
  end
  for i = 1, #mappings do
    local map = mappings[i]
    for j = 1, #map do
      if map[j] == key then
        return true
      end
    end
  end
  return false
end

--- Adds mapping
-- @param sz mapping string
function actionmap.addMapping(sz)
  -- strip line comments
  sz = string.gsub(sz, "#(.-)\n", "\n")
  -- strip new line symbol
  sz = string.gsub(sz, "\n", "")
  -- trailing comma
  sz = sz .. ','
  -- devices
  local id = nil
  for pair in string.gmatch(sz, '(.-),()') do
    -- actions and corresponding mappings
    local a, s = string.match(pair, "(.-):(.-)$")
    if a and s then
      if id then
        local map = _mapped[id] or {}
        _mapped[id] = map
        actionmap.addAction(id, a, s)
      end
    else
      id = pair
    end
  end
end

--- Gets mapping
-- @param id device id
function actionmap.getMapping(id)
  local devices = {}
  for id in pairs(_mapped) do
    local mappings = _mapped[id]
    local actions = {}
    for i = 1, #mappings do
      local map = mappings[i]
      local a = map.action
      if actions[a] == nil then
        actions[a] = actionmap.getAction(id, a)
      end
    end
    local sz = id .. ','
    for action, map in pairs(actions) do
      sz = sz .. action .. ':' .. map .. ','
    end
    devices[#devices + 1] = sz
  end
  return table.concat(devices, '\n')
end

--- Removes mapping for device
function actionmap.removeMapping(id)
  if _mapped[id] then
    _mapped[id] = {}
  end
end

--- Process

--- Checks if a given action is active
function actionmap.isActive(id, action)
  return _active[id][action] ~= nil
end

function actionmap.pollDevice(id)
  -- todo: inefficient
  local device = _attached[id]
  local down = device.is_down
  local state = _active[id]
  -- for each mapping
  local mappings = _mapped[id]
  for i = 1, #mappings do
    local map = mappings[i]
    local a = map.action
    local s = state[a]
    if s ~= true then
      local d = true
      -- for each assigned button
      for j = 1, #map do
        if not down(device, map[j]) then
          d = false
          break
        end
      end
      -- evoke callbacks
      if d then
        state[a] = true
        -- just pressed?
        if s == nil then
          actionmap.beginAction(id, a)
        end
      end
      -- callback removed the device?
      if _attached[id] == nil then
        break
      end
    end
  end
  for a, s in pairs(state) do
    if s == true then
      state[a] = false
    else
      state[a] = nil
      actionmap.endAction(id, a)
    end
    -- callback removed the device?
    if _attached[id] == nil then
      break
    end
  end
end

--- Updates devices and triggers callbacks
function actionmap.poll()
  for id in pairs(_attached) do
    actionmap.pollDevice(id)
  end
end

function actionmap.beginAction(id, action)
  
end

function actionmap.endAction(id, action)

end

return actionmap