-- Combined callbacks for mice and touchscreens

local input = {}

local _tx = {}
local _ty = {}

-- multitouch input
if mtouch then --and display:multitouch_enable() == true then
  function mtouch:on_begin(id, x, y)
    _tx[id] = x
    _ty[id] = y
    input.tbegin(id, x, y)
  end
  function mtouch:on_end(id, x, y)
    input.tend(id, x, y)
    _tx[id] = nil
    _ty[id] = nil
  end
  function mtouch:on_cancel(id)
    input.tcancel(id)
    _tx[id] = nil
    _ty[id] = nil
  end
  function mtouch:on_move(id, x, y)
    _tx[id] = x
    _ty[id] = y
    input.tmove(id, x, y)
  end
end

-- mouse input
if mouse then
  local mbid =
  {
    [108] = 'left',
    [110] = 'right',
    [109] = 'middle',
    [111] = 'x'
  }
  
  function mouse:on_press(b)
    local x, y = mouse.xaxis, mouse.yaxis
    local id = mbid[b]
    _tx[id] = x
    _ty[id] = y
    input.tbegin(id, x, y)
  end
  function mouse:on_release(b)
    local x, y = mouse.xaxis, mouse.yaxis
    local id = mbid[b]
    input.tend(id, x, y)
    _tx[id] = nil
    _ty[id] = nil
  end
  function mouse:on_move(dx, dy)
    -- temporary switch to absolute positions
    local x, y = mouse.xaxis, mouse.yaxis
    for b, id in pairs(mbid) do
      if mouse:is_down(b) == true then
        _tx[id] = x
        _ty[id] = y
        input.tmove(id, x, y)
      end
    end
  end
  function mouse:on_wheelmove(z)
    input.wmove(z)
  end
end

function input.get_position(id)
  if id == "mouse" then
    if mouse then
      return mouse.xaxis, mouse.yaxis
    end
  end
  return _tx[id], _ty[id]
end

-- callbacks
function input.tbegin(id, x, y)
end
function input.tmove(id, x, y)
end
function input.tend(id, x, y)
end
function input.tcancel(id)
end
function input.wmove(z)
end

return input