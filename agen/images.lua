local byte = string.byte
local sub = string.sub
local format = string.format

local images = {}

-- enable or disable image loading
images.enabled = true
images.loaded = 0
images.size = 0

local lookup = {}

images.lookup = lookup

--- Finds image from filename
-- @param fn filename
-- @return image
function images.find(fn)
  return lookup[fn]
end

--- Finds filename from image
-- @param i image
-- @return id
function images.filename(i)
  for n, i2 in pairs(lookup) do
    if i == i2 then
      return n
    end
  end
end

--- Load an image from filename
-- @param fn filename
-- @return loaded image
function images.load(fn)
  if images.enabled ~= true then
    return
  end
  local i = lookup[fn]
  -- already loaded?
  if i == nil then
    i = Image()
    if i:load(fn) == false then
      return
    end
    -- reference
    lookup[fn] = i
    local s = i.width*i.height
    images.loaded = images.loaded + 1
    images.size = images.size + s
  end
  return i
end

--- Loads a spritesheet image from filename
-- @param fn filename
-- @param c columns
-- @param r rows
-- @return image object
function images.load_sheet(fn, c, r)
  if images.enabled ~= true then
    return
  end
  assert(c, "number of columns is nil")
  assert(r, "number of rows is nil")
  local image = images.load(fn)
  if image == nil then
    return
  end
  -- tile dimensions from cols and rows
  local w = image.width/c
  local h = image.height/r
  for x = 0, c - 1 do
    for y = 0, r - 1 do
      local i =
      {
        texture = image,
        -- position in the spritesheet
        x = x*w,
        y = y*h,
        -- tile dimensions
        width = w,
        height = h
      }
      -- spritesheet tiles are indexed as follows: 
      -- "filename.1", "filename.2", "filename.3", etc
      local fn2 = format("%s.%d", fn, x + y*c)
      assert(lookup[fn2] == nil, "image id already exists")
      --images.add(n, i)
      lookup[fn2] = i
    end
  end
  return image
end

--- Loads a texture atlas from filename
--- Requires LuaXML
-- @param fn filename
-- @return image object
--function images.load_atlas(fn)
function images.load_atlas(atlas)
  if images.enabled ~= true then
    return
  end
  local image = images.load(atlas.imagePath)
  if image == nil then
    return
  end
  -- add entry for each sprite tag
  for i = 1, #atlas do
    local v = atlas[i]
    if type(v) == 'table' then
      if v[0] == 'sprite' then
        local n = v.n
        if lookup[n] == nil then
          --assert(lookup[v.name] == nil, "image id already exists")
          -- add to images
          local w, h = v.w, v.h
          local ox, oy = 0, 0
          if v.wo and v.ho then
            ox = -(v.wo/2 - (v.xo + w/2))
            oy = v.ho/2 - (v.yo + h/2)
          end
          local i =
          {
            texture = image,
            -- position in the spritesheet
            x = v.x,
            y = v.y,
            -- tile dimensions
            width = w,
            height = h,
            -- drawing offset
            ox = ox,
            oy = oy
          }
          lookup[n] = i
        end
      end
    end
  end
  return image
end

--- Unloads an image
-- @param fn filename
function images.unload(fn)
  local i = lookup[fn]
  if i then
    local s = i.width*i.height
    if i.unload == nil or i:unload() == true then
      -- dereference
      lookup[fn] = nil
      if i.unload then
        images.loaded = images.loaded - 1
        images.size = images.size - s
      end
      return true
    end
  end
  return false
end

--- Unloads all images
function images.unload_all()
  for i, v in pairs(lookup) do
    images.unload(i)
  end
end

return images