local _mode = DisplayMode()
local _modes = display.modes
local _orient = 'portrait'
local _init = false
local _fw, _fh = 0, 0

--- Checks if the display is initialized
-- @return True if initialized
function display.initialized()
  return _init
end

--- Sets the current display mode
-- @param w Width
-- @param h Height
-- @param b Color depth
-- @param m Windowed mode
-- @return True if successful
function display.set_res(w, h, b, m)
  if m == false and not display.is_supported(w, h, b) then
    return false
  end
  --if not display:get_mode(_mode) then
    --return false
  --end
  _mode.width = w
  _mode.height = h
  _mode.depth = b
  _mode.windowed = m
  -- try to set the new mode
  if not _init then
    _init = display:create("2DEngine", w, h, b, m)
    return _init
  else
    return display:set_mode(_mode)
  end
end

--- Gets the current display mode
-- @return Resolution parameters
function display.get_res()
  if display:get_mode(_mode) then
    return _mode.width, _mode.height, _mode.depth, _mode.windowed
  end
end

--- Checks if a full-screen resolution is supported
-- @param w Width
-- @param h Height
-- @param d Depth
-- @return True if supported
function display.is_supported(w, h, d)
  if #_modes == 0 then
    return true
  end
  -- iterate enumerated resolutions
  for i = 1, #_modes do
    local mode = _modes[i]
    if mode.width == w and mode.height == h and mode.depth == d then
      return true
    end
  end
  return false
end

--- Sets the current orientation
-- @param o Orientation
function display.set_orient(o)
  assert(o == 'landscape' or o == 'portrait', 'invalid orientation')
  _orient = o
  -- untransformed geometry
  local a = 0
  if o == 'landscape' then
    a = -90
  end
  --local v = display.viewport
  --v:set_rotation(a)
  -- transformed geometry (camera)
  --scene.camera:set_rotation(-a)
end

--- Gets the current orientation
-- @return Orientation
function display.get_orient()
  return _orient
end

--- Gets the current resolution (oriented)
-- @return Dimensions
function display.get_res_orient()
  local w, h = display.get_res()
  if _orient == 'landscape' then
    w, h = h, w
  end
  return w, h
end

--- Sets the frustum area
-- @param w Width in units
-- @param h Height in units
function display.set_frustum(w, h)
  local w2, h2 = display.get_res()
  local sx = w/w2
  local sy = h/h2
  local s = math.max(sx, sy)
  --display.viewport:set_scale(s, s)
  display.viewport.camera:set_scale(s, s)
  _fw = w
  _fh = h
end
--- Gets the frustum area
function display.get_frustum()
  return _fw, _fh
end

local _vw, _vh = 0, 0
function display.set_viewarea(vw, vh)
  _vw, _vh = vw, vh
end

function display.get_viewarea()
  -- todo: learn to use viewports
  return _vw, _vh
end

function display.set_camera(c)
  display.viewport.camera = c
end
function display.get_camera(c)
  return display.viewport.camera
end
function display.add_node(c)
  display.viewport:add_child(c)
end
function display.remove_node(c)
  return display.viewport:remove_child(c)
end
function display.remove_nodes()
  display.viewport.remove_children()
end
