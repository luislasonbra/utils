if audio == nil then
  audio = {}
  audio.name = "unavailable"
  audio.volume = 0
  --- Overwrite missing functions to prevent Lua errors
  local _default = function() end
  audio.set_mode = _default
  audio.get_mode = _default
  audio.play = _default
  audio.play_loop = _default
  audio.resume = _default
  audio.pause = _default
  audio.stop = _default
end

--- Allows loading and playing sounds
audio.enabled = false

--- Stats
audio.played = 0
audio.loaded = 0
audio.streamed = 0

--- Volume scale between 0-1
--- Why not just use "audio.volume"?
--- Because it affects the Windows volume slider
--- Because we want to control "music" and "sfx" volume separately
-- for sounds: volume*svolume
audio.svolume = 1
-- for music: volume*mvolume
audio.mvolume = 1

--- Pitch offset between -1 and 1
--- Why would you want to have a global pitch offset?
--- So when we do cool slow-mo effects the soundtrack slows down
-- for sounds: (pitch + spitch)/2
audio.spitch = 0
-- for music: (pitch + mpitch)/2
audio.mpitch = 0

--- Indexes sounds and music by filename
local _sounds = {}
local _music = {}

--- Initialize audio
function audio.init(b, c, f)
  audio.enabled = audio:set_mode(b, c, f)
  return audio.enabled
end

--- Loads sound from filename
-- @param fn filename
-- @return Loaded sound object
function audio.load_sound(fn)
  --assert(f, "sound filename is nil")
  local s = _sounds[fn]
  -- already loaded?
  if s == nil and audio.enabled == true then
    s = Sound()
    if s:load(fn) == false then
      return
    end
    _sounds[fn] = s
    audio.loaded = audio.loaded + 1
  end
  return s
end

--- Unloads sound by filaname
-- @param f filename
function audio.unload_sound(fn)
  --assert(f, "sound filename is nil")
  local s = _sounds[fn]
  if s then
    if s:unload() == true then
      _sounds[fn] = nil
      audio.loaded = audio.loaded - 1
      return true
    end
  end
  return false
end

--- Unloads all sounds
function audio.unload_all_sound()
  for i, v in pairs(_sounds) do
    audio.unload_sound(i)
  end
end

--- Returns sound object from filename
-- @param fn Filename
-- @return Loaded sound object
function audio.find_sound(fn)
  --assert(f, "sound filename is nil")
  return _sounds[fn]
end

--- Plays loaded sound
-- @param fn Filename
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function audio.play_sound(fn, v, p, t)
  local s = _sounds[fn]
  if s and audio.enabled == true then
    v = (v or 1)*audio.svolume
    p = p or 0
    t = ((t or 0) + audio.mpitch)/2
    audio.played = audio.played + 1
    return audio:play(s, v, p, t)
  end
end

--- Plays loaded sound in a loop
-- @param fn Filename
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function audio.loop_sound(fn, v, p, t)
  local s = _sounds[fn]
  if s and audio.enabled == true then
    v = (v or 1)*audio.svolume
    p = p or 0
    t = ((t or 0) + audio.mpitch)/2
    audio.played = audio.played + 1
    return audio:play_loop(s, v, p, t)
  end
end



--- Loads music object by filaname
-- @param f Filename
function audio.load_music(fn)
  --assert(f, "music filename is nil")
  local s = _music[fn]
  -- already loaded?
  if s == nil and audio.enabled == true then
    s = Music()
    if s:load(fn) == false then
      return
    end
    _music[fn] = s
    audio.streamed = audio.streamed + 1
  end
  return s
end

--- Unloads music object by filaname
-- @param f Filename
function audio.unload_music(fn)
  --assert(f, "music filename is nil")
  local s = _music[fn]
  if s then
    if s:unload() == true then
      _music[fn] = nil
      audio.streamed = audio.streamed - 1
      return true
    end
  end
  return false
end

--- Unloads all music objects
function audio.unload_all_music()
  for i, v in pairs(_music) do
    audio.unload_music(i)
  end
end

function audio.find_music(fn)
  --assert(f, "music filename is nil")
  return _music[fn]
end

--- Plays loaded music
-- @param fn filename
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function audio.play_music(fn, v, p, t)
  local m = _music[fn]
  if m and audio.enabled == true then
    v = (v or 1)*audio.mvolume
    p = p or 0
    t = ((t or 0) + audio.mpitch)/2
    return audio:play(m, v, p, t)
  end
end

--- Plays loaded music in a loop
-- @param f Filename
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function audio.loop_music(fn, v, p, t)
  local m = _music[fn]
  if m and audio.enabled == true then
    v = (v or 1)*audio.mvolume
    p = p or 0
    t = ((t or 0) + audio.mpitch)/2
    return audio:play_loop(m, v, p, t)
  end
end



function audio.hard_stop(a)
  if a then
    a:stop()
    a.sound = nil -- Phil's hack
  end
end