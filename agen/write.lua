local sub = string.sub
local len = string.len
local match = string.match
local gmatch = string.gmatch
local tinsert = table.insert
local floor = math.floor
local get_width = Font.get_width

local write = {}

-- todo: vertical flow
-- todo: adjustable line height
-- todo: unicode

-- enables or disables debug lines
write.debug = false

-- font
local _font = nil
-- font height
local _hfont = 0
-- font size
local _sfont = 0
-- line height
local _hline = 0

local _color = Color(255, 255, 255)
local _alpha = 1

-- horizontal
local _align = 'left'
-- vertical
local _valign = 'top'

-- line breaks
local _wrap = 'words'
-- text direction
local _flow = 'ltr'

local lime = Color(0, 255, 0)
local blue = Color(0, 0, 255)
local yellow = Color(0, 255, 255)
local white = Color(255, 255, 255)
local red = Color(255, 0, 0)

--- Sets the font
-- @param font font object
-- @param line line height in pixels (optional)
function write.font(font, line)
  _font = font
  _hfont = font:get_height()
  _sfont = font:get_size()
  _hline = line or _hfont
end

--- Sets the color and alpha
-- @param color color object
-- @param alpha alpha value
function write.style(color, alpha)
  _color = color or white
  _alpha = alpha or 1
end

--- Sets the alignement
-- @param align horizontal alignment: 'left', 'center' or 'right'
-- @param valign vertical alignment: 'top', 'middle' or 'bottom'
function write.align(align, valign)
  _align = align
  _valign = valign or 'top'
end

--- Sets the line breaking and flow direction
-- @param wrap line breaking style: 'none', 'chars' or 'words'
-- @param flow flow direction: 'ltr' or 'rtl'
function write.flow(wrap, flow)
  _wrap = wrap
  _flow = flow or 'ltr'
end

--- Wraps a single line based on character length
-- @param sz input string
-- @param width width in pixels
-- @return two strings
function write.wrapchars(sz, width)
  -- fits already
  if get_width(_font, sz) <= width then
    return sz, ""
  end
  -- trim
  -- todo: very slow for long text without line breaks
  local sz2 = ""
  local sz3 = ""
  if _flow == 'rtl' then
    for i = len(sz), 1, -1 do
      local _sz = sub(sz, i, -1)
      if get_width(_font, _sz) > width then
        break
      end
      sz2 = _sz
    end
    local i2 = len(sz2) + 1
    sz3 = sub(sz, 1, -i2)
  else
    for i = len(sz), 1, -1 do
      local _sz = sub(sz, 1, -i)
      if get_width(_font, _sz) > width then
        break
      end
      sz2 = _sz
    end
    local i2 = len(sz2) + 1
    sz3 = sub(sz, i2, -1)
  end
  return sz2, sz3
end

--- Wraps a single line based on line breaks
-- @param sz input string
-- @param width width in pixels
-- @return two strings
function write.wrapwords(sz, width)
  -- trim
  local sz2, sz3 = write.wrapchars(sz, width)
  if sz == sz2 then
    return sz2, sz3
  end
  -- break
  -- todo: figure out how to wrap lines ending with space
  -- todo: figure out break characters for UTF-8
  if _flow == 'rtl' then
    if not match(sz2, "^([%s%-])") then
      local _, sz4 = match(sz2, "^(.-)([%s%-].*)")
      if sz4 then
        local i2 = len(sz4) + 1
        sz2 = sz4
        sz3 = sub(sz, 1, -i2)
      end
    end
  else
    if not match(sz2, "([%s%-])$") then
      local sz4, _ = match(sz2, "(.*[%s%-])(.+)$")
      if sz4 then
        local i2 = len(sz4) + 1
        sz2 = sz4
        sz3 = sub(sz, i2, -1)
      end
    end
  end
  return sz2, sz3
end

--- Wraps a single line
-- @param sz input string
-- @param width width in pixels
-- @return two strings
function write.wrapline(sz, width)
  if _wrap == 'words' then
    return write.wrapwords(sz, width)
  else
    return write.wrapchars(sz, width)
  end
end

--- Wraps a multi-line string to fit a predefined width
-- @param sz input string
-- @param width width in pixels
-- @param maxlines number of lines to generate
-- @return table containing lines
function write.wrap(sz, width, maxlines)
  -- break the string into lines
  local lines = {}
  for t in gmatch(sz, "[^\n]+") do
    tinsert(lines, t)
  end
  -- break lines longer than the predefined width
  local i = 1
  while i <= #lines and i <= maxlines do
    local sz2 = lines[i]
    local sz3, sz4 = write.wrapline(sz2, width)
    if sz3 == "" then
      return {}
    elseif sz4 ~= "" then
      lines[i] = sz3
      if _wrap ~= "none" then
        tinsert(lines, i + 1, sz4)
      end
    end
    i = i + 1
  end
  return lines
end

--- Writes text
-- @param sz input string
-- @param x, y position
function write.text(c, sz, x, y)
  -- debug
  if write.debug then
    local _w = get_width(_font, sz)
    local y2 = y + _sfont
    local y3 = y + _sfont/2 + _hfont/2
    local y4 = y + _sfont/2 - _hfont/2
    -- baseline and size
    c:move_to(x, y)
    c:rel_line_to(_w, 0)
    c:move_to(x, y2)
    c:rel_line_to(_w, 0)
    c:set_line_style(1, lime, 1)
    c:stroke()
    -- ascent and descent
    c:move_to(x, y3)
    c:rel_line_to(_w, 0)
    c:move_to(x, y4)
    c:rel_line_to(_w, 0)
    c:set_line_style(1, blue, 1)
    c:stroke()
    -- ends
    c:move_to(x, y4)
    c:rel_line_to(0, _hfont)
    c:move_to(x + _w, y4)
    c:rel_line_to(0, _hfont)
    c:set_line_style(1, yellow, 1)
    c:stroke()
  end
  -- write
  c:move_to(x, y)
  c:set_font(_font, _color, _alpha)
  c:write(sz)
end

--- Writes clampped line of text
-- @param sz input string
-- @param x, y position
-- @param width width in pixels
function write.line(c, sz, x, y, w)
  -- clamp
  sz = write.wrapline(sz, w)
  -- align
  local _w = get_width(_font, sz)
  local _x = x
  if _align == 'center' then
    _x = x + w/2 - _w/2
  elseif _align == 'right' then
    _x = x + w - _w
  end
  -- debug
  if write.debug then
    local s = _font:get_size()
    c:move_to(x, y)
    c:rel_line_to(w, 0)
    c:rel_line_to(0, s)
    c:rel_line_to(-w, 0)
    c:close_path()
    c:set_line_style(1, red, 1)
    c:stroke()
  end
  -- write
  write.text(c, sz, _x, y)
end

-- todo: negative height "h" should not render anything

--- Write a text box
-- @param sz input string
-- @param x, y position
-- @param w width in pixels
-- @param h height in pixels
function write.rect(c, sz, x, y, w, h)
  -- lines
  local rows = 1
  if _hline > 0 then
    rows = floor(h/_hline)
    if rows <= 0 then
      rows = 1
    end
  end
  local lines = write.wrap(sz, w, rows)
  if #lines < rows then
    rows = #lines
  end
  -- debug
  if write.debug then
    c:move_to(x, y)
    c:rel_line_to(w, 0)
    c:rel_line_to(0, -h)
    c:rel_line_to(-w, 0)
    c:close_path()
    c:set_line_style(1, red, 1)
    c:stroke()
  end
  -- valign
  local _h = rows*_hline
  local oy = 0
  if _valign == 'middle' then
    y = y - h/2 + _h/2 + (_hfont/2 - _sfont/2)
  elseif _valign == 'bottom' then
    y = y - h + _h
  else
    y = y + _sfont/2
  end
  -- write
  for i = 1, rows do
    local sz = lines[i]
    local _x = x
    local _y = y - i*_hline
    local _w = get_width(_font, sz)
    if _align == 'center' then
      _x = _x + w/2 - _w/2
    elseif _align == 'right' then
      _x = _x + w - _w
    end
    write.text(c, sz, _x, _y)
  end
end

require("unicode")
local utf8 = unicode.utf8
--local utf8 = require("utils.lang.utf8")

-- Write scaled text
local shape = { type = 'triList' }
function write.scaled(c, sz, x, y, sx, sy)
  sx, sy = sx or 1, sy or 1
	local prev = nil
  local vx = {}
  local uv = {}
  local ox, oy = x, y
	for i = 1, utf8.len(sz) do
    -- next char
    -- todo: UTF-8
    local s = utf8.sub(sz, i, i)
    local bs = utf8.byte(s)
    -- add kerning
    if prev ~= nil then
      ox = ox + prev:get_kerning(bs)
    end
    -- note: not GC-ed
    -- todo: "\n" doesn't have a glyph
    local q = _font:get_glyph(bs)
    if q then
      -- align with baseline
      local l, t = ox + q.hBearingX*sx, oy + q.hBearingY*sy
      local r, b = l + q.width*sx, t - q.height*sy
      local ql, qt = q.coords[1], q.coords[2]
      local qr, qb = q.coords[3], q.coords[4]
      
      local i2 = #vx --(i - 1)*6*2
      -- bottom left
      vx[i2 + 1], vx[i2 + 2] = l, b
      uv[i2 + 1], uv[i2 + 2] = ql, qb
      -- top left
      vx[i2 + 3], vx[i2 + 4] = l, t
      uv[i2 + 3], uv[i2 + 4] = ql, qt
      -- bottom right
      vx[i2 + 5], vx[i2 + 6] = r, b
      uv[i2 + 5], uv[i2 + 6] = qr, qb
      -- top right
      vx[i2 + 7], vx[i2 + 8] = r, t
      uv[i2 + 7], uv[i2 + 8] = qr, qt
      -- bottom right
      vx[i2 + 9], vx[i2 + 10] = r, b
      uv[i2 + 9], uv[i2 + 10] = qr, qb
      -- top left
      vx[i2 + 11], vx[i2 + 12] = l, t
      uv[i2 + 11], uv[i2 + 12] = ql, qt
      -- advance
      ox = ox + q.hAdvance*sx
      
      if write.debug then
        c:move_to(l, t)
        c:line_to(r, t)
        c:line_to(r, b)
        c:line_to(l, b)
        c:close_path()
        c:set_line_style(sx, lime, 1)
        c:stroke()
      end
    elseif s == "\n" or s == "\r" then
      ox = x
      oy = oy - _font:get_height()*sy
    end
    prev = q
	end
  if write.debug then
    local w = get_width(_font, sz)
    c:move_to(x, y)
    c:line_to(x + w*sx, y)
    c:set_line_style(sx, red, 1)
    c:stroke()
  end

  if #vx > 0 then
    shape.texture = _font.texture
    shape.vertices = array.floats(vx)
    shape.uv = array.floats(uv)

    c:add_shape(shape)
  end
end

return write