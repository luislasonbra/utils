display:create("terminal test", 1000, 600, 32, true, 30, false, 4)

terminal = require('utils.log.terminal')
actionmap = require("utils.agen.actionmap")

actionmap.addDevice(keyboard.name, keyboard)

--actionmap.addAction(keyboard.name, "up", "up|w|i")
--actionmap.addAction(keyboard.name, "down", "down|s|k")
--actionmap.addMapping("down:down|s|k,up:up|w|i", keyboard.name)
--actionmap.loadMapping("utils/agen/controllers.txt")
actionmap.loadMapping("ftm/op/keyboard.map", keyboard.name)

--actionmap.loadMapping("ftm/op/keyboard.map", keyboard.name)

function actionmap.beginAction(id, action)
  terminal.print(action, "+")
end
function actionmap.endAction(id, action)
  terminal.print(action, "-")
end

timer = Timer()
function timer:on_tick()
  actionmap.process()
  terminal.trace("mapping", "\n" .. actionmap.getMapping(keyboard.name))
end
timer:start(16, true)