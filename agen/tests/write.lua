local text1 =
[[The retrograde movements in history, the so-called
periods of restoration, which try to revive intellectual and social
conditions that existed before those immediately preceding,�and seem
really to succeed in giving them a brief resurrection,�have the charm of
sentimental recollection, ardent longing for what is almost lost, hasty
embracing of a transitory happiness. It is on account of this strange
trend towards seriousness that in such transient and almost dreamy periods
art and poetry find a natural soil, just as the tenderest and rarest
plants grow on mountain-slopes of steep declivity.�Thus many a good artist
is unwittingly impelled to a �restoration� way of thinking in politics and
society, for which, on his own account, he prepares a quiet little corner
and garden. Here he collects about himself the human remains of the
historical epoch that appeals to him, and plays his lyre to many who are
dead, half-dead, and weary to death, perhaps with the above-mentioned
result of a brief resurrection.]]
local text2 = string.gsub(text1, "\n", " ")
local text3 = [[1234567890]]

local text = text2
local align = 'left'
local valign = 'top'
local flow = 'ltr'
local wrap = 'chars'
local lineh = 24
local fontn = 'Bernhard Modern.ttf'
local fonts = 16
local white = Color(255, 255, 255)

display:create("write", 800, 600, 32, true)

terminal = require("utils.log.terminal")

local write = require("utils.agen.write")

local s = Sprite()
display.viewport:add_child(s)
local font = Font()

local function textbox(x, y, w, h)
  if font.status ~= 'loaded' then
    font:load_file("utils/agen/tests/" .. fontn, fonts)
  end

  s.canvas:clear()
  write.font(font, lineh)
  write.style(white, 1)
  write.align(align, valign)
  write.flow(wrap, flow)
  write.rect(s.canvas, text, x, y, w, h)
  
  terminal.trace('align (LMB)', align)
  terminal.trace('valign (MMB)', valign)
  terminal.trace('wrap (RMB)', wrap)
  terminal.trace('lineh (U/D)', lineh)
  terminal.trace('flow (L)', flow)
  terminal.trace('text (T)', #text)
  terminal.trace('debug (D)', write.debug)
  terminal.trace('font (F)', fontn)
  terminal.trace('fsize (MW)', fonts)
end

local function mousebox()
  textbox(-300, 200, mouse.xaxis + 300, -mouse.yaxis + 200)
end

function mouse:on_move(dx, dy)
  mousebox()
end

function keyboard:on_press(k)
  if k == KEY_L then
    if flow == 'rtl' then
      flow = 'ltr'
    else
      flow = 'rtl'
    end
  elseif k == KEY_F then
    if fontn == 'Bernhard Modern.ttf' then
      fontn = 'Cloister Black.ttf'
    elseif fontn == 'Cloister Black.ttf' then
      fontn = 'VAG Rounded.ttf'
    else
      fontn = 'Bernhard Modern.ttf'
    end
    s.canvas:clear()
    font:unload()
  elseif k == KEY_T then
    if text == text1 then
      text = text2
    elseif text == text2 then
      text = text3
    else
      text = text1
    end
  elseif k == KEY_D then
    write.debug = not write.debug
  elseif k == KEY_UP then
    lineh = math.min(lineh + 1, 600)
  elseif k == KEY_DOWN then
    lineh = math.max(lineh - 1, 0)
  end
  mousebox()
end

function mouse:on_wheelmove(z)
  if z > 0 then
    fonts = fonts + 1
    s.canvas:clear()
    font:unload()
    lineh = fonts*1.5
  else
    fonts = math.max(fonts - 1, 2)
    s.canvas:clear()
    font:unload()
    lineh = fonts*1.5
  end
  mousebox()
end

function mouse:on_press(b)
  if b == MBUTTON_LEFT then
    if align == 'left' then
      align = 'center'
    elseif align == 'center' then
      align = 'right'
    else
      align = 'left'
    end
  elseif b == MBUTTON_MIDDLE then
    if valign == 'top' then
      valign = 'middle'
    elseif valign == 'middle' then
      valign = 'bottom'
    else
      valign = 'top'
    end
  elseif b == MBUTTON_RIGHT then
    if wrap == 'none' then
      wrap = 'chars'
    elseif wrap == 'chars' then
      wrap = 'words'
    else
      wrap = 'none'
    end
  end
  mousebox()
end