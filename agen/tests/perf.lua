display:create("perf tests", 1000, 600, 32, true, 30, false, 4)

terminal = require('utils.log.terminal')
terminal.immediate = true

tests = {}
tests[1] = function(ntests)
  for i = 1, ntests do
  end
  terminal.print("iteration test")
  return true
end
tests[2] = function(ntests)
  local c = Canvas()
  for i = 1, ntests do
    c:square(1)
    c:fill()
  end
  terminal.printf("canvas:square()+canvas:fill()x%d (~%d verts)", ntests, ntests*6)
  return true
end
tests[3] = function(ntests)
  local c = Canvas()
  for i = 1, ntests do
    c:circle(1)
    c:fill()
    c:clear()
  end
  terminal.printf("canvas:circle()+canvas:fill()+canvas:clear()x%d (~%d verts)", ntests, ntests*32)
  return true
end
tests[4] = function(ntests)
  for i = 1, ntests do
    local s = Sprite()
    display.viewport:add_child(s)
  end
  display.viewport:remove_children()
  terminal.printf("display.viewport:add_child()x%d", ntests)
  return true
end

_test = 1
local ntests = 1
local results = {}

function mouse:on_wheelmove(z)
  if z > 0 then
    mouse:on_press(MBUTTON_MIDDLE)
  else
    mouse:on_press(MBUTTON_LEFT)
  end
end

function mouse:on_press(b)
  if b == MBUTTON_LEFT then
    ntests = ntests*2
  elseif b == MBUTTON_RIGHT then
    _test = _test + 1
    if _test > #tests then
      _test = 1
    end
    ntests = 1
  else
    ntests = math.max(ntests/2, 1)
  end
  terminal.clear()
  terminal.printf('TEST %d (RMB for next, MW to test)', _test)
  terminal.print("\n")
  terminal.printf('x%d tests (LMB/MW to change)', ntests)
  
  local t1 = system.get_ticks()
  local r = tests[_test](ntests)
  local t2 = system.get_ticks()
  
  if r then
    terminal.printf("PASSED in %d ticks", t2 - t1)
  else
    terminal.print("FAILED!")
  end
  
  results[_test] = results[_test] or {}
  results[_test][ntests] = t2 - t1
  terminal.print("\n")
  local list = {}
  for i, v in pairs(results[_test]) do
    table.insert(list, i)
  end
  table.sort(list)
  for i, v in ipairs(list) do
    local this = ""
    if v == ntests then
      this = "<---"
    end
    terminal.printf("x%d\t%d ticks %s", v, results[_test][v], this)  
  end
end

mouse:on_press(MBUTTON_MIDDLE)