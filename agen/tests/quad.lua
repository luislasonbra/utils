display:create("write", 800, 600, 32, true)

terminal = require("utils.log.terminal")

local quad = require("utils.agen.quad")

local s = Sprite()
display.viewport:add_child(s)
local i = Image("utils/agen/tests/cracked.png")

local style = 'scale'

local function quadbox(x, y, w, h)
  s.canvas:clear()
  if style == 'scale' then
    quad.source(i)
    quad.quad(s.canvas, x, y, w, h)
  else
    quad.source(i, 0, 0, w, h)
    quad.quad(s.canvas, x, y, w, h)
  end
end

local function mousebox()
  quadbox(-300, 200, mouse.xaxis + 300, -mouse.yaxis + 200)
end

function mouse:on_move(dx, dy)
  mousebox()
end

function mouse:on_press()
  if style == 'scale' then
    style = 'tile'
  else
    style = 'scale'
  end
  mousebox()
end
