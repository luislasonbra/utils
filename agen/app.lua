local tremove = table.remove

-- Platform variables
local _variables =
{
  -- app data directory
  private = system.get_private_dir(),
  -- platform
  platform = system.get_platform(),
  -- locale
  locale = system.get_locale(),
  -- user
  user = "user",
  -- counter
  frame = 0
}

-- Local user (platform dependent)
-- will probably not work with Unicode
local _user = os.getenv("USERNAME")
_user = _user or os.getenv("USER")
_user = _user or os.getenv("LOGNAME") -- Linux
_variables['user'] = _user or _variables['user']

local app = {}

local _errors = {}

--- Add new error
-- @param e Error
function app.error(e)
  local n = #_errors
  _errors[n + 1] = e
  -- sanity check
  if n > 256 then
    tremove(_errors, 1)
  end
end

--- Pop and return the last error
-- @return Error
function app.lasterror()
  return tremove(_errors)
end

--- Gets platform variable
-- @param k Key
-- @return Value
function app.getvar(k)
  return _variables[k]
end

--- Sets platform variable
-- @param k Key
-- @param v Value
function app.setvar(k, v)
  _variables[k] = v
end

--- Returns the private save directory in the format:
--- "private/filename"
-- @param fn Filename (optional)
-- @return Absolute path
function app.savepath(fn)
  fn = fn or ""
  local p = _variables['private']
  p = string.format("%s/%s", p, fn)
  -- replace native windows slash "\"
  p = string.gsub(p, "\\", "/")
  -- remove duplicate slashes
  p = string.gsub(p, "/+", "/")
  return p
end

app.timer = Timer()
function app.timer:on_tick()
  _variables['frame'] = _variables['frame'] + 1 
  if type(app.update) == "function" then
    local ms = app.timer:get_delta_ms()
    app.update(ms)
  end
end

--- Starts the update callback
-- @param ms Interval in milliseconds
function app.start(ms)
  _variables['frame'] = 0
  app.timer:start(ms, true)
  app.running = true
end

--- Stops the update callback
function app.stop()
  app.timer:stop()
  app.running = false
end
  
return app
