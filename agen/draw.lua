local pi = math.pi
local pi2 = pi*2
local cos = math.cos
local sin = math.sin

local draw = {}

local lg = LinearGradient()

-- Rectangle filled with a solid color
-- @param c canvas
-- @param x, y top-left point
-- @param w, h width and height
-- @param color color
-- @param alpha alpha (optional)
function draw.rect(c, x, y, w, h, color, alpha)
  alpha = alpha or 1
  c:move_to(x + w/2, y + h/2)
  c:rectangle(w, h)
  c:set_fill_style(color, alpha)
  c:fill()
end

--- Rectangle filled with a vertical linear gradient
-- @param c canvas
-- @param x, y top-left point
-- @param w, h width and height
-- @param tc, bc top color and bottom color
-- @param ta, ba top alpha and bottom alpha (optional)
function draw.vlg(c, x, y, w, h, tc, bc, ta, ba)
  ta = ta or 1
  ba = ba or 1
  c:move_to(x + w/2, y + h/2)
  c:rectangle(w, h)
  lg:stop_point1(x, y, tc, ta)
  lg:stop_point2(x, y + h, bc, ba)
  c:set_fill_style(lg)
  c:fill_gradient()
end

--- Rectangle filled with a horizontal linear gradient
-- @param c canvas
-- @param x, y top-left point
-- @param w, h width and height
-- @param tc, bc top color and bottom color
-- @param ta, ba top alpha and bottom alpha (optional)
function draw.hlg(c, x, y, w, h, tc, bc, ta, ba)
  ta = ta or 1
  ba = ba or 1
  c:move_to(x + w/2, y + h/2)
  c:rectangle(w, h)
  lg:stop_point1(x, y, tc, ta)
  lg:stop_point2(x + w, y, bc, ba)
  c:set_fill_style(lg)
  c:fill_gradient()
end

-- Polygon filled with a solid color
-- @param c canvas
-- @param p vertex list
-- @param color color
-- @param alpha alpha (optional)
function draw.polygon(c, p, color, alpha)
  alpha = alpha or 1
  c:move_to(p[1], p[2])
  for i = 3, #p, 2 do
    c:line_to(p[i], p[i + 1])
  end
  c:set_fill_style(color, alpha)
  c:fill()
end

-- Ellipse filled with a solid color
-- @param c canvas
-- @param x, y center position
-- @param rx, ry radius
-- @param color color
-- @param alpha alpha (optional)
-- @param s segments (optional)
function draw.ellipse(c, x, y, rx, ry, color, alpha, s)
  alpha = alpha or 1
  s = s or 32
  c:move_to(x + rx, y)
  for i = 1, s do
    local a = i/s*pi2
    local lx, ly = cos(a), sin(a)
    c:line_to(x + lx*rx, y + ly*ry)
  end
  c:close_path()
  c:set_fill_style(color, alpha)
  c:fill()
end

-- Circle filled with a solid color
-- @param c canvas
-- @param x, y center position
-- @param r radius
-- @param color color
-- @param alpha alpha (optional)
-- @param s segments (optional)
function draw.circle(c, x, y, r, color, alpha, s)
  draw.ellipse(c, x, y, r, r, color, alpha, s)
end

return draw