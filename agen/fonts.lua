--local codes = require("utils.lang.codes")

local fonts = {}
local _all = { en = {} }
local _enc = 'en'

--[[
function fonts.get_codes(...)
  local c = {}
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      if a ~= nil then
        assert(type(a) == "string", "block name must be a string")
        local b = codes[a]
        if b == nil then
          assert(false, "unknown block: " .. a)
        end
        for j = b[1], b[2] do
          assert(table.find(c, j) == nil, "code already included")
          table.insert(c, j)
        end
      end
    end
  end
  return c
end
]]

function fonts.set_encoding(enc)
  if _all[enc] == nil then
    _all[enc] = {}
  end
  _enc = enc
end

function fonts.get_encoding()
  return _enc
end

function fonts.load_file(fn, s, c)
  local e = _all[_enc]
  local f = e[fn]
  if f == nil then
    f = Font()
    local p, _, x = string.match(fn, "(.-)([^\\/]-%.?([^%.]*))$")
    p = string.gsub(p, "\\", "/")
    p = string.gsub(p, "/$", "")
    local l = false
    if x == "fnt" then
      l = f:load_bmfont(fn, p)
    else
      if c then
        l = f:load_file_codes(fn, s, c)
      else
        l = f:load_file(fn, s)
      end
    end
    -- loaded successfully?
    if l == false then
      return
    end
    -- index by filename
    e[fn] = f
  end
  return f
end

function fonts.load_system(sn, s, c)
  local e = _all[_enc]
  local f = e[fn]
  if f == nil then
    f = Font()
    local l = false
    if c then
      l = f:load_system_codes(sn, s, c)
    else
      l = f:load_system(sn, s)
    end
    -- loaded successfully?
    if l == false then
      return
    end
    -- index by filename
    e[fn] = f
  end
  return f
end

function fonts.unload(n)
  local e = _all[_enc]
  local f = e[n]
  if f and f:unload() == true then
    e[n] = nil
    return true
  end
  return false
end

function fonts.unload_all()
  for enc, e in pairs(_all) do
    for f in pairs(e) do
      fonts.unload(f)
    end
  end
end

function fonts.set_name(f2, n2)
  local e = _all[_enc]
  for n1, f1 in pairs(e) do
    if f1 == f2 then
      e[n2] = f2
      e[n1] = nil
      break
    end
  end
end

function fonts.find(f)
  local e = _all[_enc]
  if e then
    return e[f]
  end
end

return fonts