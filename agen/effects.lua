require('utils.agen.audio')

--- Utility for playing sound effects
--- An "effect" is represented by one or more sounds
--- When playing effects you choose which sound to play,
--- either by its numeric index or one of the keywords:
--- "random", "next", "previous", "first", "last"

local effects = {}

audio.effects = effects

local _all = {}
local _next = {}

--- Associates a sound file with an effect
--- An effect could have several sound filenames associated to it
-- @param n Effect name
-- @param f Sound filename
function effects.add(n, f)
  --assert(n, "effect name is nil")
  if _all[n] == nil then
    _all[n] = {}
  end
  --audio.load_sound(f)
  local e = _all[n]
  e[#e + 1] = f
  _next[n] = 0
end

--- Removes one or all sounds associated with an effect
-- @param n Effect name
-- @param f Sound filename (optional)
function effects.remove(n, f)
  --assert(n, "effect name is nil")
  local e = _all[n]
  if e == nil then
    return
  end
  for i = #e, 1, -1 do
    if e[i] == f then
      table.remove(e, i)
      break
    end
  end
  if f == nil or #e == 0 then
    _all[n] = nil
  end
end

--- Returns sound filename associated with an effect
-- @param n Effect name
-- @param i Sound file index
-- @return Sound filename
function effects.get(n, i)
  local e = _all[n]
  -- no sounds associated with this effect?
  if e == nil or #e == 0 then
    return
  end
  -- 
  if i == 'first' then
    i = 1
  elseif i == 'last' then
    i = #e
  elseif i == 'next' then
    i = _next[n] + 1
    if i > #e then
      i = 1
    end
  elseif i == 'previous' then
    i = _next[n] - 1
    if i < 1 then
      i = #e
    end
  else
    i = math.random(#e)
  end
  _next[n] = i
  return e[i]
end

--- Plays an effect
-- @param n Effect name
-- @param i Sound file index
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function effects.play(n, i, v, p, t)
  local fn = effects.get(n, i)
  if fn then
    return audio.play_sound(fn, v, p, t)
  end
end

--- Plays an effect in a loop
-- @param n Effect name
-- @param i Sound file index
-- @param v Volume
-- @param p Pan
-- @param t Pitch
-- @return Player object
function effects.loop(n, i, v, p, t)
  local fn = effects.get(n, i)
  if fn then
    return audio.loop_sound(fn, v, p, t)
  end
end

return effects