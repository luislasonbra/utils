lfs = lfs or require("lfs")
require("utils.io.path")

-- Gets the list of all files and directories recursively
-- @param path Directory path
-- @param out Table of files and directories (optional)
-- @return Table of files and directories
function lfs.get_rdir(path, out)
  out = out or {}
  -- remove backslashes
  path = string.gsub(path, "\\", "/")
  -- remove trailing slash
  path = string.gsub(path, "/$", "")
  -- scan directory
  for file in lfs.dir(path) do
    if file ~= "." and file ~= ".." then
      -- is this a directory?
      local full = file
      if path ~= "." then
        full = string.format("%s/%s", path, file)
      end
      out[#out + 1] = full
      local mode = lfs.attributes(full, "mode")
      if mode == "directory" then
        lfs.get_rdir(full, out)
      end
    end
  end
  return out
end

-- Iterates all files and directories recursively
-- @param path Directory path
-- @return Iterator providing filenames
function lfs.rdir(path)
  local files = lfs.get_rdir(path)
  local i = 0
  return function()
    i = i + 1
    return files[i]
  end
end

-- Returns the size of all files in a directory
-- @path path Directory path
-- @param s Initial size (optional)
-- @return Size in bytes
function lfs.get_dirsize(path, s)
  s = s or 0
  -- scan directory
  for file in lfs.dir(path) do
    if file ~= "." and file ~= ".." then
      -- is this a directory?
      local full = string.format("%s/%s", path, file)
      local attr = lfs.attributes(full)
      if attr.mode == "directory" then
        s = lfs.get_dirsize(full, s)
      else
        s = s + attr.size
      end
    end
  end
  return s
end

-- Create directory from a path recursively
-- @param path Directory path
-- @return True if successful or false otherwise
function lfs.mkrdir(path)
  -- parse sub directories
  local ptb = io.parse(path)
  local psz = ptb[1]
  for i = 1, #ptb do
    local a = lfs.attributes(psz)
    -- check if directory exists
    if a == nil or a.mode ~= "directory" then
      -- attempt to create directory
      if lfs.mkdir(psz) == false then
        return false
      end
    end
    if i < #ptb then
      psz = string.format("%s/%s", psz, ptb[i + 1])
    end
  end
  return true
end