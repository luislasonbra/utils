local sub = string.sub
local gmatch = string.gmatch

--- Line iterator, similar to io.lines
--- Captures do not end with the new line character
-- @param s string
-- @return lines iterator
function string.lines(s)
  -- type checking
  assert(type(s) == "string", "first argument must be a string")
  if sub(s, -1) ~= "\n" then
    s = s .. "\n"
  end
  return gmatch(s, "(.-)[\r]?\n")
end