--[[Copyright 2011 Bart van Strien. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY BART VAN STRIEN ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BART VAN STRIEN OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Bart van Strien.

The above license is known as the Simplified BSD license.]]

local inifile = {}

--- Converts ini string to table
-- @param sz ini string
-- @param t table (optional)
-- @return table
function inifile.eval(sz, t)
	t = t or {}
	local section
  for line in (sz .. '\n'):gmatch("(.-)[\r]?\n") do
		local s = line:match("^%[([^%]]+)%]$")
		if s then
      if type(t[s]) ~= 'table' then
        t[s] = {}
      end
			section = t[s]
		else
      local key, value = line:match("^([%w_]+)%s-=%s-(.+)$")
      if key and value then
        if value == "true" then
          value = true
        elseif value == "false" then
          value = false
        else
          value = tonumber(value) or value
        end
        if section then
          section[key] = value
        else
          t[key] = value
        end
      end
		end
	end
  return t
end

--- Converts table to ini string
-- @param t table
-- @return ini string
function inifile.dump(t)
	local out = {}
  local n = 1
  -- non-sectioned
	for key, value in pairs(t) do
    if type(value) ~= "table" then
      value = tostring(value)
      out[n] = ("%s=%s"):format(key, value)
      n = n + 1
    end
	end
  -- sectioned
	for s, section in pairs(t) do
    if type(section) == "table" then
      out[n] = ("\n[%s]"):format(s)
      n = n + 1
      for key, value in pairs(section) do
        value = tostring(value)
        out[n] = ("%s=%s"):format(key, value)
        n = n + 1
      end
    end
	end
  return table.concat(out, "\n")
end

--- Loads ini file
-- @param fn filename
-- @return table
function inifile.load(name)
  local f = io.open(name, "r")
  if f then
    local contents = f:read("*a")
    f:close()
    return inifile.eval(contents)
  end
end

--- Saves ini file
-- @param fn filename
-- @param table
-- @return true if successful
function inifile.save(name, t)
  local f = io.open(name, "w")
  if f then
    local contents = inifile.dump(t)
    f:write(contents)
    f:close()
    return true
  end
  return false
end

return inifile
