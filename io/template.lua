--- Template builder
-- Allows embedding Lua code similarly to PHP
-- Example:
-- <table>
-- <?lua for i = 1, 5 do ?>
--  <tr>
--   <td><?lua print(i) ?></td>
--  </tr>
-- <?lua end ?>
-- </table>

local find = string.find
local sub = string.sub
local format = string.format
local concat = table.concat

local function tpl(sz, ...)
  local l = string.len(sz)
  local i = 1
  -- todo: handle nested quotes
  local code = { [[local _output = {}
local _args = {}
local function print(sz, ...)
  -- handle non-string values
  sz = tostring(sz) or "nil"
  -- handle multiple arguments
  local nargs = select('#', ...)
  if nargs > 0 then
    _args[1] = sz
    for i = 1, nargs do
      local a = select(i, ...)
      _args[i + 1] = tostring(a)
    end
    sz = concat(_args, ", ")
    -- clear arguments table
    for i = nargs + 1, 1, -1 do
      _args[i] = nil
    end
  end
  _output[#_output + 1] = sz
end
local function printf(sz, ...)
  -- handle non-string values
  sz = tostring(sz) or "nil"
  -- handle format parameters
  if select('#', ...) > 0 then
    sz = string.format(sz, ...)
  end
  print(sz)
end
local function include(fn)
  local f = io.open(fn)
  if f then
    local sz = f:read("*a")
    print(sz)
  end
end]] }
  while i < l do
    local i1 = find(sz, "<?lua", i, true)
    if i1 then
      local i2 = find(sz, "?>", i1 + 5, true)
      assert(i2)
      local s1 = sub(sz, i, i1 - 1)
      local s2 = sub(sz, i1 + 5, i2 - 1)
      -- todo: nested double quotes
      code[#code + 1] = format('print([[%s]])', s1)
      code[#code + 1] = s2
      i = i2 + 2
    else
      local s1 = sub(sz, i)
      code[#code + 1] = format('print([[%s]])', s1)
      i = l
    end
  end
  code[#code + 1] = "return table.concat(_output)"
  local c = concat(code, "\n")
  local f, e = loadstring(c)
  assert(f, e)
  return f(...)
end

return tpl