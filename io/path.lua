--- Checks if a file exists
--- Will not work with directories
-- @param fn Filename
-- @return True if the file exists
function io.fexists(fn)
  local f = io.open(fn, "r")
  if f == nil then
    return false
  end
  f:close()
  return true
end

--- Checks if a file is writable
--- Will not work with directories
-- @param fn Filename
-- @return True if the file is writable
function io.fwriteable(fn)
  local e = io.fexists(fn)
  local f = io.open(fn, "a")
  if f == nil then
    return false
  end
  f:close()
  if e == false then
    os.remove(fn)
  end
  return true
end

function io.copy(from, to)
  -- open current save
  local f1 = io.open(from, 'r')
  if f1 == nil then
    return false
  end
  local sz = f1:read("*a")
  f1:close()
  -- save
  local f2 = io.open(to, 'w')
  if f2 == nil then
    return false
  end
  local r = f2:write(sz)
  f2:close()
  return r == true
end

--- Splits a filename into directory, filename and extension
--- Assumes the the filename does not end with a slash
--- Adapted from Paul Kulchenko
-- @param fn Filename
-- @return Directory path, filename and extension
function io.split(fn)
  return string.match(fn, "(.-)([^\\/]-%.?([^%.]*))$")
end

--- Returns the extension from a filename, ex: "lua"
-- @param fn Filename
-- @return File extension or nil
function io.ext(fn)
  return string.match(fn, "%.([^%./]+)$")
end

--- Parses a filename into a list of directories
--- For absolute paths, the first value in the returned table is:
--- the root folder, ex: "/root" or
--- the drive letter, ex: "C:"
-- @param p Filename
-- @return Table and absolute path boolean
function io.parse(p)
  -- todo: massive refactoring needed
  -- todo: preserve ending slash
  
  -- replace back slashes "\"
  p = string.gsub(p, "\\", "/")
  -- remove empty directories "/ /"
  p = string.gsub(p, "/[%s]+/", "/")
  p = string.gsub(p, "^[%s]+/", "/")
  p = string.gsub(p, "/[%s]+$", "/")
  -- remove duplicate forward slashes "//"
  p = string.gsub(p, "/+", "/")

  -- handle absolute paths
  -- windows drive letter, ex: "C:"
  local d = string.match(p, "^([%a]:)")
  if d == nil then
    -- OSX leading slash, ex: "/drive1"
    -- Unix root directory, ex: "/root"
    d = string.match(p, "^(/[^/]+)[/]?")
  end
  if d then
    local l = string.len(d)
    p = string.sub(p, l + 1)
  end

  -- remove disallowed characters :*?"<>|
  -- p = string.gsub(p, "[:*?\"<>|]", "")

  -- list directories
  local ptb = {}
  for t in string.gmatch(p, "[^/]+") do
    if string.gsub(t, "[%s%.]*", "") == "" then
      -- remove white spaces, ex: " .. "
      t = string.gsub(t, "%s", "")
      -- remove more than three consecutive dots, ex: "..."
      t = string.gsub(t, "^%.%.[%.]+$", ".")
    end
    if t ~= "" then
      table.insert(ptb, t)
    end
  end

  -- remove unnecessary "../" and "./"
  local i = 1
  while i <= #ptb do
    if ptb[i] == ".." then
      -- "directory/.." or "C:/.." or "/root/.."
      if ptb[i - 1] ~= ".." then
        if i > 1 or d ~= nil then
          table.remove(ptb, i)
          i = i - 1
          if i >= 1 or d ~= nil then
            table.remove(ptb, i)
            i = i - 1
          end
        end
      end
    elseif ptb[i] == "." then
      table.remove(ptb, i)
      i = i - 1
    end
    i = i + 1
  end
  
  -- for absolute paths, add root or drive letter
  if d then
    table.insert(ptb, 1, d)
  end
  
  -- empty path?
  if #ptb == 0 then
    table.insert(ptb, ".")
  end
  
  return ptb, d ~= nil
end

-- Iterates directories in a given filename
-- @param p Filename
-- @return Iterator providing pairs of name and path
function io.paths(p)
  local ptb, a = io.parse(p)
  local i = 0
  local full
  return function()
    i = i + 1
    if ptb[i] == nil then
      return
    end
    if full == nil then
      full = ptb[i]
    else
      full = string.format("%s/%s", full, ptb[i])
    end
    return ptb[i], full
  end
end

--- Sanitizes a filename
--- 1. Replaces back slash (\)
--- ex: "c:\folder" becomes "c:/folder"
--- UTF-8 filenames containing the back slash (\) should be careful
--- ex: "c:\утф8\абв" becomes "c:/утф8абв"
--- ex: "c:\\утф8\\абв" becomes "c:/утф8/абв"
--- 2. Removes ending slash
--- ex: "c:/folder/" becomes "c:/folder"
--- 3. Removes duplicate slashes (//)
--- ex: "/root//folder" becomes "/root/folder"
--- 4. Removes unnecesary "." and ".."
--- ex: "folder/.." becomes "."
--- ex: "./../../" becomes "../.."
--- ex: "c:/.." becomes "c:"
--- ex: "/root/.." becomes "/root"
--- ex: ".../" becomes "."
--- 5. Trims white spaces
--- ex: " /dir" becomes "/dir"
--- ex: "/ /dir" becomes "/dir"
--- ex: " dir/ /" becomes " dir"
--- ex: ".. / .." becomes "../.."
--- ex: "/ /" becomes "."

--- Disabled since it varies across platforms:
--- 6. Removes disallowed characters (:*?"<>|)
--- ex: "c:/folder?/" becomes "c:/folder/"

-- @param p Filename
-- @return Sanitized filename
function io.sanitize(p)
  -- todo: preserve ending slash
  local ptb = io.parse(p)
  return table.concat(ptb, '/')
end

--[[
-- Tests
local input =
{
  -- input, expected output
  -- forward slash "\" is replaced with "/"
  "c:\\folder", "c:/folder",
  "c:\утф8\абв", "c:/утф8абв",
  "c:\\утф8\\абв", "c:/утф8/абв",
  -- final slash "/" is removed
  "c:/folder/", "c:/folder",
  -- "//" is replaced with "/"
  "/root//folder", "/root/folder",
  -- relative path "folder/" is ignored when followed by ".."
  "folder/..", ".",
  -- "." is ignored
  "./../../", "../..",
  -- ".." is ignored when preceeded by a drive letter or root directory
  "c:/..", "c:",
  "/root/..", "/root",
  -- "..." is ignored
  ".../", ".",
  " /dir", "/dir",
  "/ /dir", "/dir",
  -- directories " dir" can have leading spaces
  " dir/ /", " dir",
  -- spaces in " .. / .. " are trimmed
  ".. / ..", "../..",
  -- empty dir "/ /" is ignored
  "/ /", ".",
  -- "..." and "." are ignored
  ".../.././", ".."
}
for i = 1, #input, 2 do
  local out = io.sanitize(input[i])
  local sz = string.format("%d. '%s' ~= '%s'", (i + 1)/2, out, input[i + 1])
  assert(out == input[i + 1], sz)
end
]]

--- Returns a path where application data can be stored
-- @return Directory path
function io.appdata()
  -- Windows 7/8: "%SystemDrive%\Users\{username}\AppData\Local"
  local path = os.getenv("LOCALAPPDATA")
  -- Windows 7/8: "%SystemDrive%\Users\{username}\AppData\Roaming"
  -- Windows XP: "%SystemDrive%\Documents and Settings\{username}\Application Data"
  path = path or os.getenv("APPDATA")
  -- Linux: "/home/myuser1"
  -- OSX
  path = path or os.getenv("HOME")
  if path then
    return io.sanitize(path)
  end
end

--- Returns a path where temporary files can be stored
-- @return Directory path
function io.tempdir()
  -- Windows 7/8: "%SystemDrive%\Users\{username}\AppData\Local\Temp"
  -- Windows XP: "%SystemDrive%\Documents and Settings\{username}\Local Settings\Temp"
  local path = os.getenv("TEMP")
  path = path or os.getenv("TMP")
  -- Linux: "/var/tmp"
  -- OSX
  path = path or os.getenv("TMPDIR")
  if path then
    return io.sanitize(path)
  end
end

function io.getos()
  if package.config:sub(1,1) == '\\' then
    return 'win32'
  elseif (io.popen("uname -s"):read'*a') == "Darwin" then
    return 'osx'
  else
    return 'linux'
  end
end

function io.url(url)
  local pf = io.getos()
  -- Windows:
  local cmd = 'start %s'
  if pf == 'linux' then
    -- Linux:
    cmd = 'xdg-open "%s"'
  elseif pf == 'osx' then
    -- OSX:
    cmd = 'open %s'
  end
  cmd = string.format(cmd, url)
  os.execute(cmd)
end