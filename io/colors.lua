local colors = {}

-- Converts hex (0xFFFFFF) to red, green and blue
-- @param rgb hex (0x000000-0xFFFFFF)
-- @return red, green and blue color components (0-255)
function colors.hex_to_rgb(rgb)
  -- clamp between 0x000000 and 0xffffff
  rgb = rgb%0x1000000 -- 0xffffff + 1

  -- extract each color
  local b = rgb%0x100 -- 0xff + 1 or 256
  local g = (rgb - b)%0x10000 -- 0xffff + 1
  local r = (rgb - g - b)
  -- shift right
  g = g/0x100 -- 0xff + 1 or 256
  r = r/0x10000 -- 0xffff + 1

  return r, g, b
end

-- Converts string hex ("0xFFFFFF") to red, green and blue
-- @param sz string hex ("0x000000"-"0xFFFFFF")
-- @return red, green and blue color components (0-255)
function colors.shex_to_rgb(sz)
  local n = tonumber(sz)
  return colors.hex_to_rgb(n)
end

-- Converts string hex ("0xFFFFFF") to red, green and blue
function colors.rgb_to_hex(r, g, b)
  return r*0x10000 + g*0x100 + b
end

-- Tests
--[[
-- rgb => hex
local hex = colors.rgb_to_hex(0, 255, 0)
assert(hex == 0x00FF00)
-- hex => rgb
local r, g, b = colors.hex_to_rgb(0x00FF00)
assert(r == 0 and g == 255 and b == 0)
-- rgb => hex => rgb
for i = 1, 1000 do
  -- range 0 - 255
  local r = math.random(255)
  local g = math.random(255)
  local b = math.random(255)
  local hex = colors.rgb_to_hex(r, g, b)
  local r2, g2, b2 = colors.hex_to_rgb(hex)
  assert(r2 >= 0 and r2 <= 255)
  assert(g2 >= 0 and g2 <= 255)
  assert(b2 >= 0 and b2 <= 255)
  assert(r == r2)
  assert(g == g2)
  assert(b == b2)
end
-- hex => rgb => hex
for i = 1, 1000 do
  -- range 0x000000 - 0xFFFFFF
  local hex = math.random()*0xFFFFFF
  local r, g, b = colors.hex_to_rgb(hex)
  local hex2 = colors.rgb_to_hex(r, g, b)
  assert(hex == hex2)
end
]]

return colors