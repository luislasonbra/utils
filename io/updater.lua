assert(base64, "base64 module required")
assert(http, "luasocket module required")
assert(lfs.rdir, "lfs module required")
assert(md5, "md5 module required")

updater = {}

updater.date_format = "%Y/%m/%d %H:%M:%S"
updater.date_pattern = "(%d%d%d%d)/(%d%d)/(%d%d) (%d%d):(%d%d):(%d%d)"

updater.tempdir = "updates"
updater.version = ""
updater.platform = system.get_platform()

-- temp directory where downloaded files are stored
updater.set_tempdir = function(dir)
  assert(dir ~= "" and dir ~= ".", "Cannot store temp files in root directory")
  -- tempdir should not end in slash "/"
  while string.byte(dir, -1) == 47 do
    dir = string.sub(dir, 1, -2)
  end
  updater.tempdir = dir
end

updater.set_version = function(v)
  updater.version = v
end

updater.set_platform = function(p)
  updater.platform = p
end

updater.date_sz = function(ts)
  return os.date(updater.date_format, ts)
end

updater.date_table = function(sz)
  if sz == nil then
    return
  end
  local Y, m, d, H, M, S = string.match(sz, updater.date_pattern)
  Y, m, d = tonumber(Y), tonumber(m), tonumber(d)
  H, M, S = tonumber(H), tonumber(M), tonumber(S)
  if Y == nil or m == nil or d == nil or H == nil or M == nil or S == nil then
    return
  end
  return { tz = "UTC", year = Y, month = m, day = d, hour = H, min = M, sec = S }
end

--[[
-- make a table of local files, indexed by filename
updater.get_files = function(dir, files)
  files = files or {}
  -- scan
  for full in lfs.rdir(dir) do
    local attr = lfs.attributes(full)
    if attr and attr.mode == "file" then
      local f = {}
      f.name = full
      f.modified = updater.date_sz(attr.modification)
      f.checksum = updater.get_file_cs(full)
      f.size = attr.size
      files[full] = f
    end
  end
  return files
end
]]

updater.get_file_cs = function(filename)
  local f = io.open(filename, "rb")
  if f then
    local sz = f:read("*all")
    f:close()
    return md5.sumhexa(sz)
  end
end

-- download a list of available updates
updater.get_updates = function(url)
  local version = updater.version
  local platform = updater.platform
  local u = string.format("%s?version=%s&platform=%s", url, version, platform)
  local b, c = http.request(u)
  if b == nil then --c ~= 200 or b == nil then
    return
  end

  -- convert the returned string to a list
  local lines = {}
  for i in string.gmatch(b, "[^\r\n]+") do
    table.insert(lines, i)
  end
  
  -- check the first line string
  local first = table.remove(lines, 1)
  if first ~= "Available updates:" then
    return {}
  end
  
  -- convert the list to a table
  local updates = {}
  for i = 1, #lines, 3 do
    local f = lines[i]
    local t = lines[i + 1]
    local c = lines[i + 2]
    if f and t and c then
      updates[f] = { name = f, modified = t, checksum = c }
    end
  end
  return updates
end

-- compare local files vs available updates
updater.check = function(updates)
  -- remove files which are not up to date

  -- iterate available updates
  for filename, file in pairs(updates) do
    local m1 = file.modified
    local m2 = lfs.attribute(filename, "modification") --files[filename].modified
    local dt1 = updater.date_table(m1)
    local dt2 = updater.date_table(m2)
    -- does this file exist locally?
    if dt1 and dt2 then
      local Y, m, d = dt1.year, dt1.month, dt1.day
      local H, M, S = dt1.hour, dt1.min, dt1.sec
      
      local Y2, m2, d2 = dt2.year, dt2.month, dt2.day
      local H2, M2, S2 = dt2.hour, dt2.min, dt2.sec
      -- is the online version of the file newer?
      local a1, a2 = tonumber(Y1 .. m1 .. d1), tonumber(H1 .. M1 .. S1)
      local b1, b2 = tonumber(Y2 .. m2 .. d2), tonumber(H2 .. M2 .. S2)
      if b1 > a1 or (b1 == a1 and b2 >= a2) then
        updates[filename] = nil
      end
    end
  end
  
  -- remove files which have already been downloaded to the tempdir
  -- and have the same checksum
  for filename, file in pairs(updates) do
    local fn = string.format("%s/%s", updater.tempdir, filename)
    local cs1 = updater.get_file_cs(fn)
    local cs2 = file.checksum
    if cs1 == cs2 then
      updates[filename] = nil
    end
  end
  
  -- build the download queue
  local download = {}
  for _, v in pairs(updates) do
    table.insert(download, v)
  end
  
  return download
end

updater.download_file = function(url, file)
  local version = updater.version
  local platform = updater.platform
  
  -- url encode the filename
  local fileurl = string.gsub(file.name, "\n", "\r\n")
  fileurl = string.gsub (fileurl, "([^%w %-%_%.%~])",
      function (c) return string.format ("%%%02X", string.byte(c)) end)
  fileurl = string.gsub (fileurl, " ", "+")
  
  -- download the file
  local u = string.format("%s?version=%s&platform=%s&filename=%s", url, version, platform, fileurl)
  local b, c = http.request(u)
  if c ~= 200 or b == nil then
    return false, "connection error"
  end
  
  -- decode request body
  b = base64.dec(b)

  -- save to file
  local fn = string.format("%s/%s", updater.tempdir, file.name)
  if updater.write_file(fn, b, "wb") == false then
    return false, "could not save file"
  end
  
  -- checksum verification
  local cs1 = updater.get_file_cs(fn)
  local cs2 = file.checksum
  if cs1 ~= cs2 then
    return false, "checksum error"
  end
  
  -- update the file modification time
  local t = file.modified
  local tt = updater.date_table(t)
  local stamp = os.time(tt)
  lfs.touch(fn, stamp, stamp)
  
  return true
end

updater.write_file = function(f, sz, flag)
  -- check if the path is writeable
  flag = flag or "wb"
  local p = lfs.fopen(f, flag)
  if p == nil then
    return false
  end
  p:write(sz)
  p:close()
  return true
end
