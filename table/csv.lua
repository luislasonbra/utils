

--- Converts list to a CSV representation
--- Works only with numerically indexed tables
--- "nil" values are included too, therefore:
--- table.tocsv({[5] = true}) ==> ",,,,true"
-- @param t Table
-- @param c Number of columns (optional)
-- @return String
function table.tocsv(t, c)
  local t2 = {}
  -- highest index including gaps
  local i2 = 0
  for i, v in pairs(t) do
    if type(i) == "number" then
      if floor(i) == i and i > 0 then
        if i > i2 then
          i2 = i
        end
      end
    end
  end
  c = c or i2
  -- optional: type checking
  assert(type(t) == "table", "first argument must be a table")
  assert(type(c) == "number", "second argument must be a number")
  assert(c > 0, "number of columns is zero")
  -- number of rows
  local r = ceil(i2/c)
  for i = 1, i2 do
    local v = t[i]
    local vt = type(v)
    if vt == "string" then
      -- handle double quotes and new line
      v = toQuotedString(v)
    elseif vt == "nil" then
      v = ""
    else
      v = tostring(v)
    end
    insert(t2, v)
    if i < r*c then
      if c > 0 and i%c == 0 then
        insert(t2, "\n")
      else
        insert(t2, ",")
      end
    end
  end
  for i = 1, r*c - i2 - 1 do
    insert(t2, ",")
  end
  insert(t2, "\n")
  return concat(t2)
end

--- Converts a CSV string to a table
--- Adapted from: http://lua-users.org/wiki/CsvUtils
-- @param sz String
-- @return Table
function table.fromcsv(sz)
  assert(type(sz) == "string", "input is not a string")
  local t = {}
  local tn = 0
  local si, ei = 1, len(sz)
  while si < ei do
    -- single line
    local nl = find(sz, "\n", si, true) or ei
    local line = sub(sz, si, nl)
    si = nl + 1

    -- ending comma
    line = line .. ','
    local fs = 1
    repeat
      local f = nil
      if find(line, '^"', fs) then
        -- quoted
        local a, c
        local i = fs
        repeat
          -- find closing quote
          a, i, c = find(line, '"("?)', i + 1)
          -- quote not followed by quote?
        until c ~= '"'
        assert(i, 'unmatched double quote')
        f = sub(line, fs + 1, i - 1)
        f = gsub(f, '""', '"')
        fs = find(line, ',', i, true) + 1
      else
        -- unquoted
        -- find next comma
        local nexti = find(line, ',', fs, true)
        f = sub(line, fs, nexti - 1)
        if f == "" then
          f = nil
        elseif f == "true" then
          f = true
        elseif f == "false" then
          f = false
        else
          f = tonumber(f) or f
        end
        fs = nexti + 1
      end
      tn = tn + 1
      t[tn] = f
    until fs > len(line)
  end
  return t
end