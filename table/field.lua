-- Allows access to table fields
-- References
-- Roberto: http://www.lua.org/pil/14.1.html

local len = string.len
local format = string.format
local byte = string.byte
local gsub = string.gsub
local gmatch = string.gmatch

local remove = table.remove
local sort = table.sort

--- Asserts that the string is a valid field
-- @param f Field
local function assertf(f)
  -- must be a string
  if type(f) ~= "string" then
    error(format("field is not a string '%s'", tostring(f)))
  end
  -- must have length greater than 0
  local l = len(f)
  if l == 0 then
    error("field is empty")
  end
  -- cannot end with the percent sign ('%')
  -- cannot end with the dot symbol ('.') except for ('%.')
  local lc = byte(f, l)
  local nlc = byte(f, l - 1)
  if (lc == 46 and nlc ~= 37) or (lc == 37) then
    error(format("invalid termination character in field '%s'", f))
  end
end

--- Accesses the table using a field
-- @param t Table
-- @param f Field
-- @return Value
local function getfield(t, f)
  --local p = type(t)
  --assert(p == "table" or p == "userdata", "first argument must be a table or userdata")
  assertf(f)
  -- handle special sequence ('%.')
  f = gsub(f, "%%%.", "\001")
  for k, d in gmatch(f, "([^%.]+)(%.?)") do
    k = gsub(k, "\001", ".")
    -- allow numeric key access
    if t[k] == nil then
      local nk = tonumber(k)
      if nk and t[nk] then
        k = nk
      end
    end
    -- check if key exists
    if d == "." then
      local tk = type(t[k])
      if tk ~= "table" and tk ~= "userdata" then
        return
      end
    end
    -- descend to nested table or userdata
    t = t[k]
  end
  return t
end

--- Assigns the given value using a field
--- Overwrites values and creates sub-tables if necessary
-- @param t Table
-- @param f Field
local function setfield(t, f, v)
  -- optional: type checking
  --local p = type(t)
  --assert(p == "table" or p == "userdata", "first argument must be a table or userdata")
  assertf(f)
  -- handle special sequence ('%.')
  f = gsub(f, "%%%.", "\001")
  for k, d in gmatch(f, "([^%.]+)(%.?)") do
    k = gsub(k, "\001", ".")
    -- allow numeric key access
    if t[k] == nil then
      local nk = tonumber(k)
      if t[nk] then
        k = nk
      end
    end
    if d == "." then
      -- create nested table if non-existent
      local tk = type(t[k])
      if tk ~= "table" and tk ~= "userdata" then
        t[k] = {}
      end
      -- descend to nested table or userdata
      t = t[k]
    else
      -- assign value
      t[k] = v
    end
  end
end

-- queue used for breadth first iteration
local _queue = {}
-- temporary lookup for visited tables
local _visited = {}
local _fields = {}

--- Returns a list of all fields in a table
--- Breadth first traversal provides the shortest keys
--- Ignores cycles
-- @param t Table
-- @param field Output table (optional)
-- @return List of fields
local function getfields(t, fields)
  fields = fields or {}
  -- optional: type checking
  --local p = type(t)
  --assert(p == "table" or p == "userdata", "first argument must be a table or userdata")
  --assert(type(fields) == "table", "second argument must be a table")
  -- queue
  _queue[#_queue + 1] = t
  while #_queue > 0 do
    -- dequeue
    local c = remove(_queue, 1)
    -- queue child nodes
    _visited[c] = true
    for k, v in pairs(c) do
      -- supported types: string or number
      local kt = type(k)
      if kt == 'string' or kt == 'number' then
        local f = tostring(k)
        -- handle special sequence ('%.')
        f = gsub(f, "%.", "%%%.")
        local p = _fields[c]
        if p then
          f = format("%s.%s", p, f)
        end
        fields[#fields + 1] = f
        if type(v) == "table" then
          if not _visited[v] then
            _fields[v] = f
            _queue[#_queue + 1] = v
          end
        end
      end
    end
  end
  -- clear the lookup tables
  for k, v in pairs(_visited) do
    _visited[k] = nil
  end
  for k, v in pairs(_fields) do
    _fields[k] = nil
  end
  -- optional: alphabetic sort
  -- todo: "1.10" is sorted above "1.2"
  sort(fields)
  return fields
end

--- Returns an iterator providing pairs of concatenated key and value
-- @param t Table
-- @return Iterator
local function iterate(t)
  -- optional: type checking
  --local p = type(t)
  --assert(p == "table" or p == "userdata", "first argument must be a table or userdata")
  -- build list
  local fields = getfields(t)
  local i = 0
  return function()
    i = i + 1
    if i > #fields then
      return
    end
    return fields[i], getfield(t, fields[i])
  end
end

table.get = getfield
table.set = setfield
table.getf = getfields
table.fields = iterate