local sub = string.sub
local gsub = string.gsub
local char = string.char
local find = string.find
local rep = string.rep
local format = string.format
local insert = table.insert
local remove = table.remove

local function toXMLString(sz)
  sz = gsub(sz, "&", "&amp;")
  sz = gsub(sz, "<", "&lt;")
  sz = gsub(sz, ">", "&gt;")
  --sz = gsub(sz, "'", "&apos;")
  sz = gsub(sz, "\"", "&quot;")
  -- replace non printable char -> "&#xD;"
  sz = gsub(sz, "([^%w%&%;%p%\t% ])",
  function (c) 
    return format("&#x%X;", byte(c)) 
    --return format("&#x%02X;", byte(c)) 
    --return format("&#%02d;", byte(c)) 
  end)
  return sz
end

local function fromXMLString(sz)
  sz = gsub(sz, "&#x([%x]+)%;",
  function(h) 
    return char(tonumber(h,16)) 
  end)
  sz = gsub(sz, "&#([0-9]+)%;",
  function(h) 
    return char(tonumber(h,10)) 
  end)
	sz = gsub(sz, "&quot;", "\"")
	sz = gsub(sz, "&apos;", "'")
	sz = gsub(sz, "&gt;", ">")
	sz = gsub(sz, "&lt;", "<")
	sz = gsub(sz, "&amp;", "&")
	return sz
end

--- Adapted from Roberto Ierusalimschy: http://lua-users.org/wiki/LuaXml
function table.fromxml(s)
  assert(type(s) == "string", "input is not a string")
  -- replace prologue
  s = gsub(s, "<%?(.*)%?>", "")
  -- replace doctype
  --s = gsub(s, "<!(.*)%>", "")
  -- replace comments
  s = gsub(s, "<!%-%-(.-)%-%->", "")
  
  local stack = {}
  local top = {}
  insert(stack, top)
  local i = 1
  while true do
    local ni, j, c, tag, xarg, empty = find(s, "<(%/?)([%w:]+)(.-)(%/?)>", i)
    if not ni then
      break
    end
    local text = sub(s, i, ni - 1)
    if not find(text, "^%s*$") then
      insert(top, text)
    end
    if empty == "/" or c == "" then
      -- start tag
      -- store the tag name in the zero index [0]
      local t = { [0] = tag }
      -- load attributes
      gsub(xarg, "([%-%w]+)=([\"'])(.-)%2",
      function (w, _, a)
        t[w] = fromXMLString(a)
      end)
      if empty == "/" then
        -- empty
        insert(top, t)
      else
        -- non-empty
        top = t
        -- new level
        insert(stack, top)
      end
    else
      -- end tag
      local toclose = remove(stack)
      -- remove top
      top = stack[#stack]
      if #stack < 1 then
        error("nothing to close with " .. tag)
      end
      if toclose[0] ~= tag then
        error("trying to close " .. toclose[0] .. " with " .. tag)
      end
      insert(top, toclose)
    end
    i = j + 1
  end
  local text = sub(s, i)
  if not find(text, "^%s*$") then
    insert(stack[#stack], text)
  end
  if #stack > 1 then
    error("unclosed " .. stack[#stack][0])
  end
  return stack[1]
end

function table.toxml(t, g, i, i2)
  g = g or t[0] or "table"
  g = tostring(g)
  if not match(g, '^[%a_][%w_]*$') then
    g = type(t)
  end
  i = i or 0
  -- optional: type checking
  assert(type(t) == "table", "first argument must be a table")
  assert(type(g) == "string", "second argument must be a string")

  -- build a list of attributes and sub-tags
  local t1 = {}
  local t2 = {}
  for k, v in pairs(t) do
    local kt = type(k)
    local vt = type(v)
    if vt ~= "table" then
      v = toXMLString(v)
      v = toQuotedString(v)
      t1[#t1 + 1] = format('%s=%s', k, v)
    else
      --if kt == "string" then --and match(k, '^[%a_][%w_]*$') then
        t2[#t2 + 1] = table.toxml(v, k, i + 1)
      --end
    end
  end
  local a = ""
  if #t1 > 0 then
    a = " " .. concat(t1, " ")
  end
  local f1 = rep("  ", i)
  local f2 = rep("  ", i + 1)
  if #t2 == 0 then
    return format("%s<%s%s />", f1, g, a)
  end
  -- indent all key and value pairs
  local tsz = f2 .. concat(t2, "\n" .. f2)
  -- add brackets
  if i2 or i == 0 then
    return format("<%s%s>\n%s\n%s</%s>", g, a, tsz, f1, g)
  else
    return format("\n%s<%s%s>\n%s\n%s</%s>", f1, g, a, tsz, f1, g)
  end
end
