local function parse(b)
  -- evaluate and parse
  local r, x = pcall(xml.eval, b)
  if r == false or x == nil then
    return "could not evaluate results"
  end
  local board = xml.find(x, 'Board')
  if board == nil then
    return "no entries found"
  end
  -- convert XML to list
  local list = {}
  for i = 1, #board do
    local r = board[i]
    local tag = xml.tag(r)
    if tag == "Entry" then
      local entry =
      {
        userid = r.userid,
        name = r.name,
        rank = r.rank,
        score = tonumber(r.score),
        country = r.country
      }
      list[i] = entry
    end
  end
  return list
end

-- url encode
local encode_sz = function(sz)
  local sz = string.gsub(sz, "\n", "\r\n")
  sz = string.gsub (sz, "([^%w %-%_%.%~])",
      function (c) return string.format ("%%%02X", string.byte(c)) end)
  sz = string.gsub (sz, " ", "+")
  return sz
end

local function download(url, b, s, e, t)
  -- request a leaderboard
  b = encode_sz(b)
  local u = string.format("%s?b=%s&s=%d&e=%d&f=%s", url, b, s, e, t)
  local b, c = http.request(u)
  if b == nil or c == nil or c ~= 200 then
    return "http request failed"
  end
  return parse(b)
end

local function donloadFor(url, b, u)
  -- request a particular entry
  b = encode_sz(b)
  u = encode_sz(u)
  local u = string.format("%s?b=%s&n=%s", url, b, u)
  local b, c = http.request(u)
  if b == nil or c == nil or c ~= 200 then
    return "http request failed"
  end
  return parse(b)
end

local function upload(url, b, n, v, m)
  -- checksum of the parameters
  local params = string.format("%s%s", n, v)
  local cs = md5.sumhexa(params)
  -- perform an upload
  b = encode_sz(b)
  n = encode_sz(n)
  local u = string.format("b=%s&n=%s&v=%d&m=%s&cs=%s", b, n, v, m, cs)
  local b, c = http.request(url, u)
  -- was the upload successful?
  if b == nil or c == nil or c ~= 200 then
    return "http request failed"
  end
  return parse(b)
end

return function()
  local lib = http.avatar
  local board = {}
  
  board.connected = false
  board.busy = false

  -- connect and find a leaderboard
  function board:connect(b)
    assert(board.routine == nil, "board not ready")
    -- url should point to the hiscore .PHP file
    board.name = b
    board.connected = (lib.url and lib.user and lib.userid)
    lib.boards[#lib.boards + 1] = board
  end
  
  -- disconnect
  function board:disconnect()
    assert(board.routine == nil, "board not ready")
    board.name = nil
    board.connected = false
    for i = 1, #lib.boards do
      if lib.boards[i] == board then
        table.remove(lib.boards, i)
        break
      end
    end
  end
  
  -- download entries in range (s)tart to (e)nd
  function board:loadScores(s, e, t)
    assert(s and e, "start and end values must be specified")
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    t = t or "global"
    board.busy = true
    board.state = download
    board.params = { lib.url, board.name, s, e, t }
  end
  
  -- download entry for a particular user
  function board:loadScoresFor(user)
    user = user or lib.user
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    board.busy = true
    board.state = downloadFor
    board.params = { lib.url, board.name, user }
  end
  
  -- upload a new entry
  function board:saveScore(v, t)
    t = t or "best"
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    assert(v == math.floor(v), "score must be an integer")
    assert(t == "best" or t == "replace" or t == "insert", "unsupported upload method")
    board.busy = true
    board.state = upload
    board.params = { lib.url, board.name, lib.user, v, t }
  end
  
  -- update the current state
  function board:update()
    if board.busy and board.connected then
      board.wait = (board.wait or 8) - 1
      if board.wait > 0 then
        return
      end
      local s = board.state
      local v = s(unpack(board.params))
      if type(v) == "string" then
        board:onCallback("onError", v)
        board.connected = false
      else
        if s == upload then
          board:onCallback("onUpload", v)
        elseif s == download or s == downloadFor then
          board.scores = v
          board:onCallback("onDownload", v)
        end
      end
      board.busy = false
      board.state = nil
      board.params = nil
      board.wait = 8
    end
  end
  
  -- generic callback
  function board:onCallback(f, ...)
    if board[f] then
      board[f](board, ...)
    end
  end
  
  return board
end
