-- php web client
http = require("socket.http")
require("md5")
require('LuaXml')

local url = "http://2dengine.com/commando/hs/"

local lib = {}

http.TIMEOUT = 3
http.avatar = lib
lib.board = require('utils.avatar.http.board')
lib.boards = {}

function lib.init()
end

function lib.release()
end

--- Connects to account
function lib.connect()
  local b, c = http.request(url)
  if b == nil or c == nil or c ~= 200 then
    return
  end
  local r, x = pcall(xml.eval, b)
  if r == false then
    return
  end
  local u = xml.find(x, 'Message')
  if u.type == "ip" then
    lib.url = url
    lib.user = os.getenv("USERNAME")
    lib.userid = u[1]
  end
end

--- Disconnects from account
function lib.disconnect()
  lib.url = nil
  lib.user = nil
  lib.userid = nil
end

--- Checks if Steamworks is connected
-- @return true if initialized and authenticated
function lib.isConnected()
  return lib.userid ~= nil
end

--- Updates Steamworks
function lib.update()
  for i = 1, #lib.boards do
    lib.boards[i]:update()
  end
end

--- Returns the account username and ip
-- @return username and ip address
function lib.getUser()
  return lib.user, lib.userid
end

function lib.setUser(n)
  lib.user = n
end

-- User stats (unsupported)

function lib.loadStats()
  return false
end

function lib.saveStats()
  return false
end

function lib.setStat(t, k, v)
end

function lib.getStat(t, k)
end

return lib