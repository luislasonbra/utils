return function()
  -- check if connected
  if not sworks.isInitialized or not sworks.user or not sworks.user.isAuthenticated then
    return
  end

  local board = sworks.LeaderBoard()
  
  board.connected = false
  board.busy = false
  
  -- find and connect to a board
  function board:connect(b)
    assert(not board.busy, "board not ready")
    board.connected = false
    if board:Find(b) then
      board.busy = true
    end
  end
  
  function board.disconnect()
    assert(not board.busy, "board not ready")
    board.connected = false
    board.scores = nil
  end
  
  function board:onFound(status)
    board.busy = false
    board.connected = status
    if not status then
      board:onCallback("onError", "board not found")
      return
    end
  end
  
  -- download entries in range (s)tart to (e)nd
  function board:loadScores(s, e, t)
    assert(s and e and s <= e, "invalid start and end values")
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    -- global, aroundUser, friends, users
    t = t or 'global'
    board.scores = nil
    if board:DownloadEntries(t, s, e) then
      board.busy = true
    end
  end
  
  -- download entry for a particular user
  function board:loadScoresFor(uid)
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    board.scores = nil
    if board:DownloadEntriesForUsers({uid}) then
      board.busy = true
    end
  end
  
  function board:onDownloaded(status, entries)
    board.busy = false
    if not status then
      board:onCallback("onError", "board download failed")
      return
    end
    -- re-format the downloaded entry table
    local list = {}
    for _, e in ipairs(entries) do
      local u = sworks.SteamUser(e.userID)
      local entry =
      {
        userid = e.userID,
        rank = e.rank,
        score = e.score,
        name = u.name,
        country = nil
      }
      list[#list + 1] = entry
    end
    board.scores = list
    -- callback
    board:onCallback("onDownload", list)
  end
  
  -- upload a new entry
  function board:saveScore(v, t)
    assert(board.connected, "board not selected")
    assert(not board.busy, "board not ready")
    -- replace, keepBest
    t = t or "keepBest"
    if board:UploadScore(t, v, nil) then
      board.busy = true
    end
  end
  
  function board:onUploaded(status, score, changed, rank, oldrank)
    board.busy = false
    if not status then
      board:onCallback("onError", "board upload failed")
      return
    end
    if changed == 0 then
      rank = oldrank
    end
    local lp = sworks.LocalSteamUser()
    local entry =
    {
      changed = (changed == 1),
      -- todo userID is a userdata object
      userid = lp:GetSteamID(),
      rank = rank,
      score = score,
      name = lp.name,
      country = lp:GetCountry() -- only available for local user
    }
    local list = { entry }
    board.scores = list
    -- callback
    board:onCallback("onUpload", list)
  end
  
  -- generic callback
  function board:onCallback(f, ...)
    if board[f] then
      board[f](board, ...)
    end
  end

  return board
end