-- checks if the "steam_appid.txt" file exists
-- we could write the file at this point
local f = io.open("steam_appid.txt", "r")
local appid = nil
if f then
  appid = f:read("*all")
  appid = tonumber(appid)
  f:close()
end
-- the module cannot work without an app id
if appid == nil then
  return
end
--assert(appid and appid > 0, "invalid Steam application ID")

-- check if Steamworks is available
local r, e = pcall(require, 'sworks')
if r == false or e == nil then
  return
end

local lib = {}

lib.board = require('utils.avatar.sworks.board')

--- Initializes Steamworks
function lib.init()
  if not sworks.isInitialized then
    sworks.init()
  end
end

--- Releases Steamworks
function lib.release()
  if sworks.isInitialized then
    sworks.shutdown()
  end
end

--- Connects to account
function lib.connect()
  if sworks.isInitialized and sworks.user == nil then
    -- authentication
    -- why would we need more than one reference in Lua?
    local u = sworks.LocalSteamUser()
    sworks.user = u
    -- lua crash without these bad boys
    function u:onAchievementStored(status)
    end
    function u:onStatsStored(isGroup, name, value, maxValue)
    end
  end
end

--- Disconnects from account
function lib.disconnect()
  if sworks.isInitialized and sworks.user then
    sworks.user = nil
  end
end

--- Checks if Steamworks is connected
-- @return true if initialized and authenticated
function lib.isConnected()
  return sworks.isInitialized and sworks.user and sworks.user.isAuthenticated
end

--- Updates Steamworks
function lib.update()
  sworks.update()
end

--- Returns the username (UTF-8) and account id (ANSI)
--- Don't return the "LocalUser" object
--- in the event we want to add another backend
-- @return username and account id
function lib.getUser()
  local u = sworks.user
  if u then
    return u.name, u:GetSteamID()
  end
end

-- User stats

--- Request client to download all user stats. Always call first.
-- Note: function is not asynchronous.
function lib.loadStats()
  local u = sworks.user
  if u then
    return u:RequestStats()
  end
end

--- Store all local stat modifications on the server.
-- Don't notify the user before receiving notification from the server via:
function lib.saveStats()
  local u = sworks.user
  if u then
    return u:StoreStats()
  end
end

--- Once achievements or stats are available to the client
-- they can be modified locally.
function lib.setStat(t, k, v)
  local u = sworks.user
  if u == nil then
    return
  end
  if t == 'bool' then
    return u:SetAchievement(k)
  elseif t == 'int' then
    return u:SetStatInt(k, v)
  elseif t == 'float' then
    return u:SetStatFloat(k, v)
  end
end

function lib.getStat(t, k)
  local u = sworks.user
  if u == nil then
    return
  end
  local r, v
  if t == 'bool' then
    r, v = u:GetAchievement(k)
  elseif t == 'int' then
    r, v = u:GetStatInt(k)
  elseif t == 'float' then
    r, v = u:GetStatFloat(k)
  end
  if r == true then
    return v
  end
end

return lib