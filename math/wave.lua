-- Experimental waveform and tweening library

-- The default wave period is 2 [-1, 1]
-- therefore for tweens, easing "in" is in range [0, 0.5] or [-1, -0.5]
-- and easing "out" is in range [0.5, 1] or [-0.5, 0]

local sin = math.sin
local cos = math.cos
local floor = math.floor
local pow = math.pow
local sqrt = math.sqrt
local abs = math.abs
local pi = math.pi

local wave = {}

--- Waves

--- Sine wave
function wave.sin(x)
  return sin(x*pi)
end

--- Cosine wave
function wave.cos(x)
  return cos(x*pi)
end

--- Sawtooth wave
function wave.saw(x)
  return (x + 1)%2 - 1
end

--- Triangle wave
function wave.tri(x)
  return abs(1 - (x - 0.5)%2)*2 - 1
end

--- Square wave
-- @param d Duty cycle (optional)
function wave.square(x, d)
  d = d or 0.5
  local t = (x + d)%1
  local a = floor(t/0.5)
  return (t >= d) and 1 or -1
end

--- Tweening

local function _wrap(n)
  n = n*2 -- period is 2, make it 4
  local s = 1
  local n2 = n%1
  if n%2 >= 1 then
    n2 = 1 - n2
    s = -1
  end
  local n4 = n%4
  if n4 >= 1 and n4 < 3 then
    s = -s
  end
  return n2, s
end

--- Linear
function wave.lerp(x)
  local x2, s = _wrap(x)
  return x2*s
end

--- Quadratic
function wave.quad(x)
  local x2, s = _wrap(x)
  return x2^2*s
end

--- Cubic
function wave.cubic(x)
  local x2, s = _wrap(x)
  return x2^3*s
end

--- Quart
function wave.quart(x)
  local x2, s = _wrap(x)
  return x2^4*s
end

--- Quint
function wave.quint(x)
  local x2, s = _wrap(x)
  return x2^5*s
end

--- Exponential
function wave.expo(x)
  local x2, s = _wrap(x)
  return pow(2, 10*(x2 - 1))*s
end

--- Circular
function wave.circ(x)
  local x2, s = _wrap(x)
  return -(sqrt(1 - x2^2) - 1)*s
end

--- Back
-- @param b Back amount (optional)
function wave.back(x, b)
  b = b or 1
  local x2, s = _wrap(x)
  x2 = x2 - 1
  return (((b + 1)*x2 + b)*x2*x2 + 1)*s
end

--[[
--- Noise wave
-- To guarantee the longest possible period of this sequence
-- c and m should be coprime, a-1 should be divisible by all prime factors of m
-- and also for 4 if m is divisible by 4.
local m, a, c = 2^24, 1140671485, 12820163
local m2 = m/2
function wave.noise(x)
  x = abs(x)
  local s, f = math.modf(x)
  if f ~= 0 then
    s = math.frexp(x)
    --s = floor(f*m)
  end
  local n = (a*s + c)%m
  return (n - m2)/m2
end
]]

return wave