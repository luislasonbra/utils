local vec = require("utils.math.vector")

local seg = vec.seg

local tremove = table.remove
local cos = math.cos
local sin = math.sin
local pi = math.pi
local pi2 = math.pi*2
local floor = math.floor

local poly = {}

--- Copies polygon vertices
-- @param p1 source
-- @param p2 destintation (optional)
-- @param destination
function poly.copy(p1, p2)
  p2 = p2 or {}
  assert(p1 ~= p2, "source and destination polygons are the same")
  local n1 = #p1
  local n2 = #p2
  -- erase vertices from n1+1 to n2
  for i = n2, n1 + 1, -1 do
    p2[i] = nil
  end
  -- overwrite vertices from 1 to n1
  for i = 1, n1 do
    p2[i] = p1[i]
  end
  return p2
end

--- Erases all vertices
-- @param p polygon
function poly.clear(p)
  for i = #p, 1, -1 do
    p[i] = nil
  end
end

--- Reverses the polygon
-- @param p polygon
function poly.reverse(p)
  local n = #p
  -- reverse in place
  for i = 1, floor(n/2) do
    local i2 = n - i + 1
    p[i], p[i2] = p[i2], p[i]
  end
end

--- Finds the extents of the polygon
-- @param p polygon
-- @return left, top, right and bottom extents
function poly.extents(p)
  local v1 = p[1]
  local l, t = v1.x, v1.y
  local r, b = l, t
  for i = 2, #p do
    local v2 = p[i]
    local x, y = v2.x, v2.y
    if x < l then
      l = x
    elseif x > r then
      r = x
    end
    if y < t then
      t = y
    elseif y > b then
      b = y
    end
  end
  return l, t, r, b
end

--- Translates the polygon
-- @param p polygon
-- @param x, y translation offset
function poly.translate(p, x, y)
  for i = 1, #p do
    local v = p[i]
    v.x = v.x + x
    v.y = v.y + y
  end
  return p
end

--- Scales the polygon
-- @param p polygon
-- @param sx, sy scale
function poly.scale(p, sx, sy)
  for i = 1, #p do
    local v = p[i]
    v.x = v.x*sx
    v.y = v.y*sy
  end
  return p
end

--- Rotates the polygon
-- @param p polygon
-- @param a angle in radians
function poly.rotate(p, a)
  local c = cos(a)
  local s = sin(a)
  for i = 1, #p do
    local v = p[i]
    local x, y = v.x, v.y
    v.x = c*x - s*y
    v.y = s*x + c*y
  end
  return p
end

function poly.regular(n, r, out)
  out = out or {}
  for i = 1, n do
    local a = (i - 1)/n*pi2
    local x = cos(a)*r
    local y = sin(a)*r
    out[i] = { x = x, y = y }
  end
  return out
end

function poly.regular2(n, s, out)
  local r = s/(2*sin(pi/n))
  return poly.regular(n, r, out)
end

function poly.regular3(n, a, out)
  local r = a/(cos(pi/n))
  return poly.regular(n, r, out)
end

function poly.area2(p)
  local s = 0
  local v1 = p[#p]
  local x1, y1 = v1.x, v1.y
  for i = 1, #p do
    local v2 = p[i]
    local x2, y2 = v2.x, v2.y
    s = s + (x2 + x1)*(y2 - y1)
    x1, y1 = x2, y2
  end
  return s
end

function poly.area(p)
  local s = poly.area2(p)/2
  if s < 0 then
    s = -s
  end
  return s
end

--- Checks if the polygon winding is counter-clockwise
--- Range:
-- Assuming the y-axis points up and the x-axis points right
--- Parameters:
-- @p polygon
-- @return true if the polygon is counter-clockwise
function poly.ccw(p)
  return poly.area2(p) > 0
end

--- Checks if the polygon is convex
--- Description:
-- For any counter-clockwise oriented polygon,
-- as soon as we find a clockwise turn
-- then we know it's not convex and vice versa
--- Parameters:
-- @param p polygon
-- @param ccw winding of the polygon
-- @return true if the polygon is convex or false otherwise
function poly.convex2(p, ccw)
  if #p < 3 then
    return false
  end
  local v3 = p[2]
  local v2 = p[1]
  local x3, y3 = v3.x, v3.y
  local x2, y2 = v2.x, v2.y
  for i = #p, 1, -1 do
    local v1 = p[i]
    local x1, y1 = v1.x, v1.y
    -- signed triangle area
    local s = (x1 - x3)*(y2 - y3) - (y1 - y3)*(x2 - x3)
    if ccw then
      -- ccw polygons: if we make a cw (right) turn we know it's not convex
      if s < 0 then
        return false
      end
    else
      -- cw polygons: if we make a ccw (left) turn we know it's not convex
      if s > 0 then
        return false
      end
    end
    x3, y3 = x2, y2
    x2, y2 = x1, y1
  end
  return true
end

function poly.convex(p)
  local ccw = poly.ccw(p)
  return poly.convex2(p, ccw)
end

function poly.simple(p)
  -- ignore the last edge (#p to 1) on the first pass
  local n = #p - 2
  -- edges #p to 1 to #p
  local v2 = p[#p]
  for i = 1, #p do
    local v1 = p[i]
    -- edges i + 1 to n
    local v4 = p[i + 1]
    for j = i + 2, n do
      local v3 = p[j]
      if seg(v1, v2, v3, v4) then
        return false
      end
      v4 = v3
    end
    v2 = v1
    -- include the last edge (#p to 1) after the first pass
    n = #p
  end
  return true
end

-- Tests if a point is inside the polygon
-- based on Matthias Richter
function poly.point(p, pt)
  local x, y = pt.x, pt.y
  local inside = false
  local v1 = p[#p]
  local x1, y1 = v1.x, v1.y
  for i = 1, #p do
    local v2 = p[i]
    local x2, y2 = v2.x, v2.y
    local cc =
      -- cut
      (((y1 > y and y2 < y) or (y1 < y and y2 > y)) and
      (x - x1 < (y - y1)*(x2 - x1)/(y2 - y1))) or
      -- cross
      ((y1 == y and x1 > x and y2 < y) or
      (y2 == y and x2 > x and y1 < y))
    if cc then
      inside = not inside
    end
    x1, y1 = x2, y2
  end
  --if not poly.clockwise(p) then
    --inside = not inside
  --end
  return inside
end

-- Tests if one polygon is inside another
-- Both must be non-self-intersecting
function poly.polygon(p, p2)
  -- all points in p2 must be inside p
  for i = 1, #p2, 2 do
    if not poly.point(p, p2[i]) then
      return false
    end
  end
  -- no points from p can be in p2
  for i = 1, #p, 2 do
    if poly.point(p2, p[i]) then
      return false
    end
  end
  -- no edges can intersect
  local v1 = p[#p]
  for i = 1, #p do
    local v2 = p[i]
    local v3 = p2[#p2]
    for j = 1, #p2 do
      local v4 = p2[j]
      if seg(v1, v2, v3, v4) then
        return false
      end
      v3 = v4
    end
    v1 = v2
  end
  return true
end

--[[
function poly.area3(p, i1)
  local n = #p
  if i1 < 1 or i1 > n then
    error("Vertex index out of range: " .. i1)
  end
  -- wrap vertices
  local i0 = i1 - 1
  local i2 = i1 + 1
  i0 = (i0 - 1)%n + 1
  i2 = (i2 - 1)%n + 1
  
  -- same as sta
  local v0 = p[i0]
  local v1 = p[i1]
  local v2 = p[i2]
  return (v2.y - v0.y)*(v1.x - v0.x) - (v2.x - v0.x)*(v1.y - v0.y)
end

function poly.reflex_ccw(p, i1)
 return poly.area3(p, i1) <= 0
end

function poly.reflex(p, i1)
  local r = poly.reflex_ccw(p, i1)
  if not poly.ccw(p) then
    r = not r
  end
  return r
end
]]

--- Merges two simple polygons into one weak polygon
-- @p1 first polygon
-- @p2 second polygon
-- @i index on the first polygon
-- @j index on the second polygon
-- @out resulting polygon (optional)
-- @return resulting polygon
local _p2 = {}
function poly.bridge(p, p2, i, j, out)
  assert(i, "first polygon index is nil")
  assert(j, "second polygon index is nil")
  -- the two polygons must have reverse windings
  if poly.ccw(p) == poly.ccw(p2) then
    -- fill buffer
    p2 = poly.copy(p2, _p2)
    poly.reverse(p2)
    j = #p2 - j
  end
  assert(p[i] and p[i + 1], "first polygon index is out of range")
  assert(p2[j] and p2[j + 1], "second polygon index is out of range")
  -- build the resulting polygon
  out = out or {}
  if out == p then
    -- vertices j to #p2 on the second polygon
    for j2 = #p2, j, -1 do
      tinsert(out, i, p2[j2])
    end
    -- vertices 1 to j on the second polygon
    for j2 = j + 1, 1, -1 do
      tinsert(out, i, p2[j2])
    end
  else
    local no = #out
    -- vertices 1 to i on the first polygon
    for i2 = 1, i + 1 do
      out[#out + 1] = p[i2]
    end
    -- vertices j to #p2 on the second polygon
    for j2 = j, #p2 do
      out[#out + 1] = p2[j2]
    end
    -- vertices 1 to j on the second polygon
    for j2 = 1, j + 1 do
      out[#out + 1] = p2[j2]
    end
    -- vertices i to #p on the first polygon
    for i2 = i, #p do
      out[#out + 1] = p[i2]
    end
    -- merging, produces a weak polygon with two extra vertices
    assert(#p + #p2 + 4 - no == #out)
  end
  return out
end

return poly