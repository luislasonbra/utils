display:create('polytest', 800, 600, 32, true)

require('utils.part.strict')
strict_declare_globals('timer', 'lfs')

local profile = require('utils.log.profile')
profile.setclock(system.get_ticks)

local s = Sprite()
display.viewport:add_child(s)
local c = s.canvas
local f = Font()
f:load_file('utils/fonts/ttf/FreeMonoBold.ttf', 14)

local poly = require('utils.math.polygon')
local tri = require('utils.math.triangulate')
local polygons = {}
local polygon
local vertex
local labels = false
local vertices = false
local terminal = require('utils.log.terminal')

local function readfile(fn)
  local f = io.open(fn, "r")
  assert(f, fn)
  local fsz = f:read("*a")
  -- polygon
  local polygon = nil
  local path = {}
  for line in string.gmatch(fsz, "[^\n]+") do
    assert(line ~= "")
    local x, y = string.match(line, "([+-]?[%deE.-]+)[%s,]+([+-]?[%deE.-]+)")
    x, y = tonumber(x), tonumber(y)
    if x and y then
      path[#path + 1] = { x = x, y = y }
    else
      if #path > 0 then
        if polygon then
          polygon.sub[#polygon.sub + 1] = path
        else
          polygon = path
          polygon.sub = {}
        end
        path = {}
      end
    end
  end
  if #path > 0 then
    if polygon then
      polygon.sub[#polygon.sub + 1] = path
    else
      polygon = path
      polygon.sub = {}
    end
  end
  return polygon
end

local function read(fn)
  math.randomseed(system.get_ticks())

  local p = readfile(fn)
  -- vertices in agen are flipped
  poly.scale(p, 1, -1)
  for i = 1, #p.sub do
    poly.scale(p.sub[i], 1, -1)
  end
  -- center it
  local l,t,r,b = poly.extents(p)
  local cx,cy = (l + r)/2, (t + b)/2
  poly.translate(p, -cx, -cy)
  for i = 1, #p.sub do
    poly.translate(p.sub[i], -cx, -cy)
  end
  -- scale it (again!)
  local w, h = (r - l), (b - t)
  local s = math.min(800/w, 600/h)
  assert(s > 0)
  poly.scale(p, s, s)
  for i = 1, #p.sub do
    poly.scale(p.sub[i], s, s)
  end
  return p
end

timer = Timer()
timer:start(16, true)

local white = Color(255,255,255)
local red = Color(255,0,0)
local lime = Color(0,255,0)

local function draw(out, color)
  if #out == 0 then
    return
  end
  c:move_to(out[1].x, out[1].y)
  for i = 2, #out do
    c:line_to(out[i].x, out[i].y)
  end
  c:line_to(out[1].x, out[1].y)
  c:set_line_style(1, color)
  c:stroke()
  if vertices then
    for i = 1, #out do
      c:move_to(out[i].x, out[i].y)
      c:square(4)
    end
  end
  c:set_line_style(1, color)
  c:stroke()
  if out.sub then
    for i = 1, #out.sub do
      draw(out.sub[i], color)
    end
  end
end

function timer:on_tick()
  if #polygons == 0 then
    polygons = { poly.regular(math.random(8, 32), 300) }
  end
  c:clear()
  for _, polygon in ipairs(polygons) do
    draw(polygon, white)
    if polygon.sub then
      local cut = tri.cutholes(polygon, polygon.sub)
      if cut then
        draw(cut, lime)
      end
    end
    if labels then
      c:set_font(f)
      for i = 1, #polygon do
        c:move_to(polygon[i].x + 10, polygon[i].y - 5)
        --[[
        if poly.reflex(polygon, i) then
          i = i .. 'R'
        end
        ]]
        c:write(i)
      end
    end
  end
  terminal.clear()
  if polygon then
    if vertex then
      c:move_to(polygon[vertex].x, polygon[vertex].y)
      c:square(8)
      c:set_fill_style(red)
      c:fill()
      if mouse:is_down(108) then
        polygon[vertex].x = math.floor(mouse.xaxis/10)*10
        polygon[vertex].y = math.floor(mouse.yaxis/10)*10
      end
    end
    
    profile.start()
    local t0 = system.get_ticks()
    local res = tri.polygon(polygon)
    local t1 = system.get_ticks()
    profile.stop()
    for i = 1, #res, 3 do
      c:move_to(res[i].x, res[i].y)
      c:line_to(res[i + 1].x, res[i + 1].y)
      c:line_to(res[i + 2].x, res[i + 2].y)
      c:close_path()
    end
    c:set_fill_style(red, 0.5)
    c:fill_preserve()
    c:set_line_style(1, red, 0.75)
    c:stroke()
    
    terminal.trace('vertices', #polygon)
    terminal.trace('area', poly.area(polygon))
    terminal.trace('area2', poly.area2(polygon))
    terminal.trace('simple', poly.simple(polygon))
    terminal.trace('ccw', poly.ccw(polygon))
    terminal.trace('convex', poly.convex(polygon))
    terminal.trace('triangles', #res/3 .. ' (should be ' .. math.max(#polygon - 2, 0) .. ')')
    terminal.trace('time', t1 - t0)

    --terminal.print(profile.report('time'))
    profile.reset()
  end
end

function mouse:on_press(b)
  polygon = nil
  vertex = nil
  local x, y = mouse.xaxis, mouse.yaxis
  for _, p in ipairs(polygons) do
    for i = 1, #p do
      local v = p[i]
      if x > v.x - 10 and x < v.x + 10 and y > v.y - 10 and y < v.y + 10 then
        polygon = p
        vertex = i
        break
      end
    end
  end
  if polygon == nil then
    local pt = { x = x, y = y }
    for _, p in ipairs(polygons) do
      if poly.point(p, pt) then
        polygon = p
        break
      end
    end
  end
end


require("lfs")
local keys = require('utils.agen.keys')
local files = {}
for file in lfs.dir("utils/math/tests/data") do
  if file ~= '.' and file ~= '..' then
    files[#files + 1] = "utils/math/tests/data/" .. file
  end
end
local i = 12

function keyboard:on_press(k)
  if (k == 83 or k == 104) and vertex then
    table.remove(polygon, vertex)
    vertex = nil
  elseif k == 87 then
    i = (i - 1)
    if i == 0 then i = #files end
  elseif k == 89 then
    i = (i + 1)
    if i > #files then i = 1 end
  elseif k == 53 then
    labels = not labels
  elseif k == 61 then
    vertices = not vertices
  end
  polygons = { read(files[i]) }
  polygon = nil
  vertex = nil
end
