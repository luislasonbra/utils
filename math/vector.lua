local sqrt = math.sqrt
local acos = math.acos
local cos = math.cos
local sin = math.sin
local atan2 = math.atan2
local pi = math.pi
local pi2 = math.pi*2
local smt = setmetatable

local vec = {}

--- Constructors

--- From coordinates
-- @param x first coordinate
-- @param y second coordinate
-- @return vector
function vec.new(x, y)
  local v = { x = x, y = y }
  smt(v, vec)
  return v
end

--- From polar coordinates
-- @param a angle
-- @param d length
-- @return vector
function vec.newp(a, d)
  local x, y = cos(a), sin(a)
  if d then
    x, y = x*d, y*d
  end
  local v = { x = x, y = y }
  smt(v, vec)
  return v
end

--- From another vector
-- @param v vector or point
-- @return vector
function vec.copy(v)
  local v = { x = v.x, y = v.y }
  smt(v, vec)
  return v
end

--- Constant

--- Square length
-- @param a vector
-- @param square length
function vec.len2(a)
  local ax, ay = a.x, a.y
  return ax*ax + ay*ay
end

--- Length
-- @param a vector
-- @param length
function vec.len(v)
  local ax, ay = a.x, a.y
  return sqrt(ax*ax + ay*ay)
end

--- Dot product
-- @param a first vector
-- @param b second vector
-- @param dot product
function vec.dot(a, b)
  return a.x*b.x + a.y*b.y
end

--- Cross product
-- @param a first vector
-- @param b second vector
-- @param cross product
function vec.cross(a, b)
  return a.x*b.y - a.y*b.x
end

--- Angle between vectors
-- @param a first vector
-- @param b second vector
-- @param angle between the two
function vec.vang(a, b)
  local ax, ay = a.x, a.y
  local bx, by = b.x, b.y
  -- local a = len2(ax, ay)*len2(bx, by)
  local a = (ax*ax + ay*ay)*(bx*bx + by*by)
  a = sqrt(a)
  if a > 0 then
    -- a = acos(dot(ax, ay, bx, by)/a)
    a = acos((ax*bx + ay*by)/a)
    if ax*by - ay*bx < 0 then
      a = -a
    end
  end
  return a
end

--- Angle between vectors (alternative)
--- Assumes both vectors have non-zero length
-- @param a first vector
-- @param b second vector
-- @param angle between the two
function vec.vang2(a, b)
  local a = atan2(b.y, b.x) - atan2(a.y, a.x)
  return (a + pi)%(pi2) - pi
end

--- Square distance between points
-- @param a first point
-- @param b second point
-- @param square distance
function vec.dist2(p1, p2)
  local dx = p1.x - p2.x
  local dy = p1.y - p2.y
  return dx*dx + dy*dy
end

--- Distance between points
-- @param a first point
-- @param b second point
-- @param distance
function vec.dist(p1, p2)
  local dx = p1.x - p2.x
  local dy = p1.y - p2.y
  return sqrt(dx*dx + dy*dy)
end

--- Angle between points
--- Assumes the points are the same
-- @param a first point
-- @param b second point
-- @param angle from the first to the second point
function vec.ang(p1, p2)
  return atan2(p1.y - p2.y, p1.x - p2.x)
end

--- Twice the signed area of a triangle
-- @param p1 first point
-- @param p2 second point
-- @param p3 third point
-- @param twice the signed area
function vec.sta2(p1, p2, p3)
  local x3, y3 = p3.x, p3.y
  return (p1.x - x3)*(p2.y - y3) - (p1.y - y3)*(p2.x - x3)
end

--- Non-constant

--- Normalize
-- @param a vector
-- @return initial lenght of the vector
function vec.norm(a)
  local ax, ay = a.x, a.y
  local d = sqrt(ax*ax + ay*ay)
  if d > 0 then
    a.x, a.y = ax/d, ay/d
  end
  return d
end

--- Clamp length
-- @param a vector
-- @return initial lenght of the vector
function vec.clamp(v, d)
  local x, y = v.x, v.y
  local _d = sqrt(x*x + y*y)
  if _d > d then
    v.x = x/_d*d
    v.y = y/_d*d
  end
  return _d
end

--- Rotate by angle
-- @param v vector
-- @param a angle
function vec.rotate(v, a)
  local x, y = v.x, v.y
  local c = cos(a)
  local s = sin(a)
  v.x = c*x - s*y
  v.y = s*x + c*y
end

--- Extend or move to point
-- @param p1 first point
-- @param p2 second point
function vec.ext(p1, p2, dist)
  local x2, y2 = p2.x, p2.y
  local dx, dy = x2 - p1.x, y2 - p1.y
  local d = sqrt(dx*dx + dy*dy)
  if d > 0 then
    d = dist/d
    p1.x = dx*d + x2
    p1.y = dy*d + y2
  end
end

--- Intersections

--- Segments
-- @param p1 start point of the first segment
-- @param p2 end point of the first segment
-- @param p2 start point of the second segment
-- @param p3 end point of the second segment
-- @return intersection values in normalized range 0-1
function vec.seg(p1, p2, p3, p4, p)
  if p then
    -- if using the colon operator, shift the stack left
    p, p1, p2, p3, p4 = p1, p2, p3, p4, p
  end
  local x1, y1 = p1.x, p1.y
  local x3, y3 = p3.x, p3.y
  local dx1, dy1 = p2.x - x1, p2.y - y1
  local dx2, dy2 = p4.x - x3, p4.y - y3
  local dx3, dy3 = x1 - x3, y1 - y3
  local d = dx1*dy2 - dy1*dx2
	if d == 0 then
    -- collinear or degenerate
    return
  end
  local t1 = (dx2*dy3 - dy2*dx3)/d
  if t1 < 0 or t1 > 1 then
    -- non-intersecting segment or ray
    return
  end
  local t2 = (dx1*dy3 - dy1*dx3)/d
  if t2 < 0 or t2 > 1 then
    -- non-intersecting segment or ray
    return
  end
  if p then
    p.x = x1 + t1*dx1
    p.y = y1 + t1*dy1
  end
  return t1, t2
end

--- Point in triangle
-- @param p point
-- @param p1 first vertex of the triangle
-- @param p2 second vertex of the triangle
-- @param p3 third vertex of the triangle
-- @return true if the point is inside
function vec.pit(p, p1, p2, p3)
  local x, y = p.x, p.y
  local px1, py1 = p1.x - x, p1.y - y
  local px2, py2 = p2.x - x, p2.y - y
  local ab = px1*py2 - py1*px2
  local px3, py3 = p3.x - x, p3.y - y
  local bc = px2*py3 - py2*px3
  local sab = ab < 0
  if sab ~= (bc < 0) then
    return false
  end
  local ca = px3*py1 - py3*px1
  return sab == (ca < 0)
end

--- Point in circle
-- @param p point
-- @param c circle center point
-- @param r radius
-- @return true if the point is inside
function vec.pic(p, c, r)
  local dx, dy = p.x - c.x, p.y - c.y
  return dx*dx + dy*dy < r*r
end

--- Point in ellipse
-- @param p point
-- @param e ellipse center point
-- @param rx x-radius
-- @param ry y-radius
-- @return true if the point is inside
function vec.pie(p, e, rx, ry)
  local dx, dy = p.x - e.x, p.y - e.y
  if rx == 0 or ry == 0 then
    return
  end
  return (dx*dx)/(rx*rx) + (dy*dy)/(ry*ry) < 1
end

--- Point in rectangle
-- @param p point
-- @param r rectangle center point
-- @param hw half-width extent
-- @param hh half-height extent
-- @return true if the point is inside
function vec.pir(p, r, hw, hh)
  local dx, dy = p.x - r.x, p.y - r.y
  return dx*dx < hw*hw and dy*dy < hh*hh
end

return vec