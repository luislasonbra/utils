--- Credits:
-- Christer Ericson’s Real-time Collision Detection
-- David Eberly’s Triangulation by Ear Clipping
-- Matthias Richter’s Hardon Collider

local vec = require("utils.math.vector")
local poly = require("utils.math.polygon")

local pit = vec.pit
local sta2 = vec.sta2
local seg = vec.seg
local sqrt = math.sqrt

local tri = {}

local _pt0 = { x = 0, y = 0 }
local _pt1 = { x = 0, y = 0 }

function tri.cuthole(p, s)
  assert(p and s)
  if #s == 0 then
    return p
  end
  
  --assert(poly.clockwise(p) ~= poly.clockwise(s))
  if poly.ccw(p) == poly.ccw(s) then
    poly.reverse(s)
  end
  
  -- search the vertices on the inner polygon
  local _i = 1
  local _imax = s[_i].x
  for i = 2, #s do
    -- find the vertex with maximum x-value
    local x = s[i].x
    if x > _imax then
      _imax = x
      _i = i
    end
  end
  local i = s[_i]
  local l,t,r,b = poly.extents(p)
  _pt1.x = r
  _pt1.y = i.y
  
  -- search the edges on the outer polygon
  local _j, _jmin
  local q = { x = 0, y = 0 }
  local i1 = #p
  local v1 = p[i1]
  for i2 = 1, #p do
    -- find the nearest edge to the right of x,y
    local v2 = p[i2]
    -- maximum x-value for the edge a-b
    local j = i2
    if v1.x > v2.x then
      --_pt1.x = v1.x
      j = i1
    end
    --_pt1.x = _pt1.x + 1
    local jmin = seg(_pt0, i, _pt1, v1, v2)
    -- if there are two or more shared edges
    -- we want to bridge the last one
    -- therefore >= is critical if there are shared edges
    if jmin and (_jmin == nil or _jmin >= jmin) then
      _j = j
      _jmin = jmin
      q.x = _pt0.x
      q.y = _pt0.y
    end
    -- move to the next edge
    i1 = i2
    v1 = v2
  end
  
  if _j == nil then
    --error("hole is not entirely inside the polygon")
    return
  end
  
  -- skip if qx,qy is a vertex on the outer polygon


  -- table of reflex vertices
  local reflex = {}
  local v0 = p[#p - 1]
  local v1 = p[#p]
  for i = 1, #p do
    local v2 = p[i]
    if sta2(v0, v1, v2) <= 0 then
      reflex[v1] = true
    end
    v0 = v1
    v1 = v2
  end

  -- search reflex vertices on the outer polygon (excluding j)
  -- find any vertex strictly inside the triangle i,q,j
  -- if there are no vertices inside i,q,j, we have a valid bridge
  -- otherwise choose the point of the minimum angle
  local j = p[_j]
  local nx, ny = j.x - i.x, j.y - i.y
  local n = sqrt(nx*nx + ny*ny)
  assert(n > 0)
  nx, ny = nx/n, ny/n
  for k = 1, #p do
    local vk = p[k]
    if vk ~= j then--and reflex[vk] then
      if pit(vk, i, q, j) then
        local nx2, ny2 = vk.x - i.x, vk.y - i.y
        local n2 = sqrt(nx2*nx2 + ny2*ny2)
        assert(n2 > 0)
        nx2, ny2 = nx2/n2, ny2/n2
        if nx2 > nx then
          _j = k
          n = n2
          nx, ny = nx2, ny2
        end
      end
    end
  end
  j = p[_j]

  -- we found "i" and "j", now cut polygon
  local out = {}
  -- vertices 1 to j on the outer polygon
  for i = 1, _j do
    out[i] = p[i]
  end
  -- vertices i to #s on the inner polygon
  for i = _i, #s do
    out[#out + 1] = s[i]
  end
  -- vertices 1 to i on the inner polygon
  for i = 1, _i - 1 do
    out[#out + 1] = s[i]
  end
  out[#out + 1] = { x = i.x, y = i.y }
  out[#out + 1] = { x = j.x, y = j.y }
  -- vertices j to #p on the outer polygon
  for i = _j + 1, #p do
    out[#out + 1] = p[i]
  end
  return out
end

--- Cuts holes in a polygon
--- Holes must not intersect with the boundary of the outer polygon
--- Holes must not intersect with each other
-- @param p Polygon (must not be self-intersecting)
-- @param b List of hole polygons
-- @return Resulting polygon
local _ex = {}
local _ey = {}
local function _comp(a, b)
  local x1, x2 = _ex[a], _ex[b]
  if x1 == x2 then
    return _ey[a] < _ey[b]
  end
  return x1 < x2
end
function tri.sortholes(s)
  -- cache holes by maximum x-value
  for i = 1, #s do
    local h = s[i]
    local x, y = h[1].x, h[1].y
    for i2 = 2, #h do
      -- find the maximum x-value
      local x2, y2 = h[i2].x, h[i2].y
      if x2 > x or (x2 == x and y2 > y) then
        x, y = x2, y2
      end
    end
    _ex[h], _ey[h] = -x, -y
  end
  -- sort holes by maximum x-value
  table.sort(s, _comp)
  -- clear cache
  poly.clear(_ex)
  poly.clear(_ey)
end

local _queue = {}
function tri.cutholes(p, s)
  assert(p and s)
  -- build queue
  for i = 1, #s do
    _queue[#_queue + 1] = s[i]
  end
  -- sort queue
  tri.sortholes(_queue)
  -- process queue
  local out = poly.copy(p)
  while #_queue > 0 do
    local s2 = table.remove(_queue, 1)
    local _out = tri.cuthole(out, s2)
    out = _out or out
  end
  return out
end

--- Decomposes a polygon into triangles through "ear clipping"
--- Assumes the polygon doesn't have zero-length edges
-- @param p Polygon (simple and counter-clockwise)
-- @param out Resulting triangles (optional)
-- @return Resulting triangles
function tri.polygon_ccw(p, out)
  -- out buffer
  out = out or {}

  -- number of vertices
  local n = #p
  -- fewer than three vertices?
  if n < 3 then
    return
  end
  
  -- double linked list of adjacent vertices
  -- todo: won't work with shared vertices
  local left = {}
  local right = {}
  for i = 1, n do
    local v = p[i]
    assert(left[v] == nil)
    assert(right[v] == nil)
    left[v] = p[i - 1]
    right[v] = p[i + 1]
  end
  local first = p[1]
  local last = p[n]
  left[first] = last
  right[last] = first

--[[
  -- table of reflex vertices
  local reflex = {}
  local v0 = p[n - 1]
  local v1 = p[n]
  for i = 1, n do
    local v2 = p[i]
    if sta2(v0, v1, v2) <= 0 then
      reflex[v1] = true
    end
    v0 = v1
    v1 = v2
  end
]]
  -- number of skipped vertices
  local nskip = 0
  -- current index
  local i1 = first
  while n >= 3 do
    local isear = true
    -- possible ear tip i0,i1,i2
    local i0, i2 = left[i1], right[i1]
    -- skip if there only three vertices left
    if n > 3 then
      -- check if vertex i0,i1,i2 is an ear tip
      --if not reflex[i1] then
      if sta2(i0, i1, i2) >= 0 then
        -- check if any reflex vertices are inside the triangle i0,i1,i2
        local j1 = right[i2]
        repeat
          -- possible reflex vertex j0,j1,j2
          local j0, j2 = left[j1], right[j1]
          -- check if vertex j0,j1,j2 is reflex
          if sta2(j0, j1, j2) <= 0 then
        --for j1 in pairs(reflex) do
          -- exlude vertices i0,i1,i2 (we already know i1 is not reflex)
          --if j1 ~= i0 and j1 ~= i2 then
            -- check if j1 is inside the triangle i0,i1,i2
            -- should be true if j1 is on an edge or vertex
            -- todo: since we know the winding of the triangle,
            -- the following test could be optimized further
            if pit(j1, i0, i1, i2) then
              isear = false
              break
            end
          --end
        --end
          end
          j1 = right[j1]
        until j1 == i0
      else
        isear = false
      end
    end
    if isear then
      -- ear tip found
      -- output triangle i0,i1,i2
      local s = #out
      out[s + 1] = i0
      out[s + 2] = i1
      out[s + 3] = i2
      -- remove vertex i1 from the linked list
      -- by redirecting "left" and "right"
      right[i0] = i2
      left[i2] = i0
      
      left[i1] = nil
      right[i1] = nil
      -- update the reflex table accordingly
      --reflex[i0] = (sta2(left[i0], i0, i2) <= 0) and true or nil
      --reflex[i1] = nil
      --reflex[i2] = (sta2(i0, i2, right[i2]) <= 0) and true or nil
      -- decrement vertex count
      n = n - 1
      nskip = 0
      -- visit the next vertex (left)
      i1 = i0
    else
      -- not an ear tip
      -- skipped vertex
      nskip = nskip + 1
      if nskip > n then
        -- we iterated all vertices, but no more ears found
        -- possibly a self-intersecting polygon
        break
      end
      -- visit the next vertex (right)
      i1 = i2
    end
  end
  -- triangulation of simple polygons:
  -- n vertices must always produce n-2 triangles
  -- assert(n2 == #p - 2, "triangulation error")
  return out
end

local _p = {}
function tri.polygon(p, out)
  out = out or {}
  local s = p.sub
  -- the input polygon must be counter-clockwise
  if not poly.ccw(p) then
    p = poly.copy(p, _p)
    p.sub = s
    poly.reverse(p)
  end
  -- todo: handle polygon holes
  if s then
    p = tri.cutholes(p, s)
  end
  -- triangulate
  if p then
    tri.polygon_ccw(p, out)
  end
  return out
end

return tri